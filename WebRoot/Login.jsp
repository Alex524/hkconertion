<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="0">
</head>

<body bottommargin="0" topmargin="0" rightmargin="0" leftmargin="0"
	class="HomeBody">

	<html:javascript formName="LoginForm" method="validateLoginAction" />
	<html:form action="/Login" method="post"
		onsubmit="return validateLoginAction(document.LoginForm);">
		<table align="center" cellspacing="10" cellpadding="5">
			<tr>
				<td>
					<font size="3"><b>Login Form :</b></font>
				</td>
				<td>
					<b><font color="red"> <html:errors /> </font> </b>
				</td>
			</tr>
			<tr>
				<td align="left" valign="middle" class="MenuTable2" colspan="2">
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td align="left">
					User Name :
				</td>

				<td align="right">
					<input type="text" name="userName" id="userName" />
				</td>
			</tr>
			<tr>
				<td align="left">
					Password:
				</td>

				<td align="right">
					<input type="password" name="password" id="password" />
				</td>
			</tr>
			<tr>
				<td align="left">
					Please enter the numbers of the image :

				</td>

				<td align="right">

					<img id="image" border=0 src="image.jsp"
						onclick="this.src='image.jsp'" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" name="valiNumber" id="valiNumber" size="4"
						maxlength="4" />
				</td>
			</tr>
			<tr>
				<td align="right">
					<input type="submit" value="Login" class="inputButton" />
				</td>
				<td>
					<input type="reset" value="Reset" class="inputButton" />
				</td>
			</tr>

		</table>
	</html:form>
</body>

</html:html>
