function $(id) {
        return document.getElementById(id);
}

function trim(s) {
        return s.replace(/(^\s*)|(\s*$)/g, "");
}

function doSearch(curpage) {
        var url = 'doSearch';   
        var param = "keyword="+$("keyword").value+"&tablenum="+$("tablenum").value
                        +"&pagenumber="+$("pagenumber").value+"&curpage="+curpage;
        var http_request = false;

        for (i=0; i<$("tablenum").value; i++) {
                if ($("table_"+i).checked) {
                        param += "&table_"+i+"="+$("table_"+i).value;
                }
        }

        document.getElementById('searchresult').innerHTML = '<img src="images/ajax-loader.gif"> Searching...';

        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                http_request = new XMLHttpRequest();
                if (http_request.overrideMimeType) {
                        http_request.overrideMimeType('text/xml');
                        // See note below about this line
                }
        } else if (window.ActiveXObject) { // IE
                try {
                        http_request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                        try {
                                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {}
                }
        }

        if (!http_request) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
        }
        http_request.onreadystatechange = function() {
                if (http_request.readyState == 4) {
                        if (http_request.status == 200) {
                                document.getElementById('searchresult').innerHTML = http_request.responseText;
                        }
                }
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http_request.send(param);

        return false;
}
function checkAddForm() {
        tb = $("tb").value;
        nd = $("nd").value;
        ew = trim($("ew").value);
        cw = trim($("cw").value);
        ed = trim($("ed").value);
        cd = trim($("cd").value);

        if (tb == "0" && nd == "") {
                alert("Please select a dict or Add Dict");
                return false;
        }

        if (ew == "" || cw == "") {
                alert("English Word and Chinese Word not be empty");
                return false;
        }
        return true;
}
function checkSearchFrom() {
        if ($('keyword').value.length < 2) {
                alert("Illegal word");
                return false;
        }

        for (i=0; i<parseInt($("tablenum").value);i++) {
                if ($("table_"+i).checked) return true;
        }
        alert("Please select a dict");
        return false;
}

var canclose = 1;
function showDict() {
//      $("searchresult").style.width = (document.body.clientWidth-parseInt($("rightdiv").style.width)-18)+"px";
//      $("rightdiv").style.display = "";
//      $("LeftMenuArrow").style.display = "none";
        $("ArrowPic").style.display = "none";
        $("rightdiv").style.left = "0px";
}
function hideDict() {
//      $("LeftMenuArrow").style.display = "";
        $("rightdiv").style.left = "-220px";
        $("ArrowPic").style.display = "";
}

function getHeight() {
        var h1 = document.body.clientHeight;
        $("rightdiv").style.height = h1-30+"px";
        $("LeftMenuArrowTable").style.height = h1-30+"px";
        $("ArrowPic").style.top = 0+"px";
        $("ArrowPic2").style.top = 0+"px";
        $("rightdiv_table").style.height = h1-30+"px";
}

function closeDict() {
        if (canclose == 1) {
                $("rightdiv").style.display = "none";
                $("searchresult").style.width = "100%";
        }
}

function selectAllDict(obj) {
        tablenum = $("tablenum").value;
        if (obj.checked) {
                for (i=0; i<tablenum; i++) {
                        $("table_"+i).checked = true;
                }
        }else {
                for (i=0; i<tablenum; i++) {
                        $("table_"+i).checked = false;
                }
        }
}
function showpage(page, tag) {
        $("current").innerHTML = page;
        pagenumber = parseInt($("pagenumber").value);
        totalpage  = parseInt($("totalpage").value);
        totalnum   = parseInt($("totalnum").value);
        if (tag == 0) {
                for (i=page*pagenumber, count=0; i<totalnum && count<pagenumber; i++,count++) {
                        $("result_"+i).style.display = "none";
                }
        }else if (tag == 1) {
                for (i=(page-2)*pagenumber, count=0, count=0; i<totalnum && count<pagenumber; i++,count++) {
                        $("result_"+i).style.display = "none";
                }
        }
        
        for (i=(page-1)*pagenumber, count=0; i<totalnum && count<pagenumber; i++, count++) {
                $("result_"+i).style.display = "";
        }
}
function prepage() {
        page = parseInt($("curpage").value);
        totalpage  = parseInt($("totalpage").value);
        $("curpage").value = page-1;
        showpage(page-1, 0);
        if (page-1>1) $("prepage").innerHTML = "<a href='javascript:prepage()'><img border='0' src='images/prevPage.gif' /> </a>";
        else $("prepage").innerHTML = "<img border='0' src='images/prevPageDisabled.gif' />";
        if (page-1<totalpage) $("nextpage").innerHTML = "<a href='javascript:nextpage()'><img border='0' src='images/nextPage.gif' /> </a>";
        else $("nextpage").innerHTML = "<img border='0' src='images/nextPageDisabled.gif' />";

        if ($("report_header").style.display != "none") {
                pagenumber = parseInt($("pagenumber").value);
                totalnum = parseInt($("totalnum").value);

                for (i=(page-1)*pagenumber; i<(page)*pagenumber && i<totalnum; i++) {
                        $("report_"+i).style.display = "none";
                }

                for (i=(page-2)*pagenumber; i<(page-1)*pagenumber && i<totalnum; i++) {
                        $("report_"+i).style.display = "";
                }
                return;
        }
}

function prepage2() {
        $("p").value = parseInt($("p").value)-1;
        $("submit_form").submit();
}

function nextpage2() {
        $("p").value = parseInt($("p").value)+1;
        $("submit_form").submit();
}

function nextpage() {
        page = parseInt($("curpage").value);
        totalpage  = parseInt($("totalpage").value);
        $("curpage").value = page+1;
        showpage(page+1, 1);
        if (page+11>1) $("prepage").innerHTML = "<a href='javascript:prepage()'><img border='0' src='images/prevPage.gif' /> </a>";
        else $("prepage").innerHTML = "<img border='0' src='images/prevPageDisabled.gif' />";
        if (page+1<totalpage) $("nextpage").innerHTML = "<a href='javascript:nextpage()'><img border='0' src='images/nextPage.gif' /> </a>";
        else $("nextpage").innerHTML = "<img border='0' src='images/nextPageDisabled.gif' />";

        if ($("report_header").style.display != "none") {
                pagenumber = parseInt($("pagenumber").value);
                totalnum = parseInt($("totalnum").value);

                for (i=(page-1)*pagenumber; i<(page)*pagenumber && i<totalnum; i++) {
                        $("report_"+i).style.display = "none";
                }

                for (i=(page)*pagenumber; i<(page+1)*pagenumber && i<totalnum; i++) {
                        $("report_"+i).style.display = "";
                }
                return;
        }
}

function ReportWordNotFound() {
        var word = $('keyword').value;
        if (word == '') return;

        var url = 'ReportWordNotFound';   
        var param = "keyword="+(word);
        var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                http_request = new XMLHttpRequest();
                if (http_request.overrideMimeType) {
                        http_request.overrideMimeType('text/xml');
                        // See note below about this line
                }
        } else if (window.ActiveXObject) { // IE
                try {
                        http_request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                        try {
                                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {}
                }
        }

        if (!http_request) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
        }
        http_request.onreadystatechange = function() {
                if (http_request.readyState == 4) {
                        if (http_request.status == 200) {
                                alert(word+' Report successful');
                        }
                }
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http_request.send(param);

        return false;
}

function ReportWordError() {
        if ($("report_header").style.display == "none") {
                $("report_header").style.display = "";
                page = parseInt($("curpage").value);
                pagenumber = parseInt($("pagenumber").value);
                totalnum = parseInt($("totalnum").value);

                for (i=(page-1)*pagenumber; i<(page)*pagenumber && i<totalnum; i++) {
                        $("report_"+i).style.display = "";
                }
                return;
        }else if (report_value == -1) {
                $("report_header").style.display = "none";
                page = parseInt($("curpage").value);
                
                totalnum   = parseInt($("totalnum").value);
                for (i=(page-1)*pagenumber; i<(page)*pagenumber && i<totalnum; i++) {
                        $("report_"+i).style.display = "none";
                }
                return;
        }

        var tb = $("input_tb_"+report_value).value;
        var id = $("input_id_"+report_value).value;

        var url = 'ReportWordError';   
        var param = "tb="+(tb)+"&id="+id;
        var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                http_request = new XMLHttpRequest();
                if (http_request.overrideMimeType) {
                        http_request.overrideMimeType('text/xml');
                        // See note below about this line
                }
        } else if (window.ActiveXObject) { // IE
                try {
                        http_request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                        try {
                                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {}
                }
        }

        if (!http_request) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
        }
        http_request.onreadystatechange = function() {
                if (http_request.readyState == 4) {
                        if (http_request.status == 200) {
                                if (http_request.responseText == 1) {
                                        
                                        alert('Report successful');
                                }else {
                                        alert("Report false. Word had bean report!");
                                }
                        }
                }
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http_request.send(param);

        return false;
}

function ConfirmReportWordNotFound(id) {
        var ew = $('ew_'+id).value;
        var cw = $('cw_'+id).value;
        var tb = $('tb_'+id).value;

        if (trim(ew) == "" ||
            trim(cw) == "" ||
            tb == 0) {
                alert("Field empty");
                return;
        }

        var url = 'ReportWordNotFound/Confirm';   
        var param = "id="+id+"&ew="+(ew)+"&cw="+(cw)+"&tb="+(tb);
        var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                http_request = new XMLHttpRequest();
                if (http_request.overrideMimeType) {
                        http_request.overrideMimeType('text/xml');
                        // See note below about this line
                }
        } else if (window.ActiveXObject) { // IE
                try {
                        http_request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                        try {
                                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {}
                }
        }

        if (!http_request) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
        }
        http_request.onreadystatechange = function() {
                if (http_request.readyState == 4) {
                        if (http_request.status == 200) {
                                if (http_request.responseText == 1) {
                                        alert("Confirm successful");
                                        window.location = window.location;
                                }else {
                                        alert("Confirm error");
                                }
                        }
                }
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http_request.send(param);

}

function DeleteReportWordNotFound(id) {
        var url = 'ReportWordNotFound/Delete';   
        var param = "id="+id;
        var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                http_request = new XMLHttpRequest();
                if (http_request.overrideMimeType) {
                        http_request.overrideMimeType('text/xml');
                        // See note below about this line
                }
        } else if (window.ActiveXObject) { // IE
                try {
                        http_request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                        try {
                                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {}
                }
        }

        if (!http_request) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
        }
        http_request.onreadystatechange = function() {
                if (http_request.readyState == 4) {
                        if (http_request.status == 200) {
                                if (http_request.responseText == 1) {
                                        alert("Delete successful");
                                        window.location = window.location;
                                }else {
                                        alert("Delete error");
                                }
                        }
                }
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http_request.send(param);

}


function DeleteReportWordError(id) {
        var url = 'ReportWordError/Delete';   
        var param = "id="+id;
        var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                http_request = new XMLHttpRequest();
                if (http_request.overrideMimeType) {
                        http_request.overrideMimeType('text/xml');
                        // See note below about this line
                }
        } else if (window.ActiveXObject) { // IE
                try {
                        http_request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                        try {
                                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {}
                }
        }

        if (!http_request) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
        }
        http_request.onreadystatechange = function() {
                if (http_request.readyState == 4) {
                        if (http_request.status == 200) {
                                if (http_request.responseText == 1) {
                                        alert("Delete successful");
                                        window.location = window.location;
                                }else {
                                        alert("Delete error");
                                }
                        }
                }
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http_request.send(param);
}

function ConfirmReportWordError(id) {
        if (trim($("ew_"+id).value) == "" ||
            trim($("cw_"+id).value) == "") {
                alert("Field empty");
                return;
        }
        var url = 'ReportWordError/Confirm';   
        var param = "id="+id+"&ew="+($("ew_"+id).value)+"&cw="+($("cw_"+id).value);
        var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
                http_request = new XMLHttpRequest();
                if (http_request.overrideMimeType) {
                        http_request.overrideMimeType('text/xml');
                        // See note below about this line
                }
        } else if (window.ActiveXObject) { // IE
                try {
                        http_request = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e) {
                        try {
                                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {}
                }
        }

        if (!http_request) {
                alert('Giving up :( Cannot create an XMLHTTP instance');
                return false;
        }
        http_request.onreadystatechange = function() {
                if (http_request.readyState == 4) {
                        if (http_request.status == 200) {
                                if (http_request.responseText == 1) {
                                        alert("Confirm successful");
                                        window.location = window.location;
                                }else {
                                        alert("Confirm error");
                                }
                        }
                }
        };
        http_request.open('POST', url, true);
        http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        http_request.send(param);
}

function URLEncode(clearString) {
  var output = '';
  var x = 0;
  clearString = clearString.toString();
  var regex = /(^[a-zA-Z0-9_.]*)/;
  while (x < clearString.length) {
    var match = regex.exec(clearString.substr(x));
    if (match != null && match.length > 1 && match[1] != '') {
        output += match[1];
      x += match[1].length;
    } else {
      if (clearString[x] == ' ')
        output += '+';
      else {
        var charCode = clearString.charCodeAt(x);
        var hexVal = charCode.toString(16);
        output += '%' + ( hexVal.length < 2 ? '0' : '' ) + hexVal.toUpperCase();
      }
      x++;
    }
  }
  return output;
}


