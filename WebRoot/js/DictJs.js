
var x0 = 0, y0 = 0, x1 = 0, y1 = 0;
var offx = 6, offy = 6;
var moveable = false;
var hover = "slategray", normal = "slategray";//color;
var index = 100000;//z-index;
function startDrag(obj) {
	if (event.button == 1) {
		obj.setCapture();
		var win = obj.parentNode;
		x0 = event.clientX;
		y0 = event.clientY;
		x1 = parseInt(win.style.left);
		y1 = parseInt(win.style.top);
		normal = obj.style.backgroundColor;
		obj.style.backgroundColor = hover;
		win.style.borderColor = hover;
		var movediv = document.getElementById("movediv");
		movediv.style.display = "block";
		movediv.style.left = win.style.left;
		movediv.style.top = win.style.top;
		movediv.style.width = win.style.width;
		movediv.style.height = win.style.height;
		obj.nextSibling.style.color = hover;
		moveable = true;
	}
}
function drag(obj) {
	if (moveable) {
		var win = document.getElementById("movediv");
		win.style.left = x1 + event.clientX - x0;
		if ((y1 + event.clientY - y0) > 0 && (y1 + event.clientY - y0) < 600) {
			win.style.top = y1 + event.clientY - y0;
		}
	}
}
function stopDrag(obj) {
	if (moveable) {
		var win = obj.parentNode;
		var movediv = document.getElementById("movediv");
		win.style.left = movediv.style.left;
		win.style.top = movediv.style.top;
		movediv.style.display = "none";
		var msg = obj.nextSibling;
		win.style.borderColor = normal;
		obj.style.backgroundColor = normal;
		msg.style.color = normal;
		obj.releaseCapture();
		moveable = false;
	}
}
function getFocus(obj) {
	if (obj.style.zIndex != index) {
		index = index + 2;
		var idx = index;
		obj.style.zIndex = idx;
		//obj.nextSibling.style.zIndex = idx - 1;
	}
}
function min(obj) {
	var win = obj.parentNode.parentNode;
	var tit = obj.parentNode;
	var msg = tit.nextSibling;
	var flg = msg.style.display == "none";
	if (flg) {
		win.style.height = parseInt(msg.style.height) + parseInt(tit.style.height) + 2 * 2;
		msg.style.display = "block";
		obj.innerHTML = "0";
	} else {
		win.style.height = parseInt(tit.style.height) + 2 * 2;
		obj.innerHTML = "2";
		msg.style.display = "none";
	}
}
function cls(obj) {
	var win = obj.parentNode.parentNode;
	win.style.visibility = "hidden";
}
function ShowHide(name) {
	var xx = document.getElementById(name + "msg");
	if (xx !== null) {
		if (xx.style.visibility == "hidden") {
			xx.style.visibility = "visible";
		} else {
			if (xx.style.visibility == "visible") {
				xx.style.visibility = "hidden";
			}
		}
	}
}
function xWin(id, name, w, h, l, t, tit, msg) {
	index = index + 2;
	this.id = id;
	this.name = name;
	this.width = w;
	this.height = h;
	this.left = l;
	this.top = t;
	this.zIndex = index;
	this.title = tit;
	this.message = msg;
	this.obj = null;
	this.bulid = bulid;
	this.bulid();
	document.getElementById(this.name + "msg").style.visibility = "hidden";
}
function bulid() {
	var str = "";
	str = str + "<div id='" + this.name + "msg'" + this.id + " " + "style='" + "z-index:" + this.zIndex + ";" + "width:" + this.width + ";" + "height:" + this.height + ";" + "left:" + this.left + ";" + "top:" + this.top + ";" + "background-color:" + normal + ";" + "color:" + normal + ";" + "font-size:11px;" + "font-family:Verdana;" + "position:absolute;" + "border:2px solid " + normal + "' " + "onmousedown='getFocus(this)'>";
	str = str + "<div " + "style='" + "background-color:" + normal + ";" + "width:" + (this.width - 2 * 2) + ";" + "height:20;" + "color:white" + "' " + "onmousedown='startDrag(this)' " + "onmouseup='stopDrag(this)' " + "onmousemove='drag(this)' " + "ondblclick='min(this.childNodes[1])'" + ">";
	str = str + "<span style='width:" + (this.width - 2 * 14 - 4) + ";padding-left:3px;cursor:move'>" + this.title + "</span>";
	str = str + "<span style='width:14;border-width:0px;color:white;font-family:webdings;cursor:default' onclick='min(this)'>0</span>";
	str = str + "<span style='width:14;border-width:0px;color:white;font-family:webdings;cursor:default' onclick='cls(this)'>r</span>";
	str = str + "</div>";
	str = str + "<div style='" + "width:100%;" + "height:" + (this.height - 20 - 4) + ";" + "background-color:white;" + "line-height:14px;" + "word-break:break-all;" + "padding:3px;" + "'>" + this.message;
	str = str + "</div>";
	str = str + "</div>";
	document.getElementById(this.name).innerHTML = str;
}

