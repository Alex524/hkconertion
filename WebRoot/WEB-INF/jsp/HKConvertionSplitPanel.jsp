<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
	<form action="HKConvertionSplitSubmit.do" method="post" name="PdfEditConditionForm">
		<table width="100%" cellspacing="5" cellpadding="15" border="0">
			<tr>
				<td class="MenuTable">
					<font size="3"><b>Split PDF</b> </font>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<td align="right">
					<font size="2" style="color:#f00000;">File Path:E:\Work Station HK\HKConvertion\pdf</font>
				</td>
			</tr>
		</table>
		<br />
		<table width="100%" cellspacing="0" cellpadding="0" border="1">
			<tr>
				<td width="50%" class="Bluewordbold12thsize" id="pdfNumber">
					<% String getPdfName=request.getAttribute("getPdfName").toString();
						if(!getPdfName.equals("no")){
					%>
						<embed id=e src="/HKConvertionPdf/pdf/<%=getPdfName%>.pdf" width="100%" height="800" type="application/pdf" />
					<%
						}
					 %>
						
				</td>
				<td width="50%" valign="top" class="Bluewordbold12thsize">
					<table width="100%" cellspacing="5" cellpadding="15" border="1">
						<tr>
							<td class="MenuTable">
								PDF List
							</td>
							<td>
								<select name="listEncryptPDF" id="listEncryptPDF" style="width:140px;" onchange="getPdf()">
									<option value="select">
											select
									</option>
									<logic:iterate id="list" name="listPDF" type="com.lighthouse.translation.hkconvertion.beans.FtpBean">
										<logic:equal value="<%=getPdfName%>" name="list" property="ftpNumber">
												<option value='<bean:write name="list" property="ftpNumber" />' id="number" name="number" selected>
										</logic:equal>
										<logic:notEqual value="<%=getPdfName%>" name="list" property="ftpNumber">
												<option value='<bean:write name="list" property="ftpNumber" />' id="number" name="number">
										</logic:notEqual>
											<bean:write name="list" property="ftpNumber"/>
											</option>
									</logic:iterate>
								</select>
							</td>
							<td class="MenuTable">
								
							</td>
						</tr>
					</table>
					<table width="100%" cellspacing="5" cellpadding="15" border="1">
						<tr>
							<td rowspan="2" width="20%" class="MenuTable">
								Applications
							</td>
							<td width="40%" class="MenuTable">
								StartPage
							</td>
							<td width="40%" class="MenuTable">
								EndPage
							</td>
							
						</tr>
						<tr>
							<td>
								<input type="text" id="startPage_applications" name="startPage_applications" size="8" />
							</td>
							<td>
								<input type="text" id="endPage_applications" name="endPage_applications" size="8" />
							</td>
						</tr>
						<tr>
							<td rowspan="2" class="MenuTable">
								Grants
							</td>
							<td class="MenuTable">
								StartPage
							</td>
							<td class="MenuTable">
								EndPage
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" id="startPage_grants" name="startPage_grants" size="8" />
							</td>
							<td>
								<input type="text" id="endPage_grants" name="endPage_grants" size="8" />
							</td>
						</tr>
						<tr>
							<td rowspan="2" class="MenuTable">
								Short-term Patents
							</td>
							<td class="MenuTable">
								StartPage
							</td>
							<td class="MenuTable">
								EndPage
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" id="startPage_shortGrants" name="startPage_shortGrants" size="8" />
							</td>
							<td>
								<input type="text" id="endPage_shortGrants" name="endPage_shortGrants" size="8" />
							</td>
						</tr>
					</table>
					<table width="100%">
						<tr>
							<td colspan="4" align="center">
								<input id="sub" type="button" value="Submit" class="inputbutton" onclick="validate()" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		
		
		
		
		
		
		
		

	</form>
</body>
</html:html>

<script type="text/javascript">
    function getPdf(){
    var selected=document.all.listEncryptPDF.value;
    document.PdfEditConditionForm.action="PdfSelect.do?pdfName="+selected;
    document.PdfEditConditionForm.submit();
    }
	
    
  function validate(){
		var value_succ = false;
		var startPageApplicationsValue = document.getElementById("startPage_applications").value;
		var endPageApplicationsValue = document.getElementById("endPage_applications").value;
		var startPageGrantsValue = document.getElementById("startPage_grants").value;
		var endPageGrantsValue = document.getElementById("endPage_grants").value;
		var startPageShortGrantsValue = document.getElementById("startPage_shortGrants").value;
		var endPageShortGrantsValue = document.getElementById("endPage_shortGrants").value;
		if (startPageApplicationsValue == "" || endPageApplicationsValue == "" || 
		    startPageGrantsValue == "" || endPageGrantsValue == "" ||
		    startPageShortGrantsValue == "" || endPageShortGrantsValue == "") {
		   value_succ = true;
		}
		
		if(value_succ){
			alert("no");
			return false;
		}else{
			document.PdfEditConditionForm.submit();
		}
		
	}
	
	
</script>