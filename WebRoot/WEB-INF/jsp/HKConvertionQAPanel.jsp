<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body bottommargin="0" topmargin="0" rightmargin="0" leftmargin="0"
	 class="HomeBody">
	<table width="100%" height="100%" cellspacing="0" cellpadding="0">
		<tr height="100%">
			<td width="15%" valign="top" bgcolor="#E8E9E2">
				<ul class="sidenavi"><li><b>Welcome :&nbsp;&nbsp;<bean:write name="userBean" property="userName" /></b>
					</li><li><a href="Logout.do">Log out</a></li><li><b>XML QA</b></li><li>
						<a href="HKConvertionQAPanel.do?lockPubNumber=N" target="contentFrame">01)XML
							QA </a></li></ul>
			</td>
			<td width="85%">
				<iframe width="100%" height="100%" src="" name="contentFrame"
					id="contentFrame">
				</iframe>
			</td>
		</tr>
	</table>
</body>
</html:html>
