
<%@page import="java.util.List"%>
<%@page import="com.lighthouse.translation.hkconvertion.beans.CountBean"%><%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<script type="text/javascript">
	function check(){
		var patent = document.getElementById("patent").value;
		if(patent==""){
			alert("please choose the patent!");
			return false;
		}else{
			document.myForm.submit();
		}
	}
</script>
<body bottommargin="0" topmargin="0" rightmargin="0" leftmargin="0"
	class="HomeBody">
	
	<form action="HKConvertionCount.do" method="post" name="myForm">
		<table width="100%">
			<tr align="center">
				<td><select name="patent" id="patent">
				<option></option>
				<% List listTime = (List)request.getAttribute("listTime"); 
					for(int i=0;i<listTime.size();i++){
				%>
				<option value="<%=listTime.get(i) %>"><%=listTime.get(i) %></option>
				<%} %>
				</select></td>
				<td><input type="button" name="bu" onclick="check()" value="submit"/></td>
			</tr>
			<tr>
				<td><font color="green">allDocCount:</font><%=(String)request.getAttribute("allDocCount") %></td>
				<td><font color="green">trDocCount(need TR):</font><%=(String)request.getAttribute("trDocCount") %></td>
				<td><font color="green">qaDocCount(need QA):</font><%=(String)request.getAttribute("qaDocCount") %></td>
			</tr>
		</table>
		<table width="100%" height="100%" border="1">
			<tr>
				<td>
					<div align="left" width="50%"  height="50%">
						<tr>
							<td  align="center"><font color="red">A2TRuserId</font></td><td  align="center" ><font color="red">A2TRcount</font></td>
						</tr>
						<% List list1 = (List)request.getAttribute("list1"); 
						   for(int i=0;i<list1.size();i++){
						   CountBean cb = (CountBean)list1.get(i);
						%>
						<tr>
							<td align="center"><%=cb.getId() %></td><td align="center"><%=cb.getCount() %></td>
						</tr>
						<%} %>
					</div> 
				</td>
				<td>
					<div align="left" width="50%"  height="50%">
						<tr>
							<td  align="center"><font color="red">A2QAuserId</font></td><td  align="center"><font color="red">A2QAcount</font></td>
						</tr>
						<% List list2 = (List)request.getAttribute("list2"); 
						   for(int j=0;j<list2.size();j++){
						   CountBean cb = (CountBean)list2.get(j);
						%>
						<tr>
							<td align="center"><%=cb.getId() %></td><td align="center"><%=cb.getCount() %></td>
						</tr>
						<%} %>
					</div>
				</td>	
			</tr>
			<tr></tr>
			<tr>
				<td>
					<div align="left" width="50%"  height="50%">
						<tr>
							<td align="center"><font color="red">OtherTRuserId</font></td><td  align="center"><font color="red">OtherTRcount</font></td>
						</tr>
						<% List list3 = (List)request.getAttribute("list3"); 
						   for(int k=0;k<list3.size();k++){
						   CountBean cb = (CountBean)list3.get(k);
						%>
						<tr>
							<td align="center"><%=cb.getId() %></td><td align="center"><%=cb.getCount() %></td>
						</tr>
						<%} %>
					</div> 
				</td>
				<td>
					<div align="left" width="50%"  height="50%">
						<tr>
							<td  align="center"><font color="red">OtherQAuserId</font></td><td  align="center"><font color="red">OtherQAcount</font></td>
						</tr>
						<% List list4 = (List)request.getAttribute("list4"); 
						   for(int l=0;l<list4.size();l++){
						   CountBean cb = (CountBean)list4.get(l);
						%>
						<tr>
							<td align="center"><%=cb.getId() %></td><td align="center"><%=cb.getCount() %></td>
						</tr>
						<%} %>
					</div>
				</td>	
			</tr>
		</table>
	</form>
	
</body>
</html>
