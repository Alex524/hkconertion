<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body bottommargin="0" topmargin="0" rightmargin="0" leftmargin="0"
	class="HomeBody">
	
	<form action="HKConvertionAddUser.do" method="post">
		<table width="100%" cellspacing="5" cellpadding="15">
			<tr>
				<td class="MenuTable">
					<font size="3"><b>Add New User</b> </font>
				</td>
			</tr>
		</table>
		<table width="50%" cellspacing="5" cellpadding="10" border="1">
			<tr>
				<td align=right width="15%" class="Bluewordbold12thsize">
					User Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td align=left width="15%" class="Bluewordbold12thsize">
					<input type="text" name="userName" id="userName" />
				</td>
			</tr>
			<tr>
				<td align=right width="15%" class="Bluewordbold12thsize">
					Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td align=left width="15%" class="Bluewordbold12thsize">

					<input type="password" name="password" id="password" />
				</td>
			</tr>
			<tr>
				<td align=right width="15%" class="Bluewordbold12thsize">
					Country:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td align=left width="15%" class="Bluewordbold12thsize">
					<lscd:CountryTags name="country"></lscd:CountryTags>
				</td>
			</tr>
			<tr>
				<td align=right width="15%" class="Bluewordbold12thsize">
					UserGroup:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td align=left width="15%" class="Bluewordbold12thsize">
					<select name="usergroup" id="usergroup">
						<option value="PdfEditor">
							editor
						</option>
						<option value="QA">
							QA
						</option>	
					</select>
				</td>
			</tr>
			<tr>
				<td align=right width="15%" class="Bluewordbold12thsize">
					<input type="Submit" name="sbbutton" value="Submit"
						class="inputButton">
					&nbsp;&nbsp;&nbsp;
				</td>
				<td align=left width="15%" class="Bluewordbold12thsize">
					&nbsp;&nbsp;&nbsp;
					<input type="reset" name="rebutton" value="Reset"
						class="inputButton">
				</td>
			</tr>

		</table>
	</form>
	
</body>
</html>
