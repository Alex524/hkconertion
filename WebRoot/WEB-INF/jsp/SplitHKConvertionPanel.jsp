<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
	<form action="HKConvertionSplitSubmit.do" method="post" name="PdfEditConditionForm">
		<table width="100%" cellspacing="5" cellpadding="15" border="0">
			<tr>
				<td class="MenuTable">
					<font size="3"><b>Split PDF</b> </font>
				</td>
			</tr>
		</table>
		<br />
		
		<table width="100%" cellspacing="0" cellpadding="0" border="1">
			<tr>
				<td width="50%" class="Bluewordbold12thsize" id="pdfNumber">
					<% String getPdfName=request.getAttribute("getPdfName").toString();
						if(!getPdfName.equals("no")){
					%>
						<!-- <embed id=e src="/PdfConvertTxt/pdf/<%=getPdfName%>.pdf" width="100%" height="800" type="application/pdf" /> -->
					<%
						}
					 %>
						
				</td>
				<td width="50%" valign="top" class="Bluewordbold12thsize">
					<table width="100%" cellspacing="5" cellpadding="15" border="1">
						<tr>
							<td class="MenuTable">
								PDF List
							</td>
							<td>
								<select name="listEncryptPDF" id="listEncryptPDF" style="width:140px;" onchange="getPdf()">
									<option value="select">
											select
									</option>
									<logic:iterate id="list" name="listPDF" type="com.lighthouse.translation.hkconvertion.beans.FtpBean">
										<logic:equal value="<%=getPdfName%>" name="list" property="ftpNumber">
												<option value='<bean:write name="list" property="ftpNumber" />' id="number" name="number" selected>
										</logic:equal>
										<logic:notEqual value="<%=getPdfName%>" name="list" property="ftpNumber">
												<option value='<bean:write name="list" property="ftpNumber" />' id="number" name="number">
										</logic:notEqual>
											<bean:write name="list" property="ftpNumber"/>
											</option>
									</logic:iterate>
								</select>
							</td>
							<td class="MenuTable">
								
							</td>
						</tr>
					</table>
					<table width="100%" cellspacing="5" cellpadding="15" border="1">
						<tr>
							<td rowspan="2" class="MenuTable">
								applications
							</td>
							<td class="MenuTable">
								StartPage
							</td>
							<td class="MenuTable">
								StartNumber
							</td>
							<td class="MenuTable">
								EndPage
							</td>
							<td class="MenuTable">
								EndNumber
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" id="startPage_applications" name="startPage_applications" size="8" />
							</td>
							<td>
								<input type="text" id="startNumber_applications" name="startNumber_applications" size="15" />
							</td>
							<td>
								<input type="text" id="endPage_applications" name="endPage_applications" size="8" />
							</td>
							<td>
								<input type="text" id="endNumber_applications" name="endNumber_applications" size="15" />
							</td>
						</tr>
						<tr>
							<td rowspan="2" class="MenuTable">
								grants
							</td>
							<td class="MenuTable">
								StartPage
							</td>
							<td class="MenuTable">
								StartNumber
							</td>
							<td class="MenuTable">
								EndPage
							</td>
							<td class="MenuTable">
								EndNumber
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" id="startPage_grants" name="startPage_grants" size="8" />
							</td>
							<td>
								<input type="text" id="startNumber_grants" name="startNumber_grants" size="15" />
							</td>
							<td>
								<input type="text" id="endPage_grants" name="endPage_grants" size="8" />
							</td>
							<td>
								<input type="text" id="endNumber_grants" name="endNumber_grants" size="15" />
							</td>
						</tr>
						<tr>
							<td rowspan="2" class="MenuTable">
								short grants
							</td>
							<td class="MenuTable">
								StartPage
							</td>
							<td class="MenuTable">
								StartNumber
							</td>
							<td class="MenuTable">
								EndPage
							</td>
							<td class="MenuTable">
								EndNumber
							</td>
						</tr>
						<tr>
							<td>
								<input type="text" id="startPage_shortGrants" name="startPage_shortGrants" size="8" />
							</td>
							<td>
								<input type="text" id="startNumber_shortGrants" name="startNumber_shortGrants" size="15" />
							</td>
							<td>
								<input type="text" id="endPage_shortGrants" name="endPage_shortGrants" size="8" />
							</td>
							<td>
								<input type="text" id="endNumber_shortGrants" name="endNumber_shortGrants" size="15" />
							</td>
						</tr>
					</table>
					<table width="100%">
						<tr>
							<td colspan="4" align="center">
								<input id="sub" type="button" value="Submit" class="inputbutton" onclick="validate()" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		
		
		
		
		
		
		
		

	</form>
</body>
</html:html>

<script type="text/javascript">
    function getPdf(){
    var selected=document.all.listEncryptPDF.value;
    document.XmlEditConditionForm.action="PdfSplitSelect.do?pdfName="+selected;
    document.XmlEditConditionForm.submit();
    }
	
    
  function validate(){
		var value_succ = false;
		var startPageApplicationsValue = document.getElementById("startPage_applications").value;
		var endPageApplicationsValue = document.getElementById("endPage_applications").value;
		var startNumberApplicationsValue = document.getElementById("startNumber_applications").value;
		var endNumberApplicationsValue = document.getElementById("endNumber_applications").value;
		var startPageGrantsValue = document.getElementById("startPage_grants").value;
		var endPageGrantsValue = document.getElementById("endPage_grants").value;
		var startNumberGrantsValue = document.getElementById("startNumber_grants").value;
		var endNumberGrantsValue = document.getElementById("endNumber_grants").value;
		var startPageShortGrantsValue = document.getElementById("startPage_shortGrants").value;
		var endPageShortGrantsValue = document.getElementById("endPage_shortGrants").value;
		var startNumberShortGrantsValue = document.getElementById("startNumber_shortGrants").value;
		var endNumberShortGrantsValue = document.getElementById("endNumber_shortGrants").value;
		if (startPageApplicationsValue == "" || endPageApplicationsValue == "" || startNumberApplicationsValue == "" || endNumberApplicationsValue == "" || 
		    startPageGrantsValue == "" || endPageGrantsValue == "" || startNumberGrantsValue == "" || endNumberGrantsValue == "" ||
		    startPageShortGrantsValue == "" || endPageShortGrantsValue == "" || startNumberShortGrantsValue == "" || endNumberShortGrantsValue == "") {
		   value_succ = true;
		}
		
		if(value_succ){
			alert("no");
			return false;
		}else{
			document.PdfEditConditionForm.submit();
		}
		
	}
	
	
</script>