<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
	<form action="HKConvertionSplitTXTSubmit.do" method="post" name="PdfEditConditionForm">
		<table width="100%" cellspacing="5" cellpadding="15" border="0">
			<tr>
				<td class="MenuTable">
					<font size="3"><b>Split TXT</b> </font>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<td align="right">
					<font size="2" style="color:#f00000;">File Path:E:\Work Station HK\HKConvertion\txt </font>
				</td>
			</tr>
		</table>
		<br />
		<table width="100%" cellspacing="0" cellpadding="0" border="1">
			<tr>
				<td width="100%" valign="top" class="Bluewordbold12thsize">
					<table width="100%" cellspacing="5" cellpadding="15" border="1">
						<tr>
							<td class="MenuTable">
							<% String getPdfName=request.getAttribute("getPdfName").toString(); %>
								TXT List
							</td>
							<td>
								<select name="listEncryptPDF" id="listEncryptPDF" style="width:140px;">
									<option value="select">
											select
									</option>
									<logic:iterate id="list" name="listPDF" type="com.lighthouse.translation.hkconvertion.beans.FtpBean">
										<logic:equal value="<%=getPdfName%>" name="list" property="ftpNumber">
												<option value='<bean:write name="list" property="ftpNumber" />' id="number" name="number" selected>
										</logic:equal>
										<logic:notEqual value="<%=getPdfName%>" name="list" property="ftpNumber">
												<option value='<bean:write name="list" property="ftpNumber" />' id="number" name="number">
										</logic:notEqual>
											<bean:write name="list" property="ftpNumber"/>
											</option>
									</logic:iterate>
								</select>
							</td>
							<td class="MenuTable">
								<input id="sub" type="Submit" value="Submit" class="inputbutton" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
</html:html>

<script type="text/javascript">
    function getPdf(){
    var selected=document.all.listEncryptPDF.value;
    document.PdfEditConditionForm.action="PdfSelect.do?pdfName="+selected;
    document.PdfEditConditionForm.submit();
    }
	
    
  
	
	
</script>