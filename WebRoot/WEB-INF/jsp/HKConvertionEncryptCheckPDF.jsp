<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
	<form action="XmlSplitCheckPdf.do" method="post" name="XmlEditConditionForm">
		<table width="100%" cellspacing="5" cellpadding="15" border="0">
			<tr>
				<td class="MenuTable">
					<font size="3"><b>Split Encrypt Check PDF</b> </font>
				</td>
			</tr>
		</table>
		<br />
		<table width="100%" cellspacing="5" cellpadding="15" border="1">
			<tr>
				<td width="70%">
					
					<input type="hidden" name="pdfNumber" value="<%=request.getAttribute("pdfNumber").toString()%>" id="pdfNumber" />
					<input type="hidden" name="flag" value="<%=request.getAttribute("flag").toString()%>" id="flag" />
					
				<embed id=e src="/xmlEditorPdf/pdf/VN/sourcePDF/<%=request.getAttribute("pdfNumber").toString()%>.pdf" width="100%" height="1000" type="application/pdf" />
				</td>
				<td width="30%" valign="top">
				<logic:iterate id="check" name="checkList" type="com.lighthouse.translation.xmleditor.beans.CheckFtpBean">
				
					<input type="hidden" name="startNumber" value="<bean:write name="check" property="startNumber"/>" id="startNumber" />
					<input type="hidden" name="endNumber" value="<bean:write name="check" property="endNumber"/>" id="endNumber" />
					<input type="hidden" name="startNumberStr" value="<bean:write name="check" property="startNumberStr"/>" id="startNumberStr" />
					<input type="hidden" name="endNumberStr" value="<bean:write name="check" property="endNumberStr"/>" id="endNumberStr" />
					<input type="hidden" name="startPage" value="<bean:write name="check" property="startPage"/>" id="startPage" />
					<input type="hidden" name="endPage" value="<bean:write name="check" property="endPage"/>" id="endPage" />
					<table width="100%" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td width="70%" align="left" class="Bluewordbold12thsize">
								startNumber:<bean:write name="check" property="startNumber"/> to endNumber:<bean:write name="check" property="endNumber"/> is error!
							</td>
							<td width="30%" align="right" class="Bluewordbold12thsize">
								<input type="button" id="addEnc" name="addEnc" value="Next Mapping" onclick="addENC()" />
							</td>
						</tr>
						<%
							int encrypt_number = 0; 
						%>
						<tr>
							<td colspan="2">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td>Number</td>
										<td>StartPage</td>
										<td>EndPage</td>
									</tr>
									<tr>
										<td width="60%">
											<input type="text" id="number_<%=encrypt_number%>" name="number_<%=encrypt_number%>" size="20" />
										</td>
										<td width="20%">
											<input type="text" id="startPage_<%=encrypt_number%>" name="startPage_<%=encrypt_number%>" size="10" />
										</td>
										<td width="20%">
											<input type="text" id="endPage_<%=encrypt_number%>" name="endPage_<%=encrypt_number%>" size="10" />
										</td>
									</tr>
									
								</table>
								
							</td>
						</tr>
						<%
									encrypt_number++;
						%>
						<tr>
							<td align="left" class="Bluewordbold12thsize" width="100%" height="50%" id="encryptNumber" style="color:red;" colspan="2">
								<input type="hidden" name="encrypt_number" value="<%=encrypt_number%>" id="encrypt_number" />
							</td>
						</tr>
					</table>
					</logic:iterate>
					
					
					
					
					<table>
						<tr>
							<td align="center"><input id="sub" type="button" value="Submit" class="inputbutton" onclick="validate()" /></td>
						</tr>
					</table>
					
					
					
				</td>
			</tr>
		</table>

	</form>
</body>
</html:html>

<script type="text/javascript">
	var encrypt_number = parseInt(document.getElementById("encrypt_number").value);
	function addENC(s){
		var i = parseInt(s);
		var tdObj =document.getElementById("encryptNumber");
		tdObj.innerHTML = tdObj.innerHTML + ("<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td>Number</td><td>StartPage</td><td>EndPage</td></tr><tr><td width='60%'><input type='text' id='number_"+encrypt_number+"' name='number_"+encrypt_number+"' size='20' /></td><td width='20%'><input type='text' id='startPage_"+encrypt_number+"' name='startPage_"+encrypt_number+"' size='10' /></td><td width='20%'><input type='text' id='endPage_"+encrypt_number+"' name='endPage_"+encrypt_number+"' size='10' /></td></tr></table>");
		encrypt_number++;
	}
	
	function validate(){
		
		var flag = document.getElementById("flag").value;
		var startNumber = parseInt(document.getElementById("startNumberStr").value);
		var endNumber = parseInt(document.getElementById("endNumberStr").value);
		var startPage = parseInt(document.getElementById("startPage").value);
		var endPage = parseInt(document.getElementById("endPage").value);
		var pageInt = 0;
		var pageCount = 0;
		var numberCount = 0;
		var numberInt = 0;
		for(i=0;i<encrypt_number;i++){
			var startPageInt = parseInt(document.getElementById("startPage_"+i).value);
			var endPageInt = parseInt(document.getElementById("endPage_"+i).value);
			pageInt += endPageInt-startPageInt+1;
			numberInt++;
		}
		
		pageCount = endPage-startPage+1;
		numberCount = endNumber-startNumber+1-numberInt+pageInt;
		if(numberCount==pageCount){
			if(confirm("Are you sure?")){
				document.getElementById("encrypt_number").value=encrypt_number;
				document.XmlEditConditionForm.submit();
			}
		}else{
			if(confirm("count is not. Are you sure?")){
				document.getElementById("encrypt_number").value=encrypt_number;
				document.XmlEditConditionForm.submit();
			}
		}
		
	}
</script>