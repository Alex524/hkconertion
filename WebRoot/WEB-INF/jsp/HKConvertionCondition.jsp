<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%
		//List listRu = (List)request.getAttribute("listRu");
		//List listTh = (List)request.getAttribute("listTh");
	 %>
	<script type="text/javascript">
		function setDate(ele){
			var obj = document.getElementById(ele);
			if(obj != null){
				obj.value = showModalDialog("calendar.htm", "yyyy-mm-dd" ,"dialogWidth:286px;dialogHeight:221px;status:no;help:no;");
			}
		}
		
		function sub(){
			return false;
		}
		
		function disable(obj){
			if(obj=="QA"||obj=="QATE"||obj=="CT"){
				document.getElementsByName("userGroup")[0].disabled = true;
			}else{
			    document.getElementsByName("userGroup")[0].disabled = false;
			}
			
			if(obj=="QATE"){
				document.getElementById("sub").disabled = true;
			}else{
				document.getElementById("sub").disabled = false;
			}
		}
	
		
	var xmlHttp;   
    var result;   
    function createXMLHttpRequest(){   
        if(window.ActiveXObject){   
            try{   
                xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");   
            }catch(e1){   
                try{   
                    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");   
                }catch(e2){   
                    alert("error");   
                }   
            }   
        }else if(window.XMLHttpRequest){   
            xmlHttp=new XMLHttpRequest();   
        }   
    }   
    
    function getCountry(){   
        var selected=document.all.country.value;
           
        if(selected==""){   
        
            while(document.all.existNumberList.options.length>0){
               
                document.all.existNumberList.removeChild(document.all.existNumberList.childNodes[0]);
                   
       		}   
       		
       		while(document.all.notExistNumberList.options.length>0){
               
                document.all.notExistNumberList.removeChild(document.all.notExistNumberList.childNodes[0]);
                   
       		}  
       		
       		while(document.all.reopenExistNumberList.options.length>0){
               
                document.all.reopenExistNumberList.removeChild(document.all.reopenExistNumberList.childNodes[0]);
                   
       		}  
       		
            var option=document.createElement("option");   
            var defaults=document.all.defaults.value;   
            option.text=defaults.split(",")[0];   
            option.value=defaults.split(",")[1];   
            document.all.number.options.add(option);   
            return; 
              
        } 
          
       var ps = "countryS="+document.getElementById("country").value;
       ps = encodeURI(ps);
 	   ps = encodeURI(ps);
       createXMLHttpRequest();   
       xmlHttp.open("POST", "XmlConditionSelect.do", true);  
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
       xmlHttp.onreadystatechange=process;  
       xmlHttp.send(ps);
    }
       
    function process(){   
    
        if(xmlHttp.readyState==4){ 
          
            if(xmlHttp.status==200){   
           
                result=xmlHttp.responseText;   
                
                while(document.all.existNumberList.options.length>0){  
                 
                    document.all.existNumberList.removeChild(document.all.existNumberList.childNodes[0]);
                }
                
                while(document.all.notExistNumberList.options.length>0){  
                 
                    document.all.notExistNumberList.removeChild(document.all.notExistNumberList.childNodes[0]);
                }
                
                while(document.all.reopenExistNumberList.options.length>0){  
                 
                    document.all.reopenExistNumberList.removeChild(document.all.reopenExistNumberList.childNodes[0]);
                }
                
                var results=result.split("/");
                var resultsExist = result.split("/")[0].split(",");
                var resultsNotExist = result.split("/")[1].split(",");
                var resultsReopenExist = result.split("/")[2].split(",");
                var pubNumber = result.split("/")[3].split(",")[0];
                
                for(var i=0;i<resultsExist.length;i++){
                	var option=document.createElement("option");
                	option.text=resultsExist[i];   
                	option.value=resultsExist[i]; 
                	document.all.existNumberList.options.add(option); 
                	document.all.existNumberList.options.disabled = false;
                	document.all.exist.checked = true;
               	}
               	
               	for(var i=0;i<resultsNotExist.length;i++){
                	var option=document.createElement("option");
                	option.text=resultsNotExist[i];   
                	option.value=resultsNotExist[i];
                	document.all.notExistNumberList.options.add(option);   
               	}
               	
               	for(var i=0;i<resultsReopenExist.length;i++){
                	var option=document.createElement("option");
                	option.text=resultsReopenExist[i];   
                	option.value=resultsReopenExist[i];
                	document.all.reopenExistNumberList.options.add(option);   
               	}
               	
               	document.all.existNumberList.removeChild(document.all.existNumberList.childNodes[document.all.existNumberList.options.length-1]);
               	document.all.notExistNumberList.removeChild(document.all.notExistNumberList.childNodes[document.all.notExistNumberList.options.length-1]);
               	document.all.reopenExistNumberList.removeChild(document.all.reopenExistNumberList.childNodes[document.all.reopenExistNumberList.options.length-1]);
               	
                if(resultsReopenExist.length==1){
                	document.all.reopenExist.disabled = true;
                	document.all.reopenExistNumberList.disabled = true;
                	document.all.reopenExistNumberList.style.width = "100px";
                } else {
                	document.all.reopenExist.disabled = false;
                	document.all.reopenExistNumberList.disabled = false;
                }
                
                if(resultsNotExist.length==1){
                	document.all.notExist.disabled = true;
                	document.all.notExistNumberList.disabled = true;
                	document.all.notExistNumberList.style.width = "100px";
                } else {
                	document.all.notExist.disabled = false;
                	document.all.notExistNumberList.disabled = false;
                	document.all.reopenExistNumberList.disabled = true;
                }
                
                if(resultsExist.length==1){
                	document.all.exist.disabled = true;
                	document.all.existNumberList.disabled = true;
                	document.all.existNumberList.style.width = "100px";
                } else {
                	document.all.exist.disabled = false;
                	document.all.existNumberList.disabled = false;
                	document.all.notExistNumberList.disabled = true;
                	document.all.reopenExistNumberList.disabled = true;
                }
                
                if (pubNumber != ""){
                	document.getElementById("lockNumber").style.display="";
                	document.getElementById("existNumber").innerHTML=pubNumber;
                	document.getElementById("notExist").disabled = true;
                	document.getElementById("notExistNumberList").disabled = true;
               	} else {
               		document.getElementById("lockNumber").style.display="none";
               	}
               	
               	document.getElementById("lockClear").style.display="none";
            }
               
        }   
    }
    
    function checkFileExist(){
    	
       var ps = "method=init&countryS=file&filename="+document.getElementById("number").value;
       ps = encodeURI(ps);
 	   ps = encodeURI(ps);
       createXMLHttpRequest();   
       xmlHttp.open("POST", "XmlEdit.do", true);  
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
       xmlHttp.onreadystatechange=processFile;  
       xmlHttp.send(ps);
       
    }
    
    function processFile(){   
    
       if(xmlHttp.readyState==4){ 
          
           if(xmlHttp.status==200){   
           
               result=xmlHttp.responseText;
               alert(result);
                 
           }
               
       }
        	   
    } 
    
    function disable(obj){
    
			if(obj=="exist"){
				document.getElementById("existNumberList").disabled = false;
				document.getElementById("notExistNumberList").disabled = true;
				document.getElementById("reopenExistNumberList").disabled = true;
			}
			
			if(obj=="notExist"){
				document.getElementById("notExistNumberList").disabled = false;
				document.getElementById("existNumberList").disabled = true;
				document.getElementById("reopenExistNumberList").disabled = true;
			}
			
			if(obj=="reopenExist"){
				document.getElementById("reopenExistNumberList").disabled = false;
				document.getElementById("existNumberList").disabled = true;
				document.getElementById("notExistNumberList").disabled = true;
			}
			
		}
	
	function validate(){
		if (document.getElementById("notExistNumberList").disabled == true && document.getElementById("existNumberList").disabled == true && document.getElementById("reopenExistNumberList").disabled == true || document.getElementById("country").value=="select"){
			alert("not pdf");
			return false;
		} else {
			document.XmlEditConditionForm.submit();
		}
	}
	
	function getlock(){
		document.XmlEditConditionForm.action="XmlCondition.do?lockExistNumber="+document.getElementById("existNumber").innerHTML;
		document.XmlEditConditionForm.submit();
	}  
	</script>
</head>

<body>
	<form action="XmlCondition.do?lockExistNumber=null" method="post" name="XmlEditConditionForm">
		<table width="100%" cellspacing="5" cellpadding="15" border="0">
			<tr>
				<td class="MenuTable">
					<font size="3"><b>OPEN XML FILE</b> </font>
				</td>
			</tr>
			<tr>
				<td>
					<table border="1" cellpadding="5" cellspacing="5" width="20%">
						<tr>
							<td class="MenuTable">
								Country
							</td>
							<td colspan="2">
								<select name="country" id="country" onchange="getCountry()">
										<option value="select">
											select
										</option>
										<option value="RU">
											RU
										</option>
										<option value="TH">
											TH
										</option>
										<option value="VN">
											VN
										</option>
										<option value="MY">
											MY
										</option>
									
								</select>
							</td>
						</tr>
						
						
						
						<tr>
							<td class="MenuTable">
								<input name="kind" id="exist" value="exist" type="radio" onclick="disable(this.value)" />
							</td>
							<td colspan="2">
								Update Existing XML
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<select name="number" id="existNumberList" style="width:140px;" disabled>
									select
								</select>
							</td>
						</tr>
						<tr>
							<td class="MenuTable">
								<input name="kind" id="notExist" value="notExist" type="radio" onclick="disable(this.value)" />
							</td>
							<td colspan="2">
								Create New XML
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<select name="number" id="notExistNumberList" style="width:140px;" disabled>
									select
								</select>
								<input type="hidden" name="defaults" value="select,select"> 
							</td>
						</tr>
						
						<tr>
							<td class="MenuTable">
								<input name="kind" id="reopenExist" value="reopenExist" type="radio" onclick="disable(this.value)" />
							</td>
							<td colspan="2">
								Reopened XML
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<select name="number" id="reopenExistNumberList" style="width:140px;" disabled>
									select
								</select>
							</td>
						</tr>
						
						
						
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<div id="lockClear">
					<logic:notEmpty name="lockPubNumber">
						<table>
							<tr>
								<td><span style="color:#f00000">lock number:</span><%=request.getAttribute("lockPubNumber").toString() %></td>
							</tr>
						</table>
					</logic:notEmpty>
					</div>
					<div id="lockNumber" style="display:none;">
						<table>
							<tr>
								<td><span style="color:#f00000">exist number:</span></td>
								<td><div id="existNumber" onclick="getlock()" style="cursor:hand;" /></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<input id="sub" type="button" value="Submit" class="inputbutton" onclick="validate()" />
				</td>
			</tr>
		</table>

	</form>
</body>
</html:html>