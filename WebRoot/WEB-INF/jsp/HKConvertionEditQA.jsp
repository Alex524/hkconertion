<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String pdfName = "";
String type = "";
if(request.getAttribute("pdfName")!=null){
	pdfName = request.getAttribute("pdfName").toString();
	type = request.getAttribute("type").toString();
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<link rel=stylesheet type="text/css" href="css/type.css" />
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
<!--
.text-style-1 {
	font-family: Tahoma;
	font-size: 11px;
	font-weight: normal;
	color: blue;
	text-decoration: none;
}
-->
</style>
</head>
  
  <body bottommargin="0" topmargin="0" rightmargin="0" leftmargin="0"	class="HomeBody">
    New XML.<br><font color="red">Pub-Number:</font> <br>
    <div id="A2_bool_div" style="display:none;">
    	<input type="hidden" name="A2_bool" value="<bean:write name="xmlBean" property="b130"/>" id="A2_bool" />
    	<table width="100%">
    		<tr>
    			<td align="left" valign="bottom">
    				<a href="E:/Work Station HK/HKConvertion/pdf/pdf_A2/<bean:write name="xmlBean" property="b110"/>A2.pdf" target="_blank">A2_pdf</a>
    			</td>
    		</tr>
    	</table>
    </div>
    <form action="HKConvertionQASubmit.do?method=QASubmit" method="post" name="TransSbmForm">
    	
		<table width="100%" cellspacing="0" cellpadding="0" border="1">
    		<tr>
    			<td width="50%" id="showPDF" class="Bluewordbold12thsize" valign="top">
						<embed id=e src="/HKConvertionPdf/pdf/<%=pdfName%>.pdf" width="100%" height="1000" type="application/pdf" />
				</td>
    			<td align="left" width="50%" class="Bluewordbold12thsize"
					valign="top">
					
					
					
					<div style="scrollbar-face-color:#8fc8e0;overflow:auto;width:100%;color:blue;scrollbar-track-color:#ffffff;height:1000px;"> 
					
					<%
						int b300_number = 0;
						int b512_number = 0;
						int b561_number = 0;
						int b562_number = 0;
						int b711_number = 0;
						int b721_number = 0;
						int b731_number = 0;
						int b741_number = 0;
						int claims_number = 0;
						int descriptions_number = 0;
						
					 %>
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
					
					    <!-- b100 -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											publ. country
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											<input type="text" size="12" value="${countryName}" readOnly />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											publ. number
										</td>
										<td align="left" class="Bluewordbold12thsize">
												<input type="text" id="b110" name="b110" size="12" maxlength="10" value="<bean:write name="xmlBean" property="b110"/>" readOnly />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											publ. kind
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<input type="text" id="b130" name="b130" size="8"  maxlength="10" value="<bean:write name="xmlBean" property="b130"/>" readOnly />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- b200 -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Appl. Number
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											<input type="text" size="15" name="b210" id="b210" value="<bean:write name="xmlBean" property="b210"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											appl. Date
										</td>
										<td align="left">
											<logic:notEmpty name="xmlBean" property="b220Year">
												<input type="text" size="15" name="b220Date" id="b220Date" value="<bean:write name="xmlBean" property="b220Day"/>/<bean:write name="xmlBean" property="b220Month"/>/<bean:write name="xmlBean" property="b220Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:notEmpty>
											<logic:empty name="xmlBean" property="b220Year">
												<input type="text" size="15" name="b220Date" id="b220Date" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:empty>
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											appl. language
										</td>
										<td align="left">
											<input type="text" size="15" name="b250" id="b250" value="<bean:write name="xmlBean" property="b250"/>"/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- b300 -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td width="20%" align="left" class="Bluewordbold12thsize">
											Priority:
										</td>
										<td width="80%" align="right" class="Bluewordbold12thsize">
											<input type="button" id="addPri" name="addPri" value="Add Priority" onclick="addPRI()" />
										</td>
									</tr>
									<logic:iterate id="b300Bean" name="xmlBean" property="b300"
										type="com.lighthouse.translation.hkconvertion.beans.B300Bean">
									<tr>
										<td colspan="2" style="color:red;">
												priority<%=(b300_number + 1)%>
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Prio. Number
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<input type="text" size="15" name="b310_<%=b300_number%>" value="<bean:write name="b300Bean" property="b310"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Prio. Date
										</td>
										<td align="left">
											<input type="text" id="b320Date_<%=b300_number%>" name="b320Date_<%=b300_number%>" size="15" value="<bean:write name="b300Bean" property="b320Day"/>/<bean:write name="b300Bean" property="b320Month"/>/<bean:write name="b300Bean" property="b320Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Prio. Country
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<input type="text" size="15" name="b330Ctry_<%=b300_number%>" id="b330Ctry_<%=b300_number%>" value="<bean:write name="b300Bean" property="b330Ctry"/>" />
										</td>
									</tr>
									<%
												b300_number++;
									%>
									</logic:iterate>
								</table>
							</td>
						</tr>
									
						<tr>
							<td align="left" class="Bluewordbold12thsize" width="100%" height="50%" id="b300Number" style="color:red;">
								<input type="hidden" name="b300_number" value="<%=b300_number%>" id="b300_number" />
							</td>
						</tr>
						
						<!-- b400 -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Publ. date
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											<logic:notEmpty name="xmlBean" property="b430Year">
												<logic:equal value="A0" name="xmlBean" property="b130">
													<input type="text" size="15" name="b430Date" id="b430Date" value="<bean:write name="xmlBean" property="b430Day"/>/<bean:write name="xmlBean" property="b430Month"/>/<bean:write name="xmlBean" property="b430Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
												</logic:equal>
												<logic:equal value="A1" name="xmlBean" property="b130">
													<input type="text" size="15" name="b430Date" id="b430Date" />eg:(DD(/.-)MM(/.-)YYYY)
												</logic:equal>
												<logic:equal value="A2" name="xmlBean" property="b130">
													<input type="text" size="15" name="b430Date" id="b430Date" />eg:(DD(/.-)MM(/.-)YYYY)
												</logic:equal>
											</logic:notEmpty>
											<logic:empty name="xmlBean" property="b430Year">
												<input type="text" size="15" id="b430Date" name="b430Date" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:empty>
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Grant. Date
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<logic:notEmpty name="xmlBean" property="b450Year">
												<input type="text" id="b450Date" name="b450Date" size="15" value="<bean:write name="xmlBean" property="b450Day"/>/<bean:write name="xmlBean" property="b450Month"/>/<bean:write name="xmlBean" property="b450Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:notEmpty>
											<logic:empty name="xmlBean" property="b450Year">
												<input type="text" id="b450Date" name="b450Date" size="15" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:empty>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- b500 -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											IPC main
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											class&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" size="20" name="b511Class" value="<bean:write name="xmlBean" property="b511Class"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
										</td>
										<td align="left" class="Bluewordbold12thsize">
											version<input type="text" size="20" name="b511Version" value="<bean:write name="xmlBean" property="b511Version"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											IPC further
										</td>
										<td align="right" class="Bluewordbold12thsize">
											<input type="button" id="addFur" name="addFur" value="Add Further" onclick="addFUR()" />
										</td>
									</tr>
									<logic:iterate id="b512Bean" name="xmlBean" property="b512" type="com.lighthouse.translation.hkconvertion.beans.B512Bean">
										<tr>
											<td colspan="2" style="color:red;">
												further<%=(b512_number + 1)%>
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												class
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="20" name="b512Class_<%=b512_number%>" id="b512Class_<%=b512_number%>" value="<bean:write name="b512Bean" property="b512Class"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												version
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="20" name="b512Version_<%=b512_number%>" id="b512Version_<%=b512_number%>" value="<bean:write name="b512Bean" property="b512Version"/>" />
											</td>
										</tr>
									<%
												b512_number++;
									%>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="b512Number" style="color:red;">
											<input type="hidden" name="b512_number" value="<%=b512_number%>" id="b512_number" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											IPC edition
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<input type="text" size="20" name="b516" value="<bean:write name="xmlBean" property="b516"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											title language
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<input type="text" size="20" id="titleLanguage" name="b541" value="<bean:write name="xmlBean" property="b541"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											title
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<textarea rows="2" cols="55" name="b542" id="title" onBlur="titleLanguageChange('title','titleLanguage')" ><bean:write name="xmlBean" property="b542"/></textarea>
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Citation
										</td>
										<td align="right" class="Bluewordbold12thsize">
											<input type="button" id="addCit" name="addCit" value="Add Citation" onclick="addCIT()" />
										</td>
									</tr>
									<logic:iterate id="b561Bean" name="xmlBean" property="b561" type="com.lighthouse.translation.hkconvertion.beans.B561Bean">
										<tr>
											<td colspan="2" style="color:red;">
												citation<%=(b561_number + 1)%>
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Citation country
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="20" name="b561Ctry_<%=b561_number%>"  value="<bean:write name="b561Bean" property="b561Ctry"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Citation number
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="20" name="b561Dnum_<%=b561_number%>" value="<bean:write name="b561Bean" property="b561Dnum"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Citation kind
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="20" name="b561Kind_<%=b561_number%>" value="<bean:write name="b561Bean" property="b561Kind"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Citation name
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="20" name="b561Snm_<%=b561_number%>" value="<bean:write name="b561Bean" property="b561Snm"/>" />
											</td>
										</tr>
										<%
											b561_number++;
										 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="b561Number" style="color:red;">
											<input type="hidden" name="b561_number" value="<%=b561_number%>" id="b561_number" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Citation identification
										</td>
										<td align="left" class="Bluewordbold12thsize">
											<input type="text" size="20" name="b563" value="<bean:write name="xmlBean" property="b563"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Non-patent reference
										</td>
										<td align="right" class="Bluewordbold12thsize">
											<input type="button" id="addRef" name="addRef" value="Add reference" onclick="addREF()" />
										</td>
									</tr>
									<logic:iterate id="b562Bean" name="xmlBean" property="b562" type="com.lighthouse.translation.hkconvertion.beans.B562Bean">
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Non-patent reference: <%=(b562_number + 1)%> 
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="20" name="b562Reference_<%=b562_number%>" value="<bean:write name="b562Bean" property="b562Reference"/>" />
											</td>
										</tr>
										<%
											b562_number++;
										 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="b562Number" style="color:red;">
											<input type="hidden" name="b562_number" value="<%=b562_number%>" id="b562_number" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- b600 -->
						
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Divisional number
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											<input type="text" name="b620Dnum" size="20" value="<bean:write name="xmlBean" property="b620Dnum"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Divisional Date
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
										<logic:empty name="xmlBean" property="b620Day">
												<input type="text" size="15" name="b620Date" id="b620Date" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:empty>
											<logic:notEmpty name="xmlBean" property="b620Day">
												<input type="text" size="15" name="b620Date" id="b620Date" value="<bean:write name="xmlBean" property="b620Day"/>/<bean:write name="xmlBean" property="b620Month"/>/<bean:write name="xmlBean" property="b620Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:notEmpty>
										</td>
									</tr>
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Reference country
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											<input type="text" name="b655Ctry" size="20" value="<bean:write name="xmlBean" property="b655Ctry"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Reference number
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											<input type="text" name="b655Pnum" size="20" value="<bean:write name="xmlBean" property="b655Pnum"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Reference kind
										</td>
										<td align="left" width="80%" class="Bluewordbold12thsize">
											<input type="text" name="b655Kind" size="20" value="<bean:write name="xmlBean" property="b655Kind"/>" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- b700 -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											Applicant
										</td>
										<td align="right" width="80%" class="Bluewordbold12thsize">
											<input type="button" id="addApp" name="addApp" value="Add Applicant" onclick="addAPP()" />
										</td>
									</tr>
									<logic:iterate id="b711Bean" name="xmlBean" property="b711" type="com.lighthouse.translation.hkconvertion.beans.B711Bean">
										<tr>
											<td colspan="2" style="color:red;">
												applicant<%=(b711_number + 1)%>
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Applicant name
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b711Snm_<%=b711_number%>" value="<bean:write name="b711Bean" property="b711Snm"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Applicant address
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b711Str_<%=b711_number%>" value="<bean:write name="b711Bean" property="b711Str"/>" />
											</td>
										</tr>										
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Applicant country
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b711Ctry_<%=b711_number%>" id="b711Ctry_<%=b711_number%>" value="<bean:write name="b711Bean" property="b711Ctry"/>" />
											</td>
										</tr>
										<%
											b711_number++;
										 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="b711Number" style="color:red;">
											<input type="hidden" name="b711_number" value="<%=b711_number%>" id="b711_number" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Inventor
										</td>
										<td align="right" class="Bluewordbold12thsize">
											<input type="button" id="addInv" name="addInv" value="Add Inventor" onclick="addINV()" />
										</td>
									</tr>
									<logic:iterate id="b721Bean" name="xmlBean" property="b721" type="com.lighthouse.translation.hkconvertion.beans.B721Bean">
										<tr>
											<td colspan="2" style="color:red;">
												inventor<%=(b721_number + 1)%>
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Inventor name
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b721Snm_<%=b721_number%>" value="<bean:write name="b721Bean" property="b721Snm"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Inventor address
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b721Str_<%=b721_number%>" value="<bean:write name="b721Bean" property="b721Str"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Inventor cntry
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b721City_<%=b721_number%>" value="<bean:write name="b721Bean" property="b721City"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Inventor country
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b721Ctry_<%=b721_number%>" id="b721Ctry_<%=b721_number%>" value="<bean:write name="b721Bean" property="b721Ctry"/>" />
											</td>
										</tr>
										<%
											b721_number++;
										 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="b721Number" style="color:red;">
											<input type="hidden" name="b721_number" value="<%=b721_number%>" id="b721_number" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Grantee
										</td>
										<td align="right" class="Bluewordbold12thsize">
											<input type="button" id="addGra" name="addGra" value="Add Grantee" onclick="addGRA()" />
										</td>
									</tr>
									<logic:iterate id="b731Bean" name="xmlBean" property="b731" type="com.lighthouse.translation.hkconvertion.beans.B731Bean">
										<tr>
											<td colspan="2" style="color:red;">
												grantee<%=(b731_number + 1)%>
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Grantee name
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b731Snm_<%=b731_number%>" value="<bean:write name="b731Bean" property="b731Snm"/>" />
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Grantee address
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b731Str_<%=b731_number%>" value="<bean:write name="b731Bean" property="b731Str"/>" />
											</td>
										</tr>										
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Grantee country
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b731Ctry_<%=b731_number%>" id="b731Ctry_<%=b731_number%>" value="<bean:write name="b731Bean" property="b731Ctry"/>" />
											</td>
										</tr>
										<%
											b731_number++;
										 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="b731Number" style="color:red;">
											<input type="hidden" name="b731_number" value="<%=b731_number%>" id="b731_number" />
										</td>
									</tr>
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Attorney
										</td>
										<td align="right" class="Bluewordbold12thsize">
											<input type="button" id="addAtt" name="addAtt" value="Add Attorney" onclick="addATT()" />
										</td>
									</tr>
									<logic:iterate id="b741Bean" name="xmlBean" property="b741" type="com.lighthouse.translation.hkconvertion.beans.B741Bean">
										<tr>
											<td colspan="2" style="color:red;">
												attorney<%=(b741_number + 1)%>
											</td>
										</tr>
										<tr>
											<td align="left" class="Bluewordbold12thsize">
												Attorney name
											</td>
											<td align="left" class="Bluewordbold12thsize">
												<input type="text" size="70" name="b741Snm_<%=b741_number%>" value="<bean:write name="b741Bean" property="b741Snm"/>" />
											</td>
										</tr>
										<%
											b741_number++;
										 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="b741Number" style="color:red;">
											<input type="hidden" name="b741_number" value="<%=b741_number%>" id="b741_number" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- b800 -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'" onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											inid 85 date
										</td>
										<td width="80%" align="left">
											<logic:notEmpty name="xmlBean" property="b850Year">
												<input type="text" name="b850Date" id="b850Date" size="15" value="<bean:write name="xmlBean" property="b850Day"/>/<bean:write name="xmlBean" property="b850Month"/>/<bean:write name="xmlBean" property="b850Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:notEmpty>
											<logic:empty name="xmlBean" property="b850Year">
												<input type="text" name="b850Date" id="b850Date" size="15" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:empty>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'" onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											WO appl. Number
										</td>
										<td width="80%" align="left">
											<input type="text" size="70" name="b861Anum" id="b861Anum" value="<bean:write name="xmlBean" property="b861Anum"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											WO appl. Date
										</td>
										<td width="80%" align="left">
											<logic:notEmpty name="xmlBean" property="b861Year">
												<input type="text" name="b861Date" size="15" id="b861Date" value="<bean:write name="xmlBean" property="b861Day"/>/<bean:write name="xmlBean" property="b861Month"/>/<bean:write name="xmlBean" property="b861Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:notEmpty>
											<logic:empty name="xmlBean" property="b861Year">
												<input type="text" name="b861Date" size="15" id="b861Date" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:empty>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'" onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											WO publ. number
										</td>
										<td width="80%" align="left">
											<input type="text" name="b871Pnum" id="b871Pnum" size="70" value="<bean:write name="xmlBean" property="b871Pnum"/>" />
										</td>
									</tr>
									<tr>
										<td align="left" width="20%" class="Bluewordbold12thsize">
											WO publ. date
										</td>
										<td width="80%" align="left">
											<logic:notEmpty name="xmlBean" property="b871Year">
												<input type="text" id="b871Date" name="b871Date" size="15" value="<bean:write name="xmlBean" property="b871Day"/>/<bean:write name="xmlBean" property="b871Month"/>/<bean:write name="xmlBean" property="b871Year"/>" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:notEmpty>
											<logic:empty name="xmlBean" property="b871Year">
												<input type="text" id="b871Date" name="b871Date" size="15" />eg:(DD(/.-)MM(/.-)YYYY)
											</logic:empty>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- sec -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'" onmouseout="this.style.backgroundColor='#FFFFFF'">
								<td align="left" valign="top" width="100%" height="50%">
									<table width="100%" cellspacing="0" cellpadding="0" border="1">
										<tr>
											<td align="left" width="20%" class="Bluewordbold12thsize">
												Abstract
											</td>
											<td width="80%" align="left">
												<textarea rows="6" cols="65" name="abeng" id="abeng" type="_moz">
													<bean:write name="xmlBean" property="abeng" />
												</textarea>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						
						<!-- claims -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td width="20%" align="left" class="Bluewordbold12thsize">
											Claims:
										</td>
										<td width="80%" align="right" class="Bluewordbold12thsize">
											<input type="button" id="addCla" name="addCla" value="Add Claims" onclick="addCLA()" />
										</td>
									</tr>
									<logic:iterate id="claimsBean" name="xmlBean" property="claims" type="com.lighthouse.translation.hkconvertion.beans.ClaimsBean">
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Claim<%=(claims_number + 1)%>
										</td>
										<td align="left" class="Bluewordbold12thsize">
										<textarea rows="8" cols="65" name="claimsClm_<%=claims_number%>" id="claimsClm_<%=claims_number%>" type="_moz"><bean:write name="claimsBean" property="claimsClm"/></textarea>
										</td>
									</tr>
									<%
										claims_number++;
									 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="claimsNumber" style="color:red;">
											<input type="hidden" name="claims_number" value="<%=claims_number%>" id="claims_number" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<!-- discription -->
						<tr onmouseover="this.style.backgroundColor='#BFB5CA'"
							onmouseout="this.style.backgroundColor='#FFFFFF'">
							<td align="left" valign="top" width="100%" height="50%">
								<table width="100%" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td width="20%" align="left" class="Bluewordbold12thsize">
											Descriptions:
										</td>
										<td width="80%" align="right" class="Bluewordbold12thsize">
											<input type="button" id="addDes" name="addDes" value="Add Descriptions" onclick="addDES()" />
										</td>
									</tr>
									<logic:iterate id="descriptionsBean" name="xmlBean" property="descriptions" type="com.lighthouse.translation.hkconvertion.beans.DescriptionsBean">
									<tr>
										<td align="left" class="Bluewordbold12thsize">
											Description<%=(descriptions_number + 1)%>
										</td>
										<td align="left" class="Bluewordbold12thsize">
										<textarea rows="8" cols="65" name="description_<%=descriptions_number%>" id="description_<%=descriptions_number%>" type="_moz"><bean:write name="descriptionsBean" property="description"/></textarea>
										</td>
									</tr>
									<%
										descriptions_number++;
									 %>
									</logic:iterate>
									<tr>
										<td align="left" colspan="2" class="Bluewordbold12thsize" width="100%" height="50%" id="descriptionsNumber" style="color:red;">
											<input type="hidden" name="descriptions_number" value="<%=descriptions_number%>" id="descriptions_number" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						
					</table>
					
					</div>
				</td>
    		</tr>
    		<tr>
    			
    			<td align="center" height="50">
    				<input type="button" name="subbutton" onclick="validate('reopen')" value="Reopen" id="subbutton" class="inputbutton3" />
    			</td>
    			<td align="center" height="50">
    				<input type="button" name="subbutton" onclick="validate('submit')" value="Submit" id="subbutton" class="inputbutton3" />
    			</td>
    			
    		</tr>
    	</table>
    </form>
  </body>
</html>
<script type="text/javascript"><!--

	var a_bool = document.getElementById("A2_bool").value;
	
	if (a_bool == 'A0'){
		document.getElementById("addGra").disabled=true;
	}
	
	if (a_bool == 'A1') {
		document.getElementById("addApp").disabled=true;
	}
	
	if (a_bool == 'A2') {
		document.getElementById("addApp").disabled=true;
		document.getElementById("A2_bool_div").style.display="";
	}
	
	var b300_number = parseInt(document.getElementById("b300_number").value);
	var b512_number = parseInt(document.getElementById("b512_number").value);
	var b561_number = parseInt(document.getElementById("b561_number").value);
	var b562_number = parseInt(document.getElementById("b562_number").value);
	var b711_number = parseInt(document.getElementById("b711_number").value);
	var b721_number = parseInt(document.getElementById("b721_number").value);
	var b731_number = parseInt(document.getElementById("b731_number").value);
	var b741_number = parseInt(document.getElementById("b741_number").value);
	var claims_number = parseInt(document.getElementById("claims_number").value);
	var descriptions_number = parseInt(document.getElementById("descriptions_number").value);
	var pdfNumber = document.getElementById("pdfNumber").value;
	var title_language_country = document.getElementById("country").value;
	function addPRI(){
		var tdObj =document.getElementById("b300Number");
		tdObj.innerHTML = tdObj.innerHTML + ("priority: "+(b300_number+1)+":<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Prio. Number</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='15' name='b310_"+b300_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Prio. Date</td><td align='left'><input type='text' id='b320Date_"+b300_number+"' name='b320Date_"+b300_number+"' size='15' />eg:(DD(/.-)MM(/.-)YYYY)</td></tr><tr><td align='left' class='Bluewordbold12thsize'>Prio. Country</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='15' name='b330Ctry_"+b300_number+"' /></td></tr></table>");
		
		b300_number++;
	}
	
	function addFUR(){
		var tdObj =document.getElementById("b512Number");
		tdObj.innerHTML = tdObj.innerHTML + ("further: "+(b512_number+1)+":<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>class</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='20' name='b512Class_"+b512_number+"' id='b512Class_"+b512_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>version</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='20' name='b512Version_"+b512_number+"' id='b512Version_"+b512_number+"' /></td></tr></table>");
		
		b512_number++;
	}
	
	function addCIT(){
		var tdObj =document.getElementById("b561Number");
		tdObj.innerHTML = tdObj.innerHTML + ("citation: "+(b561_number+1)+":<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Citation country</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='20' name='b561Ctry_"+b561_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Citation number</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='20' name='b561Dnum_"+b561_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Citation kind</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='20' name='b561Kind_"+b561_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Citation name</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='20' name='b561Snm_"+b561_number+"' /></td></tr></table>");
		
		b561_number++;
	}
	
	function addREF(){
		var tdObj =document.getElementById("b562Number");
		tdObj.innerHTML = tdObj.innerHTML + ("<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Non-patent reference: "+(b562_number+1)+"</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='20' name='b562Reference_"+b562_number+"' /></td></tr></table>");
		
		b562_number++;
	}
	
	function addAPP(){
		var tdObj =document.getElementById("b711Number");
		tdObj.innerHTML = tdObj.innerHTML + ("applicant: "+(b711_number+1)+":<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Applicant name</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b711Snm_"+b711_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Applicant address</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b711Str_"+b711_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Applicant country</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b711Ctry_"+b711_number+"' /></td></tr></table>");
		
		b711_number++;
	}
	
	function addINV(){
		var tdObj =document.getElementById("b721Number");
		tdObj.innerHTML = tdObj.innerHTML + ("inventor: "+(b721_number+1)+":<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Inventor name</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b721Snm_"+b721_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Inventor address</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b721Str_"+b721_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Inventor cntry</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b721City_"+b721_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Inventor country</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b721Ctry_"+b721_number+"' /></td></tr></table>");
		
		b721_number++;
	}
	
	function addGRA(){
		var tdObj =document.getElementById("b731Number");
		tdObj.innerHTML = tdObj.innerHTML + ("grantee: "+(b731_number+1)+":<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Grantee name</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b731Snm_"+b731_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Grantee address</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b731Str_"+b731_number+"' /></td></tr><tr><td align='left' class='Bluewordbold12thsize'>Grantee country</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b731Ctry_"+b731_number+"' /></td></tr></table>");
		
		b731_number++;
	}
	
	function addATT(){
		var tdObj =document.getElementById("b741Number");
		tdObj.innerHTML = tdObj.innerHTML + ("attorney: "+(b741_number+1)+":<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Attorney name</td><td align='left' class='Bluewordbold12thsize'><input type='text' size='70' name='b741Snm_"+b741_number+"' /></td></tr></table>");
		
		b741_number++;
	}
	
	function addCLA(){
		var tdObj =document.getElementById("claimsNumber");
		tdObj.innerHTML = tdObj.innerHTML + ("<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Claim"+(claims_number + 1)+"</td><td align='left' class='Bluewordbold12thsize'><textarea rows='8' cols='65' name='claimsClm_"+claims_number+"' type='_moz'></textarea></td></tr></table>");
		
		claims_number++;
	}
	
	function addDES(){
		var tdObj =document.getElementById("descriptionsNumber");
		tdObj.innerHTML = tdObj.innerHTML + ("<table width='100%' cellspacing='0' cellpadding='0' border='1'><tr><td width='20%' align='left' class='Bluewordbold12thsize'>Description"+(descriptions_number + 1)+"</td><td align='left' class='Bluewordbold12thsize'><textarea rows='8' cols='65' name='description_"+descriptions_number+"' type='_moz'></textarea></td></tr></table>");
		
		descriptions_number++;
	}
	
	function validate(s){
	var msg="";
  	 succ =true;
  	 
  	 var b655_ctry =  document.getElementById("b655Ctry").value;
  	var b655_pnum =  document.getElementById("b655Pnum").value;
  	var b655_kind =  document.getElementById("b655Kind").value;
  	
  	if (!((b655_ctry=='' && b655_pnum=='' && b655_kind=='') || (b655_ctry!='' && b655_pnum!='' && b655_kind!=''))){
  		msg=msg+"Reference format is wrong!\n";
 		succ=false;
  	}
  	 
	 var b220Date=document.getElementById("b220Date").value;
	 
 		if(isNotDate(b220Date) || document.getElementById("b220Date").value == ''){
 			msg=msg+"appl. Date: International Date format is wrong!\n";
 			succ=false;
 	 	}
 	 
 	 
 	 if (b300_number != 0) {
 	 	var b320Date=document.getElementById("b320Date_"+(b300_number-1)).value;
 	 	
 		if(isNotDate(b320Date)){
 			msg=msg+"Prio. Date: International Date format is wrong!\n";
 			succ=false;
 		}
 	
 	}
 	
 	 var b210=document.getElementById("b210").value;
 		if(document.getElementById("b210").value == ''){
 		msg=msg+"Appl. Number: Appl. Number is null!\n";
 		succ=false;
 	 }
 	 
 	 var b430Date=document.getElementById("b430Date").value;
 	 if (a_bool == 'A0'){
 		if(isNotDate(b430Date) || (document.getElementById("b430Date").value == '' && title_language_country != "MY")){
 			msg=msg+"Publ. date: International Date format is wrong!\n";
 			succ=false;
 	 	}
 	 }
 	 
 	 if (a_bool == 'A1' || a_bool == 'A2'){
 			if (b430Date !=''){
 				msg=msg+"Publ. Date: is null!\n";
 				succ=false;
 			}
 	 }
 	 
 	 
 	 var b450Date=document.getElementById("b450Date").value;
 	 
 	 if (a_bool == 'A1' || a_bool == 'A2'){
 		
 			if (b450Date == ''){
 				msg=msg+"Grant. Date: is null!\n";
 				succ=false;
 			}
 		
 		}
 	 
 	 if(isNotDate(b450Date)){
 		msg=msg+"Grant. Date: International Date format is wrong!\n";
 		succ=false;
 	 }
 	  
 	 
 	 var b850Date=document.getElementById("b850Date").value;
 	 if(isNotDate(b850Date)){
 		msg=msg+"inid 85 date: International Date format is wrong!\n";
 		succ=false;
 	 }
 	 
 	  var b861Date=document.getElementById("b861Date").value;
 	 if(isNotDate(b861Date)){
 		msg=msg+"WO appl. Date: International Date format is wrong!\n";
 		succ=false;
 	 }
 	 
 	 var b871Date=document.getElementById("b871Date").value;
 	 if(isNotDate(b871Date)){
 		msg=msg+"WO publ. date: International Date format is wrong!\n";
 		succ=false;
 	 }
 	
 	
 	//b330Ctry
 	 var b300_succ = false;
 	 for(i=0;i<b300_number;i++){
 	 	var b330CtryStr = "b330Ctry_"+i;
 	 	var b310Str = "b310_"+i;
 	 	var b320DateStr = "b320Date_"+i;
 	 	
 	 		if(!(document.getElementById(b330CtryStr).value == '' && document.getElementById(b310Str).value == '' && document.getElementById(b320DateStr).value == '') && !(document.getElementById(b330CtryStr).value != '' && document.getElementById(b310Str).value != '' && document.getElementById(b320DateStr).value != '')){
 				b300_succ = true;
 			}
 	 	
 	 	
 	 	if (document.getElementById(b330CtryStr).value != ''){
 	 		var countryStr=new Array("AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GB","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","AN","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SK","SI","SB","SO","ZA","GS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","KN","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","US","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW","WO","EP","EA","IB");
 			succ=false;
 			for(j=0;j<countryStr.length;j++){
 				if(countryStr[j]==document.getElementById(b330CtryStr).value.toUpperCase()) {
 					succ=true;
 				}
 			}
 			if(!succ){
 				msg=msg+"Prio. Country: International Country's format is wrong!\n";
 			}
 		}
 	}
 	if (b300_succ){
 		msg=msg+"Priority: format is wrong!\n";
 	}
 	
 	//b450Date
 	if (title_language_country == "MY") {
 	
 		if (document.getElementById("b450Date").value == "") {
 			msg=msg+"Grant. date is null!\n";
 		}
 		
 	}
 	
 	//只有A0时才验证
 	if (a_bool == 'A0'){
 		
 	
 	//b711Ctry
 	b711_kind=false;
 	for(i=0;i<b711_number;i++){
 	 	var b711CtryStr = "b711Ctry_"+i;
 	 	if (document.getElementById(b711CtryStr).value != ''){
 	 		var countryStr=new Array("AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GB","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","AN","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SK","SI","SB","SO","ZA","GS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","KN","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","US","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW","WO","EP","EA");
 			succ=false;
 			for(j=0;j<countryStr.length;j++){
 				if(countryStr[j]==document.getElementById(b711CtryStr).value.toUpperCase()) {
 					succ=true;
 				}
 			}
 			if(!succ){
 				msg=msg+"Applicant country: International Country's format is wrong!\n";
 			}
 		}
 		
 		//b711 kind
 		if (document.getElementById("b130").value == "A" && (document.getElementById("b711Snm_"+i).value == '' || document.getElementById("b711Ctry_"+i).value == '')) {
 			b711_kind=true;
 		}
 		
 		if (document.getElementById("b130").value == "A" && document.getElementById("b711Snm_"+i).value == '' && document.getElementById("b711Str_"+i).value == '' && document.getElementById("b711City_"+i).value == '' && document.getElementById("b711Ctry_"+i).value == '') {
 			b711_kind=false;
 		}
 		
 	}
 	
 	if (b711_kind){
 		msg=msg+"Applicant: Applicant is null!\n";
 	}
 	
 	}
 	
 	//b721City
 	for(i=0;i<b721_number;i++){
 	 	var b721CityStr = "b721City_"+i;
 	 	if (document.getElementById(b721CityStr).value != ''){
 	 		var countryStr=new Array("YU","AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GB","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","AN","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SK","SI","SB","SO","ZA","GS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","KN","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","US","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW","WO","EP","EA");
 			succ=false;
 			for(j=0;j<countryStr.length;j++){
 				if(countryStr[j]==document.getElementById(b721CityStr).value.toUpperCase()) {
 					succ=true;
 				}
 			}
 			if(!succ){
 				msg=msg+"Inventor city: International City's format is wrong!\n";
 			}
 		}
 	}
 	
 	//b721Ctry
 	for(i=0;i<b721_number;i++){
 	 	var b721CtryStr = "b721Ctry_"+i;
 	 	if (document.getElementById(b721CtryStr).value != ''){
 	 		var countryStr=new Array("AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GB","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","AN","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SK","SI","SB","SO","ZA","GS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","KN","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","US","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW","WO","EP","EA");
 			succ=false;
 			for(j=0;j<countryStr.length;j++){
 				if(countryStr[j]==document.getElementById(b721CtryStr).value.toUpperCase()) {
 					succ=true;
 				}
 			}
 			if(!succ){
 				msg=msg+"Inventor country: International Country's format is wrong!\n";
 			}
 		}
 	}
 	
 	
 	if (a_bool != 'A0'){
 	
 	
 	//b731Ctry
 	b731_kind=false;
 	for(i=0;i<b731_number;i++){
 	 	var b731CtryStr = "b731Ctry_"+i;
 	 	if (document.getElementById(b731CtryStr).value != ''){
 	 		var countryStr=new Array("YU","AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GB","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","AN","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SK","SI","SB","SO","ZA","GS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","KN","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","US","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW","WO","EP","EA");
 			succ=false;
 			for(j=0;j<countryStr.length;j++){
 				if(countryStr[j]==document.getElementById(b731CtryStr).value.toUpperCase()) {
 					succ=true;
 				}
 			}
 			if(!succ){
 				msg=msg+"Grantee country: International Country's format is wrong!\n";
 			}
 		}
 		
 		//b731 kind
 		if (document.getElementById("b130").value == "B" && (document.getElementById("b731Snm_"+i).value == '' || document.getElementById("b731Str_"+i).value == '' || document.getElementById("b731City_"+i).value == '' || document.getElementById("b731Ctry_"+i).value == '')) {
 			b731_kind=true;
 		}
 		
 	}
 	
 	//b731 kind
 	if (document.getElementById("b130").value == "B" && b731_number == 0){
 		b731_kind=true;
 	}
 	
 	if (b731_kind){
 		msg=msg+"Grantee: Grantee is null!\n";
 	}
 	
 	}
 	
 	var b871Number = document.getElementById("b871Pnum").value;
 	var b861Number = document.getElementById("b861Anum").value;
 	if (!((b871Number.length==10 && b871Number.substring(4,5)=="/" && b871Number.substring(0,2)=="WO") || (b871Number.length==12 && b871Number.substring(6,7)=="/" && b871Number.substring(0,2)=="WO") || (b871Number.length==11 && b871Number.substring(4,5)=="/" && b871Number.substring(0,2)=="WO") || (b871Number.length==13 && b871Number.substring(6,7)=="/" && b871Number.substring(0,2)=="WO") || b871Number.length==0)) {
 		msg=msg+"WO publ. number format is wrong!\n";
 	}
 	
 	if (b871Number.length!=0 && b871Date.length==0){
 		msg=msg+"WO publ. date is null!\n";
 	}
 	
 	if (b861Number.length!=0 && b861Date.length==0){
 		msg=msg+"WO appl. Date is null!\n";
 	}
 	
 	if(msg){
 		 alert(msg);
  		return false;
 	}else{
 	
 		if (s=='submit'){
 		
 			if(confirm("Submit this document. Are you sure?")){
 				//update number value
 				document.getElementById("b300_number").value=b300_number;
				document.getElementById("b512_number").value=b512_number;
				document.getElementById("b561_number").value=b561_number;
				document.getElementById("b562_number").value=b562_number;
				document.getElementById("b711_number").value=b711_number;
				document.getElementById("b721_number").value=b721_number;
				document.getElementById("b731_number").value=b731_number;
				document.getElementById("b741_number").value=b741_number;
				document.getElementById("claims_number").value=claims_number;
				document.getElementById("descriptions_number").value=descriptions_number;
				document.TransSbmForm.action="HKConvertionQASubmit.do?method=QASubmit";
				document.TransSbmForm.submit();
  			}
  			
  		}
  		
  		if (s=='reopen'){
 		
 			if(confirm("Reopen this document. Are you sure?")){
 				//update number value
 				document.getElementById("b300_number").value=b300_number;
				document.getElementById("b512_number").value=b512_number;
				document.getElementById("b561_number").value=b561_number;
				document.getElementById("b711_number").value=b711_number;
				document.getElementById("b721_number").value=b721_number;
				document.getElementById("b731_number").value=b731_number;
				document.getElementById("b741_number").value=b741_number;
				document.getElementById("claims_number").value=claims_number;
				document.getElementById("descriptions_number").value=descriptions_number;
				document.TransSbmForm.action="HKConvertionQASubmit.do?method=QAReopen";
				document.TransSbmForm.submit();
  			}
  			
  		}
  		
 	}
		
		
	}
	
	function isNotDate(s){
		if (s.substring(2,3) == ' ') {
			var date_day = s.split(" ")[0];
			var date_month = s.split(" ")[1];
			var date_year = s.split(" ")[2];
			var month_bool = 'false';
			var month = new Array('January,Jan','February,Feb','March,Mar','April,Apr','May,May','June,Jun','July,Jul','August,Aug','September,Sept','October,Oct','November,Nov','December,Dec');
			for (i=0; i<month.length; i++) {
				if (month[i].split(",")[0] == date_month || month[i].split(",")[1] == date_month){
					month_bool = 'true';
					break;
				}
				
			}
			
			if ((date_year>1980 && month_bool == 'true' && date_day>=01 && date_day<=31) || s.length==0) {
				return false;
			} else {
				return true;
			}
		
		} else{
			var date_format_1 =  s.substring(2,3);
			var date_format_2 =  s.substring(5,6);
			var date_month = s.substring(3,5);
			var date_day = s.substring(0,2);
			var date_year = s.substring(6,10);
		
			if ((s.length==10 && date_year>1980 && date_month>=01 && date_month<=12 && date_day>=01 && date_day<=31 && ((date_format_1=='/' && date_format_2=='/') || (date_format_1=='.' && date_format_2=='.') || (date_format_1=='-' && date_format_2=='-'))) || s.length==0) {
				return false;
			} else {
				return true;
			}
			
		}
		
	}
	
	function isNotYear(s){
		if((s>1980 && s<2020)||s.length==0){
			return false;
		}else{
			return true;
		}
	}
	function isNotMonth(s){
		if((s>=01 && s<=12 && s.length==2)||s.length==0){
			return false;
		}else{
			return true;
		}
	}
	function isNotDay(s){
		if((s>=01 && s<=31 && s.length==2)||s.length==0){
			return false;
		}else{
			return true;
		}
	}
	
	function titleLanguageChange(title, titleLanguage){
		if (document.getElementById(title).value != '') {
			//document.getElementById(titleLanguage).value = title_language_country;
		} else {
			document.getElementById(titleLanguage).value = '';
		}
	}
	
	var xmlHttp;   
    var result;   
    function createXMLHttpRequest(){   
        if(window.ActiveXObject){   
            try{   
                xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");   
            }catch(e1){   
                try{   
                    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");   
                }catch(e2){   
                    alert("error");   
                }   
            }   
        }else if(window.XMLHttpRequest){   
            xmlHttp=new XMLHttpRequest();   
        }   
    } 
    
	function getPDF(pdfCount){
	   var ps = "method=TH_PDF&pdfCount="+pdfCount; 
       ps = encodeURI(ps);
 	   ps = encodeURI(ps);
       createXMLHttpRequest();  
       xmlHttp.open("POST", "XmlEditSubmit.do", true);  
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
       xmlHttp.onreadystatechange=processPDF;  
       xmlHttp.send(ps);
       
	}
	
	 function processPDF(){   
    
       if(xmlHttp.readyState==4){ 
          
           if(xmlHttp.status==200){   
           
               result=xmlHttp.responseText;
               if (result == 1) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/TH/TH_PDF1/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
               
			   if (result == 2) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/TH/TH_PDF2/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
               
               if (result == 3) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/RU/RU_PDF1/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
               
               if (result == 4) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/RU/RU_PDF2/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
               
                if (result == 5) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/VN/VN_PDF1/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
               
               if (result == 6) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/VN/VN_PDF2/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
               
               if (result == 7) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/MY/MY_PDF1/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
               
               if (result == 8) {
               	 document.getElementById("showPDF").innerHTML = "<embed id=e src='/xmlEditorPdf/pdf/MY/MY_PDF2/"+(pdfNumber)+".pdf' width='100%' height='1000' type='application/pdf' />";
               }
                 
           }
               
       }
        	   
    } 
--></script>
