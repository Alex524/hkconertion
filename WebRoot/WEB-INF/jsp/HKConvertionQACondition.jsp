<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/lscd-tags.tld" prefix="lscd"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
	<title>Lighthouse Software (Chengdu) Ltd. Co.</title>
	<link rel=stylesheet type="text/css" href="css/LSCD.css" />
	<link rel=stylesheet type="text/css" href="css/br.css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript">
		function setDate(ele){
			var obj = document.getElementById(ele);
			if(obj != null){
				obj.value = showModalDialog("calendar.htm", "yyyy-mm-dd" ,"dialogWidth:286px;dialogHeight:221px;status:no;help:no;");
			}
		}
		
		function sub(){
			return false;
		}
		
		
	
	var xmlHttp;   
    var result;   
    function createXMLHttpRequest(){   
        if(window.ActiveXObject){   
            try{   
                xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");   
            }catch(e1){   
                try{   
                    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");   
                }catch(e2){   
                    alert("error");   
                }   
            }   
        }else if(window.XMLHttpRequest){   
            xmlHttp=new XMLHttpRequest();   
        }   
    } 
    
	function checkFileExist(){
    	
       var ps = "method=init&countryS=file&filename="+document.getElementById("number").value;
       ps = encodeURI(ps);
 	   ps = encodeURI(ps);
       createXMLHttpRequest();   
       xmlHttp.open("POST", "XmlEdit.do", true);  
       xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded;");
       xmlHttp.onreadystatechange=processFile;  
       xmlHttp.send(ps);
       
    }
    
    function processFile(){   
    
       if(xmlHttp.readyState==4){ 
          
           if(xmlHttp.status==200){   
           
               result=xmlHttp.responseText;
               alert(result);
                 
           }
               
       }
        	   
    }
    
    function disable(obj){
			if(obj=="exist"){
				document.getElementById("existNumberList").disabled = false;
				
				document.getElementById("notExistNumberList").disabled = true;
			}
			if(obj=="notExist"){
				document.getElementById("notExistNumberList").disabled = false;
				
				document.getElementById("existNumberList").disabled = true;
			}
		}
		
	function validate(){
		if (document.getElementById("existNumberList").disabled == true){
			alert("not pdf");
			return false;
		} else if(document.getElementById("exitsFile").value != ""){
			alert("please choose exist number");
			return false;
		} else {
			document.HKConvertionConditionForm.submit();
		}
	} 
	</script>
</head>

<body>
	<form action="HKConvertionQACondition.do?lockExistNumber=null" method="post" name="HKConvertionConditionForm">
		<table width="100%" cellspacing="5" cellpadding="15" border="0">
			<tr>
				<td class="MenuTable">
					<font size="3"><b>OPEN XML FILE</b> </font>
				</td>
			</tr>
			<tr>
				<td>
					<table border="1" cellpadding="5" cellspacing="5" width="20%">
						<tr>
							<td class="MenuTable">
								Country
							</td>
							<td colspan="2">
								<input type="text" id="country" name="country" size="5" value="<%=request.getSession().getAttribute("xmlcountry") %>" readOnly />
							</td>
						</tr>
						<tr>
							<td class="MenuTable">
								<logic:empty name="submitList">
									<input name="kind" id="exist" value="exist" type="radio" onclick="disable(this.value)" disabled/>
								</logic:empty>
								<logic:notEmpty name="submitList">
									<input name="kind" id="exist" value="exist" type="radio" onclick="disable(this.value)" />
								</logic:notEmpty>
							</td>
							<td colspan="2">
								<logic:empty name="submitList">
									<select name="number" id="existNumberList" style="width:140px;" disabled>
											<option value='' id="number" name="number">
											</option>
									</select>
								</logic:empty>
								<logic:notEmpty name="submitList">
									<select name="number" id="existNumberList">
										<logic:iterate id="existNumber" name="submitList" type="com.lighthouse.translation.beans.SplitTxt">
											<option value='<bean:write name="existNumber" property="pubNumber"/>' id="number" name="number">
												<bean:write name="existNumber" property="pubNumber"/>
											</option>
										</logic:iterate>
									</select>
								</logic:notEmpty>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<logic:notEmpty name="lockPubNumber">
						<table>
							<tr>
								<td><span style="color:#f00000">lock number:</span></td>
								<td><%=request.getAttribute("lockPubNumber").toString() %></td>
							</tr>
						</table>
					</logic:notEmpty>
				
					<logic:notEmpty name="pubNumber">
						<table>
							<tr>
								<td><span style="color:#f00000">exist number:</span></td>
								<td><a href="HKConvertionQACondition.do?lockExistNumber=<%=request.getAttribute("pubNumber").toString() %>"><%=request.getAttribute("pubNumber").toString() %></a></td>
							</tr>
						</table>
					</logic:notEmpty>
				</td>
			</tr>
			<tr>
				<td>
					<input id="sub" type="button" value="Submit" class="inputbutton" onclick="validate()" />
					<input type="hidden" name="exitsFile" value="<%=request.getAttribute("pubNumber").toString() %>" id="exitsFile" />
				</td>
			</tr>
		</table>

	</form>
</body>
</html:html>