<html>
	<head>
		<title>My Word</title>
		<link rel=stylesheet type="text/css" href="css/LSCD.css" />
		<link rel=stylesheet type="text/css" href="css/br.css" />
		<script type="text/javascript">
		var doc;
		
		function doNew(){
			// IsDirty :whether changed.
			if(doc!=null && doc.IsDirty){
				if(confirm("Document has been modified.\nCreate a new word document.\nAre you sure?")){
					createNew();
				}
			}else{
				createNew();
			}
		}
		
		function createNew(){
			doc = document.getElementById("DSOFramer");
			doc.CreateNew("Word.Document");
		}
		
		function doClose(i){
			if(i==1){
				if(doc!=null){
					if(doc.IsDirty){
						if(confirm("Document has been modified. Save changes?")){
								saveDoc();
								closeDoc();
						}else{
							closeDoc();
						}
					}else{
						closeDoc();
					}
				}
			}else if(i==0){
				if(doc!=null){
					closeDoc();
				}
			}
		}
		
		function closeDoc(){
			doc.Close();
			doc=null;
		}
		
		function doSave(){
			if(doc!=null){
				saveDoc();
			}
		}
		
		function saveDoc(){
			if(doc!=null){
				return doc.ActiveDocument.Application.Dialogs(84).Show();
			}
		}
		</script>
	</head>
	<body bottommargin="0" topmargin="0" rightmargin="0" leftmargin="0"
		scroll="no" onunload="doClose(0)" class="HomeBody">
		<table height="100%" width="100%" border="1" cellspacing="0"
			bottommargin="0" topmargin="0" rightmargin="0" leftmargin="0"
			cellpadding="0">
			<tr>
				<td>
					&nbsp;&nbsp;&nbsp;<input type="button" value="New" class="inputbutton" onclick="doNew()" />
					&nbsp;&nbsp;&nbsp;<input type="button" value="Save" class="inputbutton" onclick="doSave()">
					&nbsp;&nbsp;&nbsp;<input type="button" value="Close" class="inputbutton" onclick="doClose(1)">
				</td>
			</tr>
			<tr height="99%">
				<td>
					<OBJECT id="DSOFramer" align="center"
						style="LEFT: 0px; WIDTH: 100%; TOP: 0px; HEIGHT: 100%"
						classid="clsid:00460182-9E5E-11D5-B7C8-B8269041DD57"
						codeBase="ocx/dsoframer.ocx#version=2,2,1,2">
						<param name="BorderStyle" value="0">
						<param name="Titlebar" value="0">
						<param name="Menubar" value="0">
					</OBJECT>
				</td>
			</tr>
		</table>
	</body>
</html>
