package com.lighthouse.translation.beans;

public class LockNumber implements java.io.Serializable {
	// Fields
	private String pubNumber;
	private String userName;
	private String userGroup;
	private String state;
	
	public LockNumber(String pubNumber, String userName, String userGroup, String state) {
		super();
		this.pubNumber = pubNumber;
		this.userName = userName;
		this.userGroup = userGroup;
		this.state = state;
	}
	
	public LockNumber() {
		
	}
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	
	public String getPubNumber() {
		return this.pubNumber;
	}

	public void setPubNumber(String pubNumber) {
		this.pubNumber = pubNumber;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}
