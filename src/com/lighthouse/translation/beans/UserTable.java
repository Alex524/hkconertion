package com.lighthouse.translation.beans;

public class UserTable implements java.io.Serializable {
	// Fields
	private Long id;
	private String userName;
	private String password;
	private String country;
	private String usergroup;
	
	public UserTable(Long id, String userName, String password, String country, String usergroup) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.country = country;
		this.usergroup = usergroup;
	}
	
	public UserTable() {
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getUsergroup() {
		return this.usergroup;
	}

	public void setUsergroup(String usergroup) {
		this.usergroup = usergroup;
	}
}
