package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.plugin.HibernatePlugIn;
import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDao;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDaoimp;
import com.lighthouse.translation.hkconvertion.dao.PdfConditionDao;
import com.lighthouse.translation.hkconvertion.dao.PdfConditionDaoimp;
import com.lighthouse.translation.hkconvertion.dao.PdfShowDao;
import com.lighthouse.translation.hkconvertion.dao.PdfShowDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDao;
import com.lighthouse.translation.hkconvertion.xml.OutTranslationXml;
import com.lighthouse.translation.hkconvertion.HKConvertionConditionForm;

public class HKConvertionEditorConditionAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HKConvertionConditionForm sform = (HKConvertionConditionForm)form;
		Session session = null;
		SessionFactory factory = null;
		Transaction transaction = null;
		String pubNumber = request.getParameter("number");
		String userName = request.getSession().getAttribute("username").toString();
		String lockExistNumber = request.getParameter("lockExistNumber");
		
		try {
			
			if (lockExistNumber.equals("null")) {
				pubNumber = request.getParameter("number");
			} else {
				pubNumber = request.getParameter("lockExistNumber");
			}
			
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			transaction = session.beginTransaction();
			XmlEditBean xmlBean = new XmlEditBean();
			PdfConditionDao dao = PdfConditionDaoimp.getInstance();
			boolean bool = dao.isReopen(session, pubNumber, userName);
			EditorSubmitDao editorSD = EditorSubmitDaoimp.getInstance();
			String pdfName = editorSD.getPdfName(session, pubNumber);
			PdfShowDao  pdfDao = PdfShowDaoimp.getInstance();
			if (bool) {
				//获得pdfName
				String folderPath = CommonConsts.XML_PATH_Editor;
				File file = new File(folderPath+pdfName+"\\"+pubNumber+".xml");
				OutTranslationXml outXml = new OutTranslationXml();
				xmlBean = outXml.parseXML(file, "false");
				
			} else {
				boolean boolLock = pdfDao.selectUserNameLock(session, pubNumber, userName, "editor");
				if (!boolLock) {
					response.sendRedirect("/HKConvertion/HKConvertionEditorPanel.do?lockPubNumber="+pubNumber);
					return null;
				}
				int lock = 0;
				//锁定文章
				lock = pdfDao.updateLock(session, pubNumber, userName, "editor");
				
				if (lock>0) {
					transaction.commit();
				} else {
					transaction.rollback();
				}
				xmlBean = dao.getSplitTxtBean(session, pubNumber);
			}
			
			if (xmlBean.getB541()==null){
				xmlBean.setB541("EN");
			}
			
			if (xmlBean.getB250() == null){
				xmlBean.setB250("ZH");
			} else if(xmlBean.getB250().equals("CH")){
				xmlBean.setB250("ZH");
			}
			
			
			String type = pdfDao.getPdf(session, pubNumber);
			request.setAttribute("countryName", "HK");
			request.setAttribute("type", type);
			request.setAttribute("pdfName", pdfName);
			request.setAttribute("pubNumber", pubNumber);
			request.setAttribute("xmlBean", xmlBean);
			return mapping.findForward("pdfedit");
		}catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			return mapping.findForward("exception");
		} finally{
			
			if (session != null){
				session.close();
			}
			
			if (factory != null) {
				factory.close();
			}
			
		}
		
	}
}