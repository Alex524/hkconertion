package com.lighthouse.translation.hkconvertion.beans;

public class DescriptionsBean {
	private String description;
	
	public DescriptionsBean(){}
	
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
