package com.lighthouse.translation.hkconvertion.beans;

public class B512Bean {

	private String b512Class;
	private String b512Version;
	
	public B512Bean(){}
	
	public String getB512Class() {
		return this.b512Class;
	}

	public void setB512Class(String b512Class) {
		this.b512Class = b512Class;
	}
	
	public String getB512Version() {
		return this.b512Version;
	}

	public void setB512Version(String b512Version) {
		this.b512Version = b512Version;
	}
}
