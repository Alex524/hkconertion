package com.lighthouse.translation.hkconvertion.beans;

public class B562Bean {
	private String b562Reference;
	
	public B562Bean(){}
	
	public String getB562Reference() {
		return this.b562Reference;
	}

	public void setB562Reference(String b562Reference) {
		this.b562Reference = b562Reference;
	}
}
