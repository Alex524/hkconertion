package com.lighthouse.translation.hkconvertion.beans;

public class SplitTxtBean {
	private Long id;
	private String pdfName;
	private String pubNumber;
	private String pubKind;
	private String appNumber;
	private String appLanguage;
	private String appDate;
	private String ipc;
	private String ipcClass;
	private String woAppDate;
	private String woAppNumber;
	private String woPublDate;
	private String woPublNumber;
	private String priDate;
	private String priCountry;
	private String priNumber;
	private String title;
	private String appName;
	private String invName;
	private String granName;
	private String attName;
	private String divDnum;
	private String divDate;
	private String refCountry;
	private String refNumber;
	private String editorId;
	private String qaId;
	private String status;
	private String lock;
	private String type;
	private String publDate;
	private String grantDate;
	
	
	public SplitTxtBean() {
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPdfName() {
		return pdfName;
	}
	
	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
	}
	
	public String getPubNumber() {
		return pubNumber;
	}
	
	public void setPubNumber(String pubNumber) {
		this.pubNumber = pubNumber;
	}
	
	public String getPubKind() {
		return pubKind;
	}
	public void setPubKind(String pubKind) {
		this.pubKind = pubKind;
	}
	
	public String getAppNumber() {
		return appNumber;
	}
	public void setAppNumber(String appNumber) {
		this.appNumber = appNumber;
	}
	
	public String getAppLanguage() {
		return appLanguage;
	}
	public void setAppLanguage(String appLanguage) {
		this.appLanguage = appLanguage;
	}
	
	public String getAppDate() {
		return appDate;
	}
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
	public String getIpc() {
		return ipc;
	}
	public void setIpc(String ipc) {
		this.ipc = ipc;
	}
	
	public String getIpcClass() {
		return ipcClass;
	}
	public void setIpcClass(String ipcClass) {
		this.ipcClass = ipcClass;
	}
	
	public String getWoAppDate() {
		return woAppDate;
	}
	public void setWoAppDate(String woAppDate) {
		this.woAppDate = woAppDate;
	}
	
	public String getWoAppNumber() {
		return woAppNumber;
	}
	public void setWoAppNumber(String woAppNumber) {
		this.woAppNumber = woAppNumber;
	}
	
	public String getWoPublDate() {
		return woPublDate;
	}
	public void setWoPublDate(String woPublDate) {
		this.woPublDate = woPublDate;
	}
	
	public String getWoPublNumber() {
		return woPublNumber;
	}
	public void setWoPublNumber(String woPublNumber) {
		this.woPublNumber = woPublNumber;
	}
	
	public String getPriDate() {
		return priDate;
	}
	public void setPriDate(String priDate) {
		this.priDate = priDate;
	}
	
	public String getPriCountry() {
		return priCountry;
	}
	public void setPriCountry(String priCountry) {
		this.priCountry = priCountry;
	}
	
	public String getPriNumber() {
		return priNumber;
	}
	public void setPriNumber(String priNumber) {
		this.priNumber = priNumber;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getInvName() {
		return invName;
	}
	public void setInvName(String invName) {
		this.invName = invName;
	}
	
	public String getGranName() {
		return granName;
	}
	public void setGranName(String granName) {
		this.granName = granName;
	}
	
	public String getAttName() {
		return attName;
	}
	public void setAttName(String attName) {
		this.attName = attName;
	}
	
	public String getDivDnum() {
		return divDnum;
	}
	public void setDivDnum(String divDnum) {
		this.divDnum = divDnum;
	}
	
	public String getDivDate() {
		return divDate;
	}
	public void setDivDate(String divDate) {
		this.divDate = divDate;
	}
	
	public String getRefCountry() {
		return refCountry;
	}
	public void setRefCountry(String refCountry) {
		this.refCountry = refCountry;
	}
	
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	
	public String getEditorId() {
		return editorId;
	}
	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}
	
	public String getQaId() {
		return qaId;
	}
	public void setQaId(String qaId) {
		this.qaId = qaId;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getLock() {
		return lock;
	}
	public void setLock(String lock) {
		this.lock = lock;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getPublDate() {
		return publDate;
	}
	public void setPublDate(String publDate) {
		this.publDate = publDate;
	}
	
	public String getGrantDate() {
		return grantDate;
	}
	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}
	
}
