package com.lighthouse.translation.hkconvertion.beans;

public class FtpBean {
	private String ftpNumber;
	
	public String getFtpNumber() {
		return this.ftpNumber;
	}

	public void setFtpNumber(String ftpNumber) {
		this.ftpNumber = ftpNumber;
	}
}
