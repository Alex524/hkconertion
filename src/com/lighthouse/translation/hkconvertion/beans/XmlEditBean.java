package com.lighthouse.translation.hkconvertion.beans;

import java.util.LinkedHashSet;
import java.util.Set;

public class XmlEditBean implements java.io.Serializable {
	private String b110;
	private String b130;
	private String b210;
	private String b220Year;
	private String b220Month;
	private String b220Day;
	private String b250;
	private String b430Year;
	private String b430Month;
	private String b430Day;
	private String b450Year;
	private String b450Month;
	private String b450Day;
	private String b511Class;
	private String b511Version;
	private String b516;
	private String b541;
	private String b542;
	private String b563;
	private String b620Dnum;
	private String b620Year;
	private String b620Month;
	private String b620Day;
	private String b655Ctry;
	private String b655Pnum;
	private String b655Kind;
	private String b850Year;
	private String b850Month;
	private String b850Day;
	private String b861Anum;
	private String b861Year;
	private String b861Month;
	private String b861Day;
	private String b871Pnum;
	private String b871Year;
	private String b871Month;
	private String b871Day;
	private String abvie;
	private String abtha;
	private String abrus;
	private String abeng;
	private String pdfName;
	
	private Set b300 = new LinkedHashSet(0);
	private Set b512 = new LinkedHashSet(0);
	private Set b561 = new LinkedHashSet(0);
	private Set b711 = new LinkedHashSet(0);
	private Set b721 = new LinkedHashSet(0);
	private Set b731 = new LinkedHashSet(0);
	private Set b741 = new LinkedHashSet(0);
	private Set claims = new LinkedHashSet(0);
	private Set b562 = new LinkedHashSet(0);
	private Set descriptions = new LinkedHashSet(0);
	
	public XmlEditBean(){}
	
	
	public String getB110() {
		return this.b110;
	}

	public void setB110(String b110) {
		this.b110 = b110;
	}
	
	public String getB130() {
		return this.b130;
	}

	public void setB130(String b130) {
		this.b130 = b130;
	}
	
	public String getB210() {
		return this.b210;
	}

	public void setB210(String b210) {
		this.b210 = b210;
	}
	
	public String getB220Year() {
		return this.b220Year;
	}

	public void setB220Year(String b220Year) {
		this.b220Year = b220Year;
	}
	
	public String getB220Month() {
		return this.b220Month;
	}

	public void setB220Month(String b220Month) {
		this.b220Month = b220Month;
	}
	
	public String getB220Day() {
		return this.b220Day;
	}

	public void setB220Day(String b220Day) {
		this.b220Day = b220Day;
	}
	
	public String getB250() {
		return this.b250;
	}

	public void setB250(String b250) {
		this.b250 = b250;
	}
	
	
	
	public String getB430Year() {
		return this.b430Year;
	}

	public void setB430Year(String b430Year) {
		this.b430Year = b430Year;
	}
	
	public String getB430Month() {
		return this.b430Month;
	}

	public void setB430Month(String b430Month) {
		this.b430Month = b430Month;
	}
	
	public String getB430Day() {
		return this.b430Day;
	}

	public void setB430Day(String b430Day) {
		this.b430Day = b430Day;
	}
	
	public String getB450Year() {
		return this.b450Year;
	}

	public void setB450Year(String b450Year) {
		this.b450Year = b450Year;
	}
	
	public String getB450Month() {
		return this.b450Month;
	}

	public void setB450Month(String b450Month) {
		this.b450Month = b450Month;
	}
	
	public String getB450Day() {
		return this.b450Day;
	}

	public void setB450Day(String b450Day) {
		this.b450Day = b450Day;
	}
	
	public String getB511Class() {
		return this.b511Class;
	}

	public void setB511Class(String b511Class) {
		this.b511Class = b511Class;
	}
	
	public String getB511Version() {
		return this.b511Version;
	}

	public void setB511Version(String b511Version) {
		this.b511Version = b511Version;
	}
	
	public String getB516() {
		return this.b516;
	}

	public void setB516(String b516) {
		this.b516 = b516;
	}
	
	public String getB541() {
		return this.b541;
	}

	public void setB541(String b541) {
		this.b541 = b541;
	}
	
	public String getB542() {
		return this.b542;
	}

	public void setB542(String b542) {
		this.b542 = b542;
	}
	
	public String getB563() {
		return this.b563;
	}

	public void setB563(String b563) {
		this.b563 = b563;
	}
	
	public String getB620Dnum() {
		return this.b620Dnum;
	}

	public void setB620Dnum(String b620Dnum) {
		this.b620Dnum = b620Dnum;
	}
	
	public String getB620Year() {
		return this.b620Year;
	}

	public void setB620Year(String b620Year) {
		this.b620Year = b620Year;
	}
	
	public String getB620Month() {
		return this.b620Month;
	}

	public void setB620Month(String b620Month) {
		this.b620Month = b620Month;
	}
	
	public String getB620Day() {
		return this.b620Day;
	}

	public void setB620Day(String b620Day) {
		this.b620Day = b620Day;
	}
	
	public String getB655Ctry() {
		return this.b655Ctry;
	}

	public void setB655Ctry(String b655Ctry) {
		this.b655Ctry = b655Ctry;
	}
	
	public String getB655Pnum() {
		return this.b655Pnum;
	}

	public void setB655Pnum(String b655Pnum) {
		this.b655Pnum = b655Pnum;
	}
	
	public String getB655Kind() {
		return this.b655Kind;
	}

	public void setB655Kind(String b655Kind) {
		this.b655Kind = b655Kind;
	}
	
	public String getB850Year() {
		return this.b850Year;
	}

	public void setB850Year(String b850Year) {
		this.b850Year = b850Year;
	}
	
	public String getB850Month() {
		return this.b850Month;
	}

	public void setB850Month(String b850Month) {
		this.b850Month = b850Month;
	}
	
	public String getB850Day() {
		return this.b850Day;
	}

	public void setB850Day(String b850Day) {
		this.b850Day = b850Day;
	}
	
	public String getB861Anum() {
		return this.b861Anum;
	}

	public void setB861Anum(String b861Anum) {
		this.b861Anum = b861Anum;
	}
	
	
	public String getB861Year() {
		return this.b861Year;
	}

	public void setB861Year(String b861Year) {
		this.b861Year = b861Year;
	}
	
	public String getB861Month() {
		return this.b861Month;
	}

	public void setB861Month(String b861Month) {
		this.b861Month = b861Month;
	}
	
	public String getB861Day() {
		return this.b861Day;
	}

	public void setB861Day(String b861Day) {
		this.b861Day = b861Day;
	}
	
	public String getB871Pnum() {
		return this.b871Pnum;
	}

	public void setB871Pnum(String b871Pnum) {
		this.b871Pnum = b871Pnum;
	}
	
	public String getB871Year() {
		return this.b871Year;
	}

	public void setB871Year(String b871Year) {
		this.b871Year = b871Year;
	}
	
	public String getB871Month() {
		return this.b871Month;
	}

	public void setB871Month(String b871Month) {
		this.b871Month = b871Month;
	}
	
	public String getB871Day() {
		return this.b871Day;
	}

	public void setB871Day(String b871Day) {
		this.b871Day = b871Day;
	}
	
	public String getAbvie() {
		return this.abvie;
	}

	public void setAbvie(String abvie) {
		this.abvie = abvie;
	}
	
	public String getAbtha() {
		return this.abtha;
	}

	public void setAbtha(String abtha) {
		this.abtha = abtha;
	}
	
	public String getAbrus() {
		return this.abrus;
	}

	public void setAbrus(String abrus) {
		this.abrus = abrus;
	}
	
	public String getAbeng() {
		return this.abeng;
	}

	public void setAbeng(String abeng) {
		this.abeng = abeng;
	}
	
	public String getPdfName() {
		return this.pdfName;
	}

	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
	}
	
	/*set*/
	
	public Set getB300() {
		return this.b300;
	}

	public void setB300(Set b300) {
		this.b300 = b300;
	}
	
	public Set getB512() {
		return this.b512;
	}

	public void setB512(Set b512) {
		this.b512 = b512;
	}
	
	public Set getB561() {
		return this.b561;
	}

	public void setB561(Set b561) {
		this.b561 = b561;
	}
	
	public Set getB711() {
		return this.b711;
	}

	public void setB711(Set b711) {
		this.b711 = b711;
	}
	
	public Set getB721() {
		return this.b721;
	}

	public void setB721(Set b721) {
		this.b721 = b721;
	}
	
	public Set getB731() {
		return this.b731;
	}

	public void setB731(Set b731) {
		this.b731 = b731;
	}
	
	public Set getB741() {
		return this.b741;
	}

	public void setB741(Set b741) {
		this.b741 = b741;
	}
	
	public Set getClaims() {
		return this.claims;
	}

	public void setClaims(Set claims) {
		this.claims = claims;
	}
	
	public Set getB562() {
		return this.b562;
	}

	public void setB562(Set b562) {
		this.b562 = b562;
	}
	
	public Set getDescriptions() {
		return this.descriptions;
	}

	public void setDescriptions(Set descriptions) {
		this.descriptions = descriptions;
	}
	
	
	
}
