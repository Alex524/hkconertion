package com.lighthouse.translation.hkconvertion.beans;

public class B561Bean {

	private String b561Ctry;
	private String b561Dnum;
	private String b561Kind;
	private String b561Snm;
	
	public B561Bean(){}
	
	public String getB561Ctry() {
		return this.b561Ctry;
	}

	public void setB561Ctry(String b561Ctry) {
		this.b561Ctry = b561Ctry;
	}
	
	public String getB561Dnum() {
		return this.b561Dnum;
	}

	public void setB561Dnum(String b561Dnum) {
		this.b561Dnum = b561Dnum;
	}
	
	public String getB561Kind() {
		return this.b561Kind;
	}

	public void setB561Kind(String b561Kind) {
		this.b561Kind = b561Kind;
	}
	
	public String getB561Snm() {
		return this.b561Snm;
	}

	public void setB561Snm(String b561Snm) {
		this.b561Snm = b561Snm;
	}
}
