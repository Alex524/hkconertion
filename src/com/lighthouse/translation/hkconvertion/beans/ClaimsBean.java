package com.lighthouse.translation.hkconvertion.beans;

public class ClaimsBean {

	private String claimsClm;
	
	public ClaimsBean(){}
	
	public String getClaimsClm() {
		return this.claimsClm;
	}

	public void setClaimsClm(String claimsClm) {
		this.claimsClm = claimsClm;
	}
}
