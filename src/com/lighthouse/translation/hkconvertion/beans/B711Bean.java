package com.lighthouse.translation.hkconvertion.beans;

public class B711Bean {

	private String b711Snm;
	private String b711Str;
	private String b711City;
	private String b711Ctry;
	public B711Bean(){}
	
	public String getB711Snm() {
		return this.b711Snm;
	}

	public void setB711Snm(String b711Snm) {
		this.b711Snm = b711Snm;
	}
	
	public String getB711Str() {
		return this.b711Str;
	}

	public void setB711Str(String b711Str) {
		this.b711Str = b711Str;
	}
	
	public String getB711City() {
		return this.b711City;
	}

	public void setB711City(String b711City) {
		this.b711City = b711City;
	}
	
	public String getB711Ctry() {
		return this.b711Ctry;
	}

	public void setB711Ctry(String b711Ctry) {
		this.b711Ctry = b711Ctry;
	}
	
	
}
