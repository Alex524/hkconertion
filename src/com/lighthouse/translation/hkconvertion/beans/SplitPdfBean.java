package com.lighthouse.translation.hkconvertion.beans;

public class SplitPdfBean {
	private String startPage_applications;
	private String endPage_applications;
	private String startPage_grants;
	private String endPage_grants;
	private String startPage_shortGrants;
	private String endPage_shortGrants;
	
	public SplitPdfBean(){}
	
	public String getStartPage_applications() {
		return this.startPage_applications;
	}

	public void setStartPage_applications(String startPage_applications) {
		this.startPage_applications = startPage_applications;
	}
	
	public String getEndPage_applications() {
		return this.endPage_applications;
	}

	public void setEndPage_applications(String endPage_applications) {
		this.endPage_applications = endPage_applications;
	}
	
	public String getStartPage_grants() {
		return this.startPage_grants;
	}

	public void setStartPage_grants(String startPage_grants) {
		this.startPage_grants = startPage_grants;
	}
	
	public String getEndPage_grants() {
		return this.endPage_grants;
	}

	public void setEndPage_grants(String endPage_grants) {
		this.endPage_grants = endPage_grants;
	}
	
	public String getStartPage_shortGrants() {
		return this.startPage_shortGrants;
	}

	public void setStartPage_shortGrants(String startPage_shortGrants) {
		this.startPage_shortGrants = startPage_shortGrants;
	}
	
	public String getEndPage_shortGrants() {
		return this.endPage_shortGrants;
	}

	public void setEndPage_shortGrants(String endPage_shortGrants) {
		this.endPage_shortGrants = endPage_shortGrants;
	}
	
}
