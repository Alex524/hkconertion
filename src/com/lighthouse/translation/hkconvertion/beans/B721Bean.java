package com.lighthouse.translation.hkconvertion.beans;

public class B721Bean {
	private String b721Snm;
	private String b721Str;
	private String b721City;
	private String b721Ctry;
	
	public B721Bean(){}
	
	public String getB721Snm() {
		return this.b721Snm;
	}

	public void setB721Snm(String b721Snm) {
		this.b721Snm = b721Snm;
	}
	
	public String getB721Str() {
		return this.b721Str;
	}

	public void setB721Str(String b721Str) {
		this.b721Str = b721Str;
	}
	
	public String getB721City() {
		return this.b721City;
	}

	public void setB721City(String b721City) {
		this.b721City = b721City;
	}
	
	public String getB721Ctry() {
		return this.b721Ctry;
	}

	public void setB721Ctry(String b721Ctry) {
		this.b721Ctry = b721Ctry;
	}
	
	
}
