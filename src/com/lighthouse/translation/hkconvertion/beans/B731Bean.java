package com.lighthouse.translation.hkconvertion.beans;

public class B731Bean {

	private String b731Snm;
	private String b731Str;
	private String b731City;
	private String b731Ctry;
	public B731Bean(){}
	
	public String getB731Snm() {
		return this.b731Snm;
	}

	public void setB731Snm(String b731Snm) {
		this.b731Snm = b731Snm;
	}
	
	public String getB731Str() {
		return this.b731Str;
	}

	public void setB731Str(String b731Str) {
		this.b731Str = b731Str;
	}
	
	public String getB731City() {
		return this.b731City;
	}

	public void setB731City(String b731City) {
		this.b731City = b731City;
	}
	
	public String getB731Ctry() {
		return this.b731Ctry;
	}

	public void setB731Ctry(String b731Ctry) {
		this.b731Ctry = b731Ctry;
	}
}
