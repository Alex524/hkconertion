package com.lighthouse.translation.hkconvertion.beans;

public class B300Bean {
	
	private String b310;
	private String b320Year;
	private String b320Month;
	private String b320Day;
	private String b330Ctry;
	
	public B300Bean(){}
	
	public String getB320Year() {
		return this.b320Year;
	}

	public void setB320Year(String b320Year) {
		this.b320Year = b320Year;
	}

	public String getB320Month() {
		return this.b320Month;
	}

	public void setB320Month(String b320Month) {
		this.b320Month = b320Month;
	}

	public String getB320Day() {
		return this.b320Day;
	}

	public void setB320Day(String b320Day) {
		this.b320Day = b320Day;
	}

	public String getB330Ctry() {
		return this.b330Ctry;
	}

	public void setB330Ctry(String b330Ctry) {
		this.b330Ctry = b330Ctry;
	}

	public String getB310() {
		return this.b310;
	}

	public void setB310(String b310) {
		this.b310 = b310;
	}
	
	
}
