package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.plugin.HibernatePlugIn;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;
import com.lighthouse.translation.hkconvertion.dao.EditFile;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDao;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDaoimp;
import com.lighthouse.translation.hkconvertion.dao.XmlBeanDao;
import com.lighthouse.translation.hkconvertion.dao.XmlBeanDaoimp;
import com.lighthouse.translation.hkconvertion.xml.InTranslationXml;

public class HKConvertionQASubmitAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String method = request.getParameter("method");
		Session session = null;
		SessionFactory factory = null;
		Transaction transaction = null;
		Connection conn = null;
		try {
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			transaction = session.beginTransaction();
			conn = session.connection();
			XmlEditBean xmlBean = new XmlEditBean();
			String pubNumber = request.getParameter("b110").toString().trim()+request.getParameter("b130").toString().trim();
			EditorSubmitDao editorSD = EditorSubmitDaoimp.getInstance();
			//获得pdfName
			String pdfName = editorSD.getPdfName(session, pubNumber);
			
			if (method.equals("QAReopen")) {
				
				int reopenStatus = 0;
				int lock = 0;
				//更新status状态为reopen
				reopenStatus = editorSD.updateStatus(session, "reopen", pubNumber);
				lock = editorSD.updateQALock(session, pubNumber);
				
				if (reopenStatus>0 && lock>0) {
					transaction.commit();
				} else {
					transaction.rollback();
				}
				
			}
			
			if (method.equals("QASubmit")) {
				
				XmlBeanDao dao = XmlBeanDaoimp.getInstance();
				
				//把从页面获得的对象填充进xmlBean
				xmlBean = dao.getXmlBean(request, xmlBean);
				InTranslationXml xml = new InTranslationXml();
				
				String xmlPath = CommonConsts.XML_PATH_Editor;
				String XmlValidPath = CommonConsts.XML_VALID_PATH_QA+pdfName;
				String XMLNewValidPath =  CommonConsts.XML_NEWVALID_PATH_QA+pdfName;
				String XMLBackUpPath =  CommonConsts.XML_BACKUP+pdfName;
				String tempXmlPath = xmlPath+pdfName;
				EditFile editFile = new EditFile();
				editFile.newFolder(XmlValidPath);
				editFile.newFolder(XMLNewValidPath);
				editFile.newFolder(XMLBackUpPath);
				
				//生成新的xml文件
				boolean succ = xml.parseXML(xmlBean, tempXmlPath);
				editFile.copyFile(tempXmlPath+"\\"+xmlBean.getB110()+xmlBean.getB130() + ".xml", XmlValidPath+"\\"+xmlBean.getB110()+xmlBean.getB130() + ".xml");
				editFile.copyFile(tempXmlPath+"\\"+xmlBean.getB110()+xmlBean.getB130() + ".xml", XMLNewValidPath+"\\"+xmlBean.getB110()+xmlBean.getB130() + ".xml");
				editFile.copyFile(tempXmlPath+"\\"+xmlBean.getB110()+xmlBean.getB130() + ".xml", XMLBackUpPath+"\\"+xmlBean.getB110()+xmlBean.getB130() + ".xml");
				editFile.deleteFile(tempXmlPath+"\\"+xmlBean.getB110()+xmlBean.getB130() + ".xml");
				
				int subStatus = 0;
				int i = 0;
				subStatus = editorSD.updateStatus(session, "closed", pubNumber);
				i = editorSD.updateQADate(session, pubNumber);
				//boolean deletePubNumber = editorSD.deleteLockOut(conn, pubNumber);
				//if (deletePubNumber) {
				//	transaction.commit();
				//}else{
				//	transaction.rollback();
				//}
				if (subStatus>0&&i>0) {
					transaction.commit();
				} else {
					transaction.rollback();
				}
				
				
			}
			
			//nextPage
			
				response.sendRedirect("/HKConvertion/XmlQACreateNext.do?pdfName="+pdfName);
				return null;
			
		}catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			return mapping.findForward("exception");
		} finally{
			
			if (conn != null){
				conn.close();
			}
			
			if (session != null){
				session.close();
			}
			
			if (factory != null) {
				factory.close();
			}
			
		}
		
	}
	
}
