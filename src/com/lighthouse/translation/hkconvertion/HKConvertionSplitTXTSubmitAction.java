package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.FtpBean;
import com.lighthouse.translation.hkconvertion.beans.SplitPdfBean;
import com.lighthouse.translation.hkconvertion.dao.EditFile;
import com.lighthouse.translation.hkconvertion.dao.PdfSplit;
import com.lighthouse.translation.hkconvertion.dao.SplitPDFDao;
import com.lighthouse.translation.hkconvertion.dao.SplitPDFDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtReadDao;
import com.lighthouse.translation.hkconvertion.dao.TxtReadDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtSplitDao;
import com.lighthouse.translation.hkconvertion.dao.TxtSplitDaoimp;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionSplitTXTSubmitAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
			String listEncryptPDF = request.getParameter("listEncryptPDF");
			Session session = null;
			Transaction transaction = null;
			SessionFactory factory = null;
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			transaction = session.beginTransaction();
			
			try {
				
				//分别解析成以TXT格式的4个list文件。
				TxtSplitDao txtDao = TxtSplitDaoimp.getInstance();
				String fileName = listEncryptPDF+".txt";
				String filePath = CommonConsts.TXT_PATH;
				String file = filePath+"temp\\"+listEncryptPDF+".txt";
				String applicationsFilePath = filePath+"temp\\applications.txt";
				String grantsFilePath = filePath+"temp\\grants.txt";
				String shortGrantsFilePath = filePath+"temp\\shortGrants.txt";
				txtDao.delExcessTxt(fileName, filePath);
				txtDao.createDirectoryTxt(file, applicationsFilePath, grantsFilePath, shortGrantsFilePath);
				txtDao.createListTxt(applicationsFilePath, "applications");
				txtDao.createListTxt(grantsFilePath, "grants");
				txtDao.createListTxt(shortGrantsFilePath, "shortGrants");
				
				//合并3类文件
				
				txtDao.mergeListTxt(filePath+"temp\\list1.txt", "list1");
				txtDao.mergeListTxt(filePath+"temp\\list2.txt", "list2");
				txtDao.mergeListTxt(filePath+"temp\\list3.txt", "list3");
				txtDao.mergeListTxt(filePath+"temp\\list4.txt", "list4");
				
				//获取PubNumber列表
				
				String applicationsFile = filePath+"temp\\applicationslist2.txt";
				String grantsFile = filePath+"temp\\grantslist2.txt";
				String shortGrantsFile = filePath+"temp\\shortGrantslist2.txt";
				TxtReadDao txtReadDao = TxtReadDaoimp.getInstance();
				List list = txtReadDao.getFilePath(applicationsFile, grantsFile, shortGrantsFile);
				
				//存储list列表对应信息到数据库
				boolean succ = txtReadDao.addTxt(session, list, filePath+"temp\\list1.txt", filePath+"temp\\list2.txt", filePath+"temp\\list3.txt", filePath+"temp\\list4.txt", listEncryptPDF);
				if (succ) {
					String fileDirectoryPath = CommonConsts.TXT_PATH+"temp\\";
					EditFile editFile = new EditFile();
					editFile.deleteDirectoryFile(fileDirectoryPath);
					transaction.commit();
				} else {
					transaction.rollback();
				}
				
				SplitPDFDao dao = SplitPDFDaoimp.getInstance();
				ArrayList listSplitPdf = dao.getSplitPDFNameList();
				ArrayList listTxt = dao.getTxtPdfNameList(session);
				ArrayList<FtpBean> listPDF = dao.getTxtNameList(listSplitPdf, listTxt);
			
				request.setAttribute("listPDF", listPDF);
				request.setAttribute("getPdfName", "no");
				
			} catch (Exception e) {
				e.printStackTrace();
				if (transaction != null) {
					transaction.rollback();
				}
				request.setAttribute("exception", e.getMessage());

				return mapping.findForward("exception");
			} finally {
					if (session != null)
						session.close();

					if (factory != null)
						factory.close();

			}
			
		
		
			return mapping.findForward("success");
			
	}
}