package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDao;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDaoimp;
import com.lighthouse.translation.hkconvertion.dao.PdfShowDao;
import com.lighthouse.translation.hkconvertion.dao.PdfShowDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDao;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDaoimp;
import com.lighthouse.translation.hkconvertion.xml.OutTranslationXml;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionQAConditionAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Session session = null;
		SessionFactory factory = null;
		Transaction transaction = null;
		String pubNumber = "";
		String lockExistNumber = request.getParameter("lockExistNumber");
		XmlEditBean xmlBean = new XmlEditBean();
		
		try {
			
			if (lockExistNumber.equals("null")) {
				pubNumber = request.getParameter("number");
			} else {
				pubNumber = request.getParameter("lockExistNumber");
			}
			
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			transaction = session.beginTransaction();
			PdfShowDao  pdfDao = PdfShowDaoimp.getInstance();
			String userName = request.getSession().getAttribute("username").toString();
			boolean bool = pdfDao.selectUserNameLock(session, pubNumber, userName, "QA");
			if (!bool) {
				response.sendRedirect("/HKConvertion/HKConvertionQAPanel.do?lockPubNumber="+pubNumber);
				return null;
			}
			
			
			
			//获得pdfName
			EditorSubmitDao editorSD = EditorSubmitDaoimp.getInstance();
			String pdfName = editorSD.getPdfName(session, pubNumber);
			
			String folderPath = CommonConsts.XML_PATH_Editor;
			File file = new File(folderPath+pdfName+"\\"+pubNumber+".xml");
			OutTranslationXml outXml = new OutTranslationXml();
			xmlBean = outXml.parseXML(file, "false");
			
			String type = pdfDao.getPdf(session, pubNumber);
			
			int lock = 0;
			//锁定文章
			lock = pdfDao.updateLock(session, pubNumber, userName, "QA");
			
			if (lock>0) {
				transaction.commit();
			} else {
				transaction.rollback();
			}
			
			if (xmlBean.getB541()==null){
				xmlBean.setB541("EN");
			}
			
			if (xmlBean.getB250() == null){
				xmlBean.setB250("ZH");
			} else if(xmlBean.getB250().equals("CH")){
				xmlBean.setB250("ZH");
			}
			
			request.setAttribute("countryName", "HK");
			request.setAttribute("type", type);
			request.setAttribute("pdfName", pdfName);
			request.setAttribute("pubNumber", pubNumber);
			request.setAttribute("xmlBean", xmlBean);
			return mapping.findForward("qacheck");
		}catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			return mapping.findForward("exception");
		} finally{
			
			if (session != null){
				session.close();
			}
			
			if (factory != null) {
				factory.close();
			}
			
		}
			
			
			
	}
}