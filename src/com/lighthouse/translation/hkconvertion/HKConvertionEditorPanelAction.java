package com.lighthouse.translation.hkconvertion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.plugin.HibernatePlugIn;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDao;

public class HKConvertionEditorPanelAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Session session = null;
		SessionFactory factory = null;
		String lockPubNumber = "";
		String userName = request.getSession().getAttribute("username").toString();
		try {
			
			
			if (!request.getParameter("lockPubNumber").toString().equals("N")){
				lockPubNumber = request.getParameter("lockPubNumber").toString();
			}
			
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			TxtPdfNameDao dao = TxtPdfNameDaoimp.getInstance();
			List<SplitTxt> submissionList = new ArrayList<SplitTxt>();
			List<SplitTxt> reOpenList = new ArrayList<SplitTxt>();
			reOpenList = dao.getPubNumberList(session, "reopen", "N");
			submissionList = dao.getPubNumberList(session, "submission", "N");
			
			
			List list = dao.getQAPubNumberLockList(session, userName, "editor");
			String pubNumber = "";
			if (list.size() != 0){
				SplitTxt lockNumber = (SplitTxt)list.get(0);
				pubNumber = lockNumber.getPubNumber();
			}
			
			request.setAttribute("lockPubNumber", lockPubNumber);
			request.setAttribute("reOpenList", reOpenList);
			request.setAttribute("submissionList", submissionList);
			request.setAttribute("pubNumber", pubNumber);
			return mapping.findForward("pdfeditornamelist");
		}catch (Exception e) {
			e.printStackTrace();
			return mapping.findForward("exception");
		} finally{
			
			if (session != null){
				session.close();
			}
			
			if (factory != null) {
				factory.close();
			}
			
		}
		
	}
}