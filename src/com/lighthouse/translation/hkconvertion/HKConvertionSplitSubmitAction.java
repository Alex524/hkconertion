package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.FtpBean;
import com.lighthouse.translation.hkconvertion.beans.SplitPdfBean;
import com.lighthouse.translation.hkconvertion.dao.EditFile;
import com.lighthouse.translation.hkconvertion.dao.PdfSplit;
import com.lighthouse.translation.hkconvertion.dao.SplitPDFDao;
import com.lighthouse.translation.hkconvertion.dao.SplitPDFDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtReadDao;
import com.lighthouse.translation.hkconvertion.dao.TxtReadDaoimp;
import com.lighthouse.translation.hkconvertion.dao.TxtSplitDao;
import com.lighthouse.translation.hkconvertion.dao.TxtSplitDaoimp;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionSplitSubmitAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
			String listEncryptPDF = request.getParameter("listEncryptPDF");
			
			String startPage_applications = request.getParameter("startPage_applications");
			String endPage_applications = request.getParameter("endPage_applications");
			String startPage_grants = request.getParameter("startPage_grants");
			String endPage_grants = request.getParameter("endPage_grants");
			String startPage_shortGrants = request.getParameter("startPage_shortGrants");
			String endPage_shortGrants = request.getParameter("endPage_shortGrants");
			
			String pdfFilePath=CommonConsts.PDF_PATH;
			
			try {
				SplitPdfBean bean = new SplitPdfBean();
				bean.setEndPage_applications(endPage_applications);
				bean.setEndPage_grants(endPage_grants);
				bean.setEndPage_shortGrants(endPage_shortGrants);
				bean.setStartPage_applications(startPage_applications);
				bean.setStartPage_grants(startPage_grants);
				bean.setStartPage_shortGrants(startPage_shortGrants);
				SplitPDFDao dao = SplitPDFDaoimp.getInstance();
				//boolean success = dao.addSplitPDF(session, listEncryptPDF, bean);
				
				
				SplitPDFDao splitDao = SplitPDFDaoimp.getInstance();
				ArrayList listTxtSplitPdf = dao.getSplitPDFNameList();
				ArrayList<FtpBean> listPDF = dao.getPdfNameList(listTxtSplitPdf);
				request.setAttribute("listPDF", listPDF);
				request.setAttribute("getPdfName", "no");
				
				PdfSplit pdfSplit = new PdfSplit();
				
				// 分离成3个PDF文件
				pdfSplit.split(pdfFilePath+listEncryptPDF+".pdf", listEncryptPDF, bean);
				
				
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("exception", e.getMessage());

				return mapping.findForward("exception");
			} 
		
			return mapping.findForward("success");
			
	}
}