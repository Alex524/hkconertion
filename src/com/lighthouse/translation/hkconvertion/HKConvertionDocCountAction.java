package com.lighthouse.translation.hkconvertion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.hkconvertion.beans.CountBean;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionDocCountAction extends BaseAction{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SessionFactory factory = null;
		Session session = null;
//		Transaction transaction = null;
		try {
			factory = (SessionFactory) servlet.getServletContext()
			.getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
//			transaction = session.beginTransaction();
			String patent = request.getParameter("patent");
			String sql1 = "select editorId,count(*) from SplitTxt where pdfName='"+patent+"' and pubNumber like '%A2'  and status='closed' group by editorId";
			Query query = session.createQuery(sql1);
			List list1 = new ArrayList();
			List a2TRList = query.list();
			for(int i=0;i<a2TRList.size();i++){
				CountBean cb = new CountBean();
				Object[] obj = (Object[]) a2TRList.get(i);
				cb.setId(obj[0].toString());
				cb.setCount(Integer.parseInt(obj[1].toString()));
				list1.add(cb);
			}
			String sql2 = "select qaId,count(*) from SplitTxt where pdfName='"+patent+"' and pubNumber like '%A2'  and status='closed' group by qaId";
			Query query1 = session.createQuery(sql2);
			List list2 = new ArrayList();
			List a2QAList = query1.list();
			for(int j=0;j<a2QAList.size();j++){
				CountBean cb = new CountBean();
				Object[] obj = (Object[]) a2QAList.get(j);
				cb.setId(obj[0].toString());
				cb.setCount(Integer.parseInt(obj[1].toString()));
				list2.add(cb);
			}
			String sql3 = "select editorId,count(*) from SplitTxt where pdfName='"+patent+"' and pubNumber not like '%A2'  and status='closed' group by editorId";
			Query query2 = session.createQuery(sql3);
			List list3 = new ArrayList();
			List otherTRList = query2.list();
			for(int k=0;k<otherTRList.size();k++){
				CountBean cb = new CountBean();
				Object[] obj = (Object[]) otherTRList.get(k);
				cb.setId(obj[0].toString());
				cb.setCount(Integer.parseInt(obj[1].toString()));
				list3.add(cb);
			}
			String sql4 = "select qaId,count(*) from SplitTxt where pdfName='"+patent+"' and pubNumber not like '%A2' and status='closed' group by qaId";
			Query query3 = session.createQuery(sql4);
			List list4 = new ArrayList();
			List otherQAList = query3.list();
			for(int l=0;l<otherQAList.size();l++){
				CountBean cb = new CountBean();
				Object[] obj = (Object[]) otherQAList.get(l);
				cb.setId(obj[0].toString());
				cb.setCount(Integer.parseInt(obj[1].toString()));
				list4.add(cb);
			}
			String sql5 = "select count(*) from SplitTxt where pdfName='"+patent+"'";
			Query query4 = session.createQuery(sql5);
			List allCount = query4.list();
			String allDocCount = allCount.get(0).toString();
			
			String sql6 = "select count(*) from SplitTxt where pdfName='"+patent+"' and editorId is null and qaDate is null";
			Query query5 = session.createQuery(sql6);
			List trCount = query5.list();
			String trDocCount = trCount.get(0).toString();
			
			String sql7 = "select count(*) from SplitTxt where pdfName='"+patent+"' and editorId is not null and qaDate is null";
			Query query6 = session.createQuery(sql7);
			List qaCount = query6.list();
			String qaDocCount = qaCount.get(0).toString();
			
			Connection con = session.connection();
			List listTime = new ArrayList();
			String sql = "select pdfName,CONVERT(datetime,publDate, 101) from SplitTxt group by pdfName,CONVERT(datetime,publDate, 101) order by CONVERT(datetime,publDate, 101) desc";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				listTime.add(rs.getString(1));
			}
			request.setAttribute("listTime", listTime);
			request.setAttribute("allDocCount", allDocCount);
			request.setAttribute("trDocCount", trDocCount);
			request.setAttribute("qaDocCount", qaDocCount);
			request.setAttribute("list1", list1);
			request.setAttribute("list2", list2);
			request.setAttribute("list3", list3);
			request.setAttribute("list4", list4);
		} catch (Exception e) {
			e.printStackTrace();
		}
				return mapping.findForward("docCount");
	}
}
