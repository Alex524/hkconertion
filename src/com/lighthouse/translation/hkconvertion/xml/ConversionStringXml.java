package com.lighthouse.translation.hkconvertion.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConversionStringXml {
	
	public List getDescriptionList(File file, String startTag, String endTag) throws IOException{
		FileReader filereader = null;
		BufferedReader reader = null;
		List<String> list = new ArrayList<String>();
		
		try{
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			boolean desBool = false;
			
			while((line=reader.readLine())!=null){
				StringBuffer stringBuffer = new StringBuffer();
				String conString = "";
				line=line.trim();
				
				if(line.length()>0){
					if (line.contains(startTag) && line.contains(endTag)){
						stringBuffer.append(line.substring(13, line.length()-14).toString());
						conString = this.getconString(stringBuffer);
						list.add(conString);
						continue;
					}
				
					if (desBool && !line.contains(endTag)) {
					stringBuffer.append(line);
					}
				
					if (line.contains(startTag) && !line.contains(endTag)) {
						stringBuffer.append(line.substring(13,line.length()).toString());
						desBool = true;
					}
				
					if (!line.contains(startTag) && line.contains(endTag)) {
						stringBuffer.append(line.substring(0, line.length()-14).toString());
						conString = this.getconString(stringBuffer);
						list.add(conString);
						continue;
					}
				
				}	
				
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
		
		return list;
		
	}
	
	public String getconString(StringBuffer stringBuffer){
		String conString = "";
		conString = stringBuffer.toString();
		conString = conString.replace("&lt;", "<");
		conString = conString.replace("&gt;", ">");
		return conString;
	}
	
}
