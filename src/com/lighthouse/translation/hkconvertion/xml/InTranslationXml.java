package com.lighthouse.translation.hkconvertion.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.*;

public class InTranslationXml {

	private static InTranslationXml instance;

	/**
	 * 
	 * @return This method used to get this class's instance.
	 */
	public static InTranslationXml getInstance() {
		if (instance == null) // 1

			instance = new InTranslationXml(); // 2

		return instance; // 3

	}
	
	/*
	 * 新增XML文档
	 * 
	 * */
	
	public boolean parseXML(XmlEditBean mainXEB, String path) {
		boolean succ = false;
		XMLWriter output = null;
		try{
			Document document = DocumentHelper.createDocument();
			Element root = document.addElement("patdoc");
			Element sdobiEle = root.addElement("sdobi");
			
			//b100
			
			if (!ifnull(mainXEB.getB110()) || !ifnull(mainXEB.getB130())) {
				Element B100Element = sdobiEle.addElement("B100");
			
				if (!ifnull(mainXEB.getB110())) {
					Element B110Element = B100Element.addElement("B110");
					B110Element.addText(mainXEB.getB110());
				}
			
				if (!ifnull(mainXEB.getB130())) {
					Element B110Element = B100Element.addElement("B130");
					B110Element.addText(mainXEB.getB130());
				}
			}
			
			
			
			//b200
			
			if (!ifnull(mainXEB.getB210()) || !ifnull(mainXEB.getB220Year()) || !ifnull(mainXEB.getB220Month()) || !ifnull(mainXEB.getB220Day()) || !ifnull(mainXEB.getB250())) {

				Element B200Element = sdobiEle.addElement("B200");
				
				if (!ifnull(mainXEB.getB210())) {
					Element B210Element = B200Element.addElement("B210");
					B210Element.addText(mainXEB.getB210());
				}
				
				if (!ifnull(mainXEB.getB220Year()) || !ifnull(mainXEB.getB220Month()) || !ifnull(mainXEB.getB220Day())) {
					Element B220Element = B200Element.addElement("B220");
					Element B220DateElement = B220Element.addElement("date");
					B220DateElement.addAttribute("Year", mainXEB.getB220Year());
					B220DateElement.addAttribute("Month", mainXEB.getB220Month());
					B220DateElement.addAttribute("Day", mainXEB.getB220Day());
				}
				
				if (!ifnull(mainXEB.getB250())) {
					Element B250Element = B200Element.addElement("B250");
					String b250 = mainXEB.getB250();
					if(b250.equalsIgnoreCase("JP")){
						b250 = "JA";
					}
					if(b250.equalsIgnoreCase("CH")){
						b250 = "ZH";
					}
					B250Element.addText(b250);
				}
				
			}
			
			//b300
			if (mainXEB.getB300().size() > 0) {
				Element B300Element = null;
				Iterator it = mainXEB.getB300().iterator();
				if(this.isB300NotEmpty(mainXEB.getB300())){
					B300Element = sdobiEle.addElement("B300");
				
				while (it.hasNext()) {
					
					B300Bean b300 = (B300Bean) it.next();
					
					if (!ifnull(b300.getB310())) {
						Element B310Element = B300Element.addElement("B310");
						B310Element.addText(b300.getB310());
					}
					
					if (!ifnull(b300.getB320Year()) || !ifnull(b300.getB320Month()) || !ifnull(b300.getB320Day())){
						Element B320Element = B300Element.addElement("B320");
						Element B320DateElement = B320Element.addElement("date");
						B320DateElement.addAttribute("Year", b300.getB320Year());
						B320DateElement.addAttribute("Month", b300.getB320Month());
						B320DateElement.addAttribute("Day", b300.getB320Day());
					}
					
					if (!ifnull(b300.getB330Ctry())) {
						Element B330Element = B300Element.addElement("B330");
						Element B330CtryElement = B330Element.addElement("ctry");
						B330CtryElement.addText(b300.getB330Ctry().toUpperCase());
					}
				}
				
				}
				
			}
			
			//b400
			
			if (!ifnull(mainXEB.getB430Year()) || !ifnull(mainXEB.getB430Month()) || !ifnull(mainXEB.getB430Day())
				 || !ifnull(mainXEB.getB450Year()) || !ifnull(mainXEB.getB450Month()) || !ifnull(mainXEB.getB450Day())) {

				Element B400Element = sdobiEle.addElement("B400");
				
				if (!ifnull(mainXEB.getB430Year()) || !ifnull(mainXEB.getB430Month()) || !ifnull(mainXEB.getB430Day())) {
					Element B430Element = B400Element.addElement("B430");
					Element B430DateElement = B430Element.addElement("date");
					B430DateElement.addAttribute("Year", mainXEB.getB430Year());
					B430DateElement.addAttribute("Month", mainXEB.getB430Month());
					B430DateElement.addAttribute("Day", mainXEB.getB430Day());
				}
				
				if (!ifnull(mainXEB.getB450Year()) || !ifnull(mainXEB.getB450Month()) || !ifnull(mainXEB.getB450Day())) {
					Element B450Element = B400Element.addElement("B450");
					Element B450DateElement = B450Element.addElement("date");
					B450DateElement.addAttribute("Year", mainXEB.getB450Year());
					B450DateElement.addAttribute("Month", mainXEB.getB450Month());
					B450DateElement.addAttribute("Day", mainXEB.getB450Day());
				}
				
			}
			
			//b500
			
			if (!ifnull(mainXEB.getB511Class()) || !ifnull(mainXEB.getB511Version()) || mainXEB.getB512().size() > 0 || !ifnull(mainXEB.getB516()) || !ifnull(mainXEB.getB541()) || !ifnull(mainXEB.getB542()) || mainXEB.getB561().size() > 0 || !ifnull(mainXEB.getB563()) || mainXEB.getB562().size() > 0) {
				Element B500Element = sdobiEle.addElement("B500");
				if (!ifnull(mainXEB.getB511Class()) || !ifnull(mainXEB.getB511Version()) || mainXEB.getB512().size() > 0 || !ifnull(mainXEB.getB516())) {
					Element B510Element = B500Element.addElement("B510");
					
					if (!ifnull(mainXEB.getB511Class()) || !ifnull(mainXEB.getB511Version())) {
						Element B511Element = B510Element.addElement("B511");
						if (!ifnull(mainXEB.getB511Class())) {
							Element B511ClassElement = B511Element.addElement("class");
							B511ClassElement.addText(mainXEB.getB511Class());
						}
						if (!ifnull(mainXEB.getB511Version())) {
							Element B511versionElement = B511Element.addElement("version");
							B511versionElement.addText(mainXEB.getB511Version());
						}
						
					}
					
					if (mainXEB.getB512().size() > 0) {
						Iterator it = mainXEB.getB512().iterator();
						while (it.hasNext()) {
							B512Bean b512 = (B512Bean) it.next();
							
							if (!ifnull(b512.getB512Class()) || !ifnull(b512.getB512Version())) {
								Element B512Element = B510Element.addElement("B512");
								
								if (!ifnull(b512.getB512Class())) {
									Element B512ClassElement = B512Element.addElement("class");
									B512ClassElement.addText(b512.getB512Class());
								}
								
								if (!ifnull(b512.getB512Version())) {
									Element B512VersionElement = B512Element.addElement("version");
									B512VersionElement.addText(b512.getB512Version());
								}
							
							}
							
						}
					}
					
					if (!ifnull(mainXEB.getB516())) {
						Element B516Element = B510Element.addElement("B516");
						B516Element.addText(mainXEB.getB516());
					}
					
				}
				
				if (!ifnull(mainXEB.getB541()) || !ifnull(mainXEB.getB542())) {
					Element B540Element = B500Element.addElement("B540");
					
					if (!ifnull(mainXEB.getB541())) {
						Element B541Element = B540Element.addElement("B541");
						B541Element.addText("EN");
					}
					
					if (!ifnull(mainXEB.getB542())) {
						Element B542Element = B540Element.addElement("B542");
						B542Element.addText(mainXEB.getB542());
					}
				}
				
				if (mainXEB.getB561().size() > 0 || !ifnull(mainXEB.getB563()) || mainXEB.getB562().size() > 0) {
					Element B560Element = B500Element.addElement("B560");
					
					if (mainXEB.getB561().size() > 0) {
						Iterator it = mainXEB.getB561().iterator();
						while (it.hasNext()) {
							B561Bean b561 = (B561Bean) it.next();
							
							if (!ifnull(b561.getB561Ctry()) || !ifnull(b561.getB561Dnum()) || !ifnull(b561.getB561Kind()) || !ifnull(b561.getB561Snm())) {
								Element B561Element = B560Element.addElement("B561");
								
								if (!ifnull(b561.getB561Ctry())) {
									Element B561CtryElement = B561Element.addElement("ctry");
									B561CtryElement.addText(b561.getB561Ctry().toUpperCase());
								}
								
								if (!ifnull(b561.getB561Dnum())) {
									Element B561DnumElement = B561Element.addElement("dnum");
									B561DnumElement.addText(b561.getB561Dnum());
								}
								
								if (!ifnull(b561.getB561Kind())) {
									Element B561KindElement = B561Element.addElement("kind");
									B561KindElement.addText(b561.getB561Kind());
								}
								
								if (!ifnull(b561.getB561Snm())) {
									Element B561SnmElement = B561Element.addElement("snm");
									B561SnmElement.addText(b561.getB561Snm());
								}
								
							}
							
						}
						
					}
					
					if (!ifnull(mainXEB.getB563())) {
						Element B563Element = B560Element.addElement("B563");
						B563Element.addText(mainXEB.getB563());
					}
					
					
					if (mainXEB.getB562().size() > 0) {
						Iterator it = mainXEB.getB562().iterator();
						
						while (it.hasNext()) {
							Element B562Element = B560Element.addElement("B562");
							B562Bean b562 = (B562Bean) it.next();
							B562Element.addText(b562.getB562Reference());
						}
						
					}
					
				}
				
			}
			
			
			//600
			if (!ifnull(mainXEB.getB655Ctry()) || !ifnull(mainXEB.getB655Pnum()) || !ifnull(mainXEB.getB655Kind()) || !ifnull(mainXEB.getB620Dnum()) 
				|| (!ifnull(mainXEB.getB620Year()) && !ifnull(mainXEB.getB620Month()) && !ifnull(mainXEB.getB620Day()))) {
				Element B600Element = sdobiEle.addElement("B600");
				
				if (!ifnull(mainXEB.getB620Dnum()) || (!ifnull(mainXEB.getB620Year()) && !ifnull(mainXEB.getB620Month()) && !ifnull(mainXEB.getB620Day()))) {
					Element B620Element = B600Element.addElement("B620");
					
					if (!ifnull(mainXEB.getB620Dnum())) {
						Element B620dnumElement = B620Element.addElement("dnum");
						B620dnumElement.addText(mainXEB.getB620Dnum());
					}
					
					if (!ifnull(mainXEB.getB620Year()) && !ifnull(mainXEB.getB620Month()) && !ifnull(mainXEB.getB620Day())) {
						Element B620DateElement = B620Element.addElement("date");
						B620DateElement.addAttribute("Year", mainXEB.getB620Year());
						B620DateElement.addAttribute("Month", mainXEB.getB620Month());
						B620DateElement.addAttribute("Day", mainXEB.getB620Day());
					}
					
				}
				
				if (!ifnull(mainXEB.getB655Ctry()) || !ifnull(mainXEB.getB655Pnum()) || !ifnull(mainXEB.getB655Kind())) {
					Element B655Element = B600Element.addElement("B655");
					
					if (!ifnull(mainXEB.getB655Ctry())) {
						Element ctryElement = B655Element.addElement("ctry");
						ctryElement.addText(mainXEB.getB655Ctry());
					}
				
					if (!ifnull(mainXEB.getB655Pnum())) {
						Element pnumElement = B655Element.addElement("pnum");
						pnumElement.addText(mainXEB.getB655Pnum());
					}
					
					if (!ifnull(mainXEB.getB655Kind())) {
						Element kindElement = B655Element.addElement("kind");
						kindElement.addText(mainXEB.getB655Kind());
					}
					
				}
				
			}
			
			
			
			//700
			
			if (mainXEB.getB711().size() > 0 || mainXEB.getB721().size() > 0 || mainXEB.getB731().size() > 0 || mainXEB.getB741().size() > 0) {
				Element B700Element = sdobiEle.addElement("B700");
				String replace_single_str = "";
				if (mainXEB.getB711().size() > 0) {
					Element B710Element = B700Element.addElement("B710");
					Iterator it = mainXEB.getB711().iterator();
					while (it.hasNext()) {
						B711Bean b711 = (B711Bean) it.next();
						
						if (!ifnull(b711.getB711City()) || !ifnull(b711.getB711Ctry()) || !ifnull(b711.getB711Snm()) || !ifnull(b711.getB711Str())) {
							Element B711Element = B710Element.addElement("B711");
							
							if (!ifnull(b711.getB711Snm())) {
								Element B711SnmElement = B711Element.addElement("snm");
								replace_single_str = replaceSingleStr(b711.getB711Snm(), "ü", "u");
								replace_single_str = replaceSingleStr(replace_single_str, "é", "e");
								B711SnmElement.addText(replace_single_str);
							}
							
							if (!ifnull(b711.getB711Ctry()) || !ifnull(b711.getB711City()) || !ifnull(b711.getB711Str())) {
								Element B711AdrElement = B711Element.addElement("adr");
								
								if (!ifnull(b711.getB711Str())) {
									Element B711StrElement = B711AdrElement.addElement("str");
									B711StrElement.addText(b711.getB711Str());
								}
								
								
								if (!ifnull(b711.getB711Ctry())) {
									Element B711CtryElement = B711AdrElement.addElement("ctry");
									B711CtryElement.addText(b711.getB711Ctry().toUpperCase());
								}
								
							}
							
						}
						
					}
					
				}
				
				if (mainXEB.getB721().size() > 0) {
					Element B720Element = B700Element.addElement("B720");
					Iterator it = mainXEB.getB721().iterator();
					while (it.hasNext()) {
						B721Bean b721 = (B721Bean) it.next();
						
						if (!ifnull(b721.getB721City()) || !ifnull(b721.getB721Ctry()) || !ifnull(b721.getB721Snm()) || !ifnull(b721.getB721Str())) {
							Element B721Element = B720Element.addElement("B721");
							
							if (!ifnull(b721.getB721Snm())) {
								Element B721SnmElement = B721Element.addElement("snm");
								String str_replace = replaceStr(b721.getB721Snm().toString().trim(), " ", ",");
								replace_single_str = replaceSingleStr(str_replace, "ü", "u");
								replace_single_str = replaceSingleStr(replace_single_str, "é", "e");
								B721SnmElement.addText(replace_single_str);
							}
							
							if (!ifnull(b721.getB721Ctry()) || !ifnull(b721.getB721City()) || !ifnull(b721.getB721Str())) {
								Element B721AdrElement = B721Element.addElement("adr");
								
								if (!ifnull(b721.getB721Str())) {
									Element B721StrElement = B721AdrElement.addElement("str");
									B721StrElement.addText(b721.getB721Str());
								}
								
								if (!ifnull(b721.getB721City())) {
									Element B721CityElement = B721AdrElement.addElement("cntry");
									B721CityElement.addText(b721.getB721City());
								}
								
								if (!ifnull(b721.getB721Ctry())) {
									Element B721CtryElement = B721AdrElement.addElement("ctry");
									B721CtryElement.addText(b721.getB721Ctry().toUpperCase());
								}
								
							}
							
						}
						
					}
					
				}
				
				if (mainXEB.getB731().size() > 0) {
					Element B730Element = B700Element.addElement("B730");
					Iterator it = mainXEB.getB731().iterator();
					while (it.hasNext()) {
						B731Bean b731 = (B731Bean) it.next();
						
						if (!ifnull(b731.getB731City()) || !ifnull(b731.getB731Ctry()) || !ifnull(b731.getB731Snm()) || !ifnull(b731.getB731Str())) {
							Element B731Element = B730Element.addElement("B731");
							
							if (!ifnull(b731.getB731Snm())) {
								Element B731SnmElement = B731Element.addElement("snm");
								replace_single_str = replaceSingleStr(b731.getB731Snm(), "ü", "u");
								replace_single_str = replaceSingleStr(replace_single_str, "é", "e");
								B731SnmElement.addText(replace_single_str);
							}
							
							if (!ifnull(b731.getB731Ctry()) || !ifnull(b731.getB731City()) || !ifnull(b731.getB731Str())) {
								Element B731AdrElement = B731Element.addElement("adr");
								
								if (!ifnull(b731.getB731Str())) {
									Element B731StrElement = B731AdrElement.addElement("str");
									B731StrElement.addText(b731.getB731Str());
								}
								
//								if (!ifnull(b731.getB731City())) {
//									Element B731CityElement = B731AdrElement.addElement("cntry");
//									B731CityElement.addText(b731.getB731City());
//								}
								
								if (!ifnull(b731.getB731Ctry())) {
									Element B731CtryElement = B731AdrElement.addElement("ctry");
									B731CtryElement.addText(b731.getB731Ctry().toUpperCase());
								}
								
							}
							
						}
						
					}
					
				}
				
				if (mainXEB.getB741().size() > 0) {
					Element B740Element = B700Element.addElement("B740");
					Iterator it = mainXEB.getB741().iterator();
					while (it.hasNext()) {
						B741Bean b741 = (B741Bean) it.next();
						
						if (!ifnull(b741.getB741Snm())) {
							Element B741Element = B740Element.addElement("B741");
							Element B741SnmElement = B741Element.addElement("snm");
							replace_single_str = replaceSingleStr(b741.getB741Snm(), "ü", "u");
							replace_single_str = replaceSingleStr(replace_single_str, "é", "e");
							B741SnmElement.addText(replace_single_str);
						}
					}
				}
				
			}
			
			//800
			
			if (!ifnull(mainXEB.getB850Year()) || !ifnull(mainXEB.getB850Month()) || !ifnull(mainXEB.getB850Day()) ||
					!ifnull(mainXEB.getB861Anum()) || !ifnull(mainXEB.getB861Year()) || !ifnull(mainXEB.getB861Month()) || !ifnull(mainXEB.getB861Day()) || 
					!ifnull(mainXEB.getB871Pnum()) || !ifnull(mainXEB.getB871Year()) || !ifnull(mainXEB.getB871Month()) || !ifnull(mainXEB.getB871Day())) {
				Element B800Element = sdobiEle.addElement("B800");
				
				if (!ifnull(mainXEB.getB850Year()) || !ifnull(mainXEB.getB850Month()) || !ifnull(mainXEB.getB850Day())) {
					Element B850Element = B800Element.addElement("B850");
					Element B850DateElement = B850Element.addElement("date");
					B850DateElement.addAttribute("Year", mainXEB.getB850Year());
					B850DateElement.addAttribute("Month", mainXEB.getB850Month());
					B850DateElement.addAttribute("Day", mainXEB.getB850Day());
				}
				
				if (!ifnull(mainXEB.getB861Anum()) || !ifnull(mainXEB.getB861Year()) || !ifnull(mainXEB.getB861Month()) || !ifnull(mainXEB.getB861Day())) {
					Element B860Element = B800Element.addElement("B860");
					
					if (!ifnull(mainXEB.getB861Anum()) || !ifnull(mainXEB.getB861Year()) || !ifnull(mainXEB.getB861Month()) || !ifnull(mainXEB.getB861Day())) {
						Element B861Element = B860Element.addElement("B861");
						
						if (!ifnull(mainXEB.getB861Anum())) {
							Element B861AnumElement = B861Element.addElement("anum");
							B861AnumElement.addText(mainXEB.getB861Anum());
						}
						
						if (!ifnull(mainXEB.getB861Year()) || !ifnull(mainXEB.getB861Month()) || !ifnull(mainXEB.getB861Day())) {
							Element B861DateElement = B861Element.addElement("date");
							B861DateElement.addAttribute("Year", mainXEB.getB861Year());
							B861DateElement.addAttribute("Month", mainXEB.getB861Month());
							B861DateElement.addAttribute("Day", mainXEB.getB861Day());
						}
						
					}
					
				}
				
				if (!ifnull(mainXEB.getB871Pnum()) || !ifnull(mainXEB.getB871Year()) || !ifnull(mainXEB.getB871Month()) || !ifnull(mainXEB.getB871Day())) {
					Element B870Element = B800Element.addElement("B870");
					
					if (!ifnull(mainXEB.getB871Pnum()) || !ifnull(mainXEB.getB871Year()) || !ifnull(mainXEB.getB871Month()) || !ifnull(mainXEB.getB871Day())) {
						Element B871Element = B870Element.addElement("B871");
						
						if (!ifnull(mainXEB.getB871Pnum())) {
							Element B871PnumElement = B871Element.addElement("pnum");
							B871PnumElement.addText(mainXEB.getB871Pnum());
						}
						
						if (!ifnull(mainXEB.getB871Year()) || !ifnull(mainXEB.getB871Month()) || !ifnull(mainXEB.getB871Day())) {
							Element B871DateElement = B871Element.addElement("date");
							B871DateElement.addAttribute("Year", mainXEB.getB871Year());
							B871DateElement.addAttribute("Month", mainXEB.getB871Month());
							B871DateElement.addAttribute("Day", mainXEB.getB871Day());
						}
						
					}
					
				}
				
			}
			
			//abchi abeng p
			
			if (!ifnull(mainXEB.getAbeng())) {
				Element SDOAB = root.addElement("SDOAB");
				
				if (!ifnull(mainXEB.getAbeng())) {
					Element abengElement = SDOAB.addElement("abeng");
					Element abengSecElement = abengElement.addElement("sec");
					Element abengPElement = abengSecElement.addElement("p");
					abengPElement.addText(mainXEB.getAbeng());
				}
				
			}
			
			// claims
			
			if (mainXEB.getClaims().size() > 0) {
				Element CLAIMS = root.addElement("CLAIMS");
				Iterator it = mainXEB.getClaims().iterator();
				
				while (it.hasNext()) {
					ClaimsBean claims = (ClaimsBean) it.next();
					Element clmElement = CLAIMS.addElement("CLM");
					clmElement.addText(claims.getClaimsClm());
				}
				
			}
			
			//description
			if (mainXEB.getDescriptions().size() > 0) {
				Iterator it = mainXEB.getDescriptions().iterator();
				
				while (it.hasNext()) {
					DescriptionsBean descriptions = (DescriptionsBean) it.next();
					Element descriptionElement = root.addElement("description");
					descriptionElement.addText(descriptions.getDescription());
				}
				
			}
					
					
			//删除文件
			deleteFile(path+ mainXEB.getB110()+mainXEB.getB130() + ".xml");
			
			// outPut
			//cs
			//String app_path = "E:\\Work Station\\XML\\";
			OutputFormat format = OutputFormat.createPrettyPrint();
			format.setNewlines(true);
			format.setLineSeparator(System.getProperty("line.separator"));
			format.setEncoding("UTF-8");
			path = path.endsWith("\\") || path.endsWith("/") ? path : path
					+ "\\";
			
			String pubNumber_path = "";
			pubNumber_path = mainXEB.getB110()+""+mainXEB.getB130();
			
			
			output = new XMLWriter(new FileOutputStream(path + pubNumber_path + ".xml"), format);
			output.write(document);
			output.close();
			succ = true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return succ;
	}
	
	private boolean ifnull(String str) {
		boolean ifnull = false;
		if (str == null || str.length() == 0 || str.trim().equals("")) {
			ifnull = true;
		}
		return ifnull;
	}
	
	//第一个空格用,替换
	private String replaceStr(String str, String problemStr, String replace){
		int num = str.indexOf(problemStr); 
		String new_str = "";
		if (num<str.length() && str.indexOf(",")<=0){
			
			for (int i=0; i<str.length(); i++){
				if (i==num){
					new_str = new_str + replace;
				} else{
					new_str = new_str + str.substring(i, i+1);
				}
				
			}
			
		} else {
			new_str = str;
		}

	    return new_str;

	}
	
	//替换特殊字符串
	public static String replaceSingleStr(String str, String problemStr, String replace){

	    for(int i=str.lastIndexOf(problemStr); i>=0; i=str.lastIndexOf(problemStr, i-1)){

	        if (i==0) {
	            str = replace+str.substring(i+1, str.length());
	        } else {
	            str = str.substring(0, i)+replace+str.substring(i+1, str.length());
	        }
	        
	    }
	    
	    return str;

	}
	
	
	
	//删除文件
	public  void  deleteFile(String folderPath)  {  
	       try  {  
	    	   String  filePath  =  folderPath;  
	           filePath  =  filePath.toString(); 
	           File myFilePath = new File(filePath);  
	           if (myFilePath.exists()) {  
	               myFilePath.delete();
	           }  
	       }catch (Exception  e) {  
	           System.out.println("删除文件操作出错");  
	           e.printStackTrace();  
	       }  
	   }
	
	private boolean isB300NotEmpty(Set B300bean){
		Iterator it = B300bean.iterator();
		while(it.hasNext()){
			B300Bean b300 = (B300Bean) it.next();
			if(!b300.getB310().equals("")){
				return true;
			}
		}	
		return false;
	}
}
