package com.lighthouse.translation.hkconvertion.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.lighthouse.translation.hkconvertion.xml.ConversionStringXml;
import com.lighthouse.translation.hkconvertion.beans.*;

public class OutTranslationXml {
	
	private static OutTranslationXml instance;

	/**
	 * 
	 * @return This method used to get this class's instance.
	 */
	public static OutTranslationXml getInstance() {
		if (instance == null) // 1

			instance = new OutTranslationXml(); // 2

		return instance; // 3

	}
	
	/*
	 * 解析XML文档
	 * 
	 * */
	public XmlEditBean parseXML(File file, String encodingString) {
		XmlEditBean mainXEB = new XmlEditBean();
		
		Set b300Set = new LinkedHashSet(0);
		
		Set b512Set = new LinkedHashSet(0);
		
		Set b561Set = new LinkedHashSet(0);
		
		Set b562Set = new LinkedHashSet(0);
		
		Set b711Set = new LinkedHashSet(0);
		
		Set b721Set = new LinkedHashSet(0);
		
		Set b731Set = new LinkedHashSet(0);
		
		Set b741Set = new LinkedHashSet(0);
		
		Set clmSet = new LinkedHashSet(0);
		
		Set desSet = new LinkedHashSet(0);
		
		try {
			SAXReader saxReader = new SAXReader();
			
			if (encodingString.equals("true")){
				saxReader.setEncoding("ISO-8859-1");
			}else{
				saxReader.setEncoding("UTF-8");
			}
			
			Document document = saxReader.read(file);
			
			
			// Kind

			String docKind = "";
			List list = document.selectNodes("/patdoc/sdobi/B100/B130");
			Iterator iter = list.iterator();

			if (iter.hasNext()) {
				Element ele130 = (Element) iter.next();
				docKind = ele130.getTextTrim();
				mainXEB.setB130(docKind);
			}
			
			// b110
			list = document.selectNodes("/patdoc/sdobi/B100/B110");
			 iter = list.iterator();
			String pubNumber = "";
			if (iter.hasNext()) {
				Element ele110 = (Element) iter.next();
				pubNumber = ele110.getTextTrim();
				mainXEB.setB110(pubNumber);
			}
			
			// b210
			String appNumber = "";
			list = document.selectNodes("/patdoc/sdobi/B200/B210");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele210 = (Element) iter.next();
				appNumber = ele210.getTextTrim();
				mainXEB.setB210(appNumber);
			}
			
			// b220

			String appYear = "";
			String appMonth = "";
			String appDay = "";
			list = document.selectNodes("/patdoc/sdobi/B200/B220/date");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele220 = (Element) iter.next();
				appYear = ele220.attributeValue("Year");
				appMonth = ele220.attributeValue("Month");
				appDay = ele220.attributeValue("Day");
				mainXEB.setB220Year(appYear);
				mainXEB.setB220Month(appMonth);
				mainXEB.setB220Day(appDay);

			}
			
			// b250
			list = document.selectNodes("/patdoc/sdobi/B200/B250");
			 iter = list.iterator();
			String appl_language = "";
			if (iter.hasNext()) {
				Element ele250 = (Element) iter.next();
				appl_language = ele250.getTextTrim();
				mainXEB.setB250(appl_language);
			}
			
			// pubDate b430
			
			String pubYearB430 = "";
			String pubMonthB430 = "";
			String pubDayB430 = "";
			list = document.selectNodes("/patdoc/sdobi/B400/B430/date");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele430 = (Element) iter.next();
				pubYearB430 = ele430.attributeValue("Year");
				pubMonthB430 = ele430.attributeValue("Month");
				pubDayB430 = ele430.attributeValue("Day");
				mainXEB.setB430Year(pubYearB430);
				mainXEB.setB430Month(pubMonthB430);
				mainXEB.setB430Day(pubDayB430);
			}
			
			
			// pubDate b450
			
			String pubYearB450 = "";
			String pubMonthB450 = "";
			String pubDayB450 = "";
			list = document.selectNodes("/patdoc/sdobi/B400/B450/date");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele450 = (Element) iter.next();
				pubYearB450 = ele450.attributeValue("Year");
				pubMonthB450 = ele450.attributeValue("Month");
				pubDayB450 = ele450.attributeValue("Day");
				mainXEB.setB450Year(pubYearB450);
				mainXEB.setB450Month(pubMonthB450);
				mainXEB.setB450Day(pubDayB450);
			}
			
			// B511Class
			
			String b511Class = "";
			list = document.selectNodes("/patdoc/sdobi/B500/B510/B511/class");
			iter = list.iterator();

			if (iter.hasNext()) {
				Element ele511Class = (Element) iter.next();
				b511Class = ele511Class.getTextTrim();
				mainXEB.setB511Class(b511Class);
			}
			
			// B511Version
			
			String b511Version = "";
			list = document.selectNodes("/patdoc/sdobi/B500/B510/B511/version");
			iter = list.iterator();

			if (iter.hasNext()) {
				Element ele511Version = (Element) iter.next();
				b511Version = ele511Version.getTextTrim();
				mainXEB.setB511Version(b511Version);
			}
			
			// B516
			
			String b516 = "";
			list = document.selectNodes("/patdoc/sdobi/B500/B510/B516");
			iter = list.iterator();

			if (iter.hasNext()) {
				Element ele516 = (Element) iter.next();
				b516 = ele516.getTextTrim();
				mainXEB.setB516(b516);
			}
			
			// B541
			
			String b541 = "";
			list = document.selectNodes("/patdoc/sdobi/B500/B540/B541");
			iter = list.iterator();

			if (iter.hasNext()) {
				Element ele541 = (Element) iter.next();
				b541 = ele541.getTextTrim();
				mainXEB.setB541(b541);
			}
			
			// B542
			
			String b542 = "";
			list = document.selectNodes("/patdoc/sdobi/B500/B540/B542");
			iter = list.iterator();

			if (iter.hasNext()) {
				Element ele542 = (Element) iter.next();
				b542 = ele542.getTextTrim();
				b542 = new String(b542.getBytes("ISO-8859-1"),"UTF-8");
				mainXEB.setB542(b542);
			}
			
			// B563
			
			String b563 = "";
			list = document.selectNodes("/patdoc/sdobi/B500/B560/B563");
			iter = list.iterator();

			if (iter.hasNext()) {
				Element ele563 = (Element) iter.next();
				b563 = ele563.getTextTrim();
				mainXEB.setB563(b563);
			}
			
			// B562
			
			list = document.selectNodes("/patdoc/sdobi/B500/B560/B562");
			iter = list.iterator();
			
			while (iter.hasNext()) {
				Element eleb562 = (Element) iter.next();
				String tmpb562 = eleb562.getTextTrim();
				B562Bean b562_bean = new B562Bean();
				b562_bean.setB562Reference(tmpb562);
				b562Set.add(b562_bean);
				mainXEB.setB562(b562Set);
			}
			
			// B600
			
			list = document.selectNodes("/patdoc/sdobi/B600/B620/dnum");
			iter = list.iterator();
			String b620_dnum = "";
			if (iter.hasNext()) {
				Element eleB620Dnum = (Element) iter.next();
				b620_dnum = eleB620Dnum.getTextTrim();
				mainXEB.setB620Dnum(b620_dnum);
			}
			
			String pubYearB620 = "";
			String pubMonthB620 = "";
			String pubDayB620 = "";
			list = document.selectNodes("/patdoc/sdobi/B600/B620/date");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele620 = (Element) iter.next();
				pubYearB620 = ele620.attributeValue("Year");
				pubMonthB620 = ele620.attributeValue("Month");
				pubDayB620 = ele620.attributeValue("Day");
				mainXEB.setB620Year(pubYearB620);
				mainXEB.setB620Month(pubMonthB620);
				mainXEB.setB620Day(pubDayB620);
			}
			
			
			list = document.selectNodes("/patdoc/sdobi/B600/B655/ctry");
			iter = list.iterator();
			String re_ctry = "";
			if (iter.hasNext()) {
				Element eleB655Ctry = (Element) iter.next();
				re_ctry = eleB655Ctry.getTextTrim();
				mainXEB.setB655Ctry(re_ctry);
			}
			
			list = document.selectNodes("/patdoc/sdobi/B600/B655/pnum");
			iter = list.iterator();
			String re_pnum = "";
			if (iter.hasNext()) {
				Element eleB655Pnum = (Element) iter.next();
				re_pnum = eleB655Pnum.getTextTrim();
				mainXEB.setB655Pnum(re_pnum);
			}
			
			list = document.selectNodes("/patdoc/sdobi/B600/B655/kind");
			iter = list.iterator();
			String re_kind = "";
			if (iter.hasNext()) {
				Element eleB655Kind = (Element) iter.next();
				re_kind = eleB655Kind.getTextTrim();
				mainXEB.setB655Kind(re_kind);
			}
			
			
			// B850
			
			String b850Year = "";
			String b850Month = "";
			String b850Day = "";
			list = document.selectNodes("/patdoc/sdobi/B800/B850/date");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele850 = (Element) iter.next();
				b850Year = ele850.attributeValue("Year");
				b850Month = ele850.attributeValue("Month");
				b850Day = ele850.attributeValue("Day");
				mainXEB.setB850Year(b850Year);
				mainXEB.setB850Month(b850Month);
				mainXEB.setB850Day(b850Day);
			}
			
			// B861 anum
			
			String interAppCode = "";
			list = document.selectNodes("/patdoc/sdobi/B800/B860/B861/anum");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele861anum = (Element) iter.next();
				interAppCode = ele861anum.getTextTrim();
				mainXEB.setB861Anum(interAppCode);
			}
			
			// b861 date 
			
			String interAppYear = "";
			String interAppMonth = "";
			String interAppDay = "";
			list = document.selectNodes("/patdoc/sdobi/B800/B860/B861/date");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele861Date = (Element) iter.next();
				interAppYear = ele861Date.attributeValue("Year");
				interAppMonth = ele861Date.attributeValue("Month");
				interAppDay = ele861Date.attributeValue("Day");
				mainXEB.setB861Year(interAppYear);
				mainXEB.setB861Month(interAppMonth);
				mainXEB.setB861Day(interAppDay);
			}
			
			// B871 pnum 
			
			String interPubCode = "";
			list = document.selectNodes("/patdoc/sdobi/B800/B870/B871/pnum");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele871pnum = (Element) iter.next();
				interPubCode = ele871pnum.getTextTrim();
				mainXEB.setB871Pnum(interPubCode);
			}
			
			// b871 date
			String interPubYear = "";
			String interPubMonth = "";
			String interPubDay = "";
			list = document
					.selectNodes("/patdoc/sdobi/B800/B870/B871/date");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element ele871Date = (Element) iter.next();
				interPubYear = ele871Date.attributeValue("Year");
				interPubMonth = ele871Date.attributeValue("Month");
				interPubDay = ele871Date.attributeValue("Day");
				mainXEB.setB871Year(interPubYear);
				mainXEB.setB871Month(interPubMonth);
				mainXEB.setB871Day(interPubDay);
			}
			
			//abvie sec p
			
			String abvieSecP = "";
			list = document.selectNodes("/patdoc/SDOAB/abvie/sec/p");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element eleAbvieSecP = (Element) iter.next();
				abvieSecP = eleAbvieSecP.getTextTrim();
				mainXEB.setAbvie(abvieSecP);
			}
			
			//abtha sec p
			
			String abthaSecP = "";
			list = document.selectNodes("/patdoc/SDOAB/abtha/sec/p");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element eleAbthaSecP = (Element) iter.next();
				abthaSecP = eleAbthaSecP.getTextTrim();
				mainXEB.setAbtha(abthaSecP);
			}
			
			//abrus sec p
			
			String abrusSecP = "";
			list = document.selectNodes("/patdoc/SDOAB/abrus/sec/p");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element eleAbrusSecP = (Element) iter.next();
				abrusSecP = eleAbrusSecP.getTextTrim();
				mainXEB.setAbtha(abrusSecP);
			}
			
			//abeng sec p
			
			String abengSecP = "";
			list = document.selectNodes("/patdoc/SDOAB/abeng/sec/p");
			iter = list.iterator();
			if (iter.hasNext()) {
				Element eleAbengSecP = (Element) iter.next();
				abengSecP = eleAbengSecP.getTextTrim();
				mainXEB.setAbeng(abengSecP);
			}
			
			
			//set
			//b300
			List<String> b310List = new ArrayList<String>();
			List<String> b320YearList = new ArrayList<String>();
			List<String> b320MonthList = new ArrayList<String>();
			List<String> b320DayList = new ArrayList<String>();
			List<String> b330CountryList = new ArrayList<String>();
			list = document.selectNodes("/patdoc/sdobi/B300/B310");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleB310 = (Element) iter.next();
				String tmpB310 = eleB310.getTextTrim();
				b310List.add(tmpB310);
			}
			list = document.selectNodes("/patdoc/sdobi/B300/B320/date/@Year");
			iter = list.iterator();
			while (iter.hasNext()) {
				Attribute attB320Year = (Attribute) iter.next();
				String tmpB320Year = attB320Year.getValue();
				b320YearList.add(tmpB320Year);
			}
			
			list = document.selectNodes("/patdoc/sdobi/B300/B320/date/@Month");
			iter = list.iterator();
			while (iter.hasNext()) {
				Attribute attB320Month = (Attribute) iter.next();
				String tmpB320Month = attB320Month.getValue();
				b320MonthList.add(tmpB320Month);
			}
			
			list = document.selectNodes("/patdoc/sdobi/B300/B320/date/@Day");
			iter = list.iterator();
			while (iter.hasNext()) {
				Attribute attB320Day = (Attribute) iter.next();
				String tmpB320Day = attB320Day.getValue();
				b320DayList.add(tmpB320Day);
			}
	
			list = document.selectNodes("/patdoc/sdobi/B300/B330/ctry");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleb330Country = (Element) iter.next();
				String tmpb330Country = eleb330Country.getTextTrim();
				b330CountryList.add(tmpb330Country);
			}

			for (int j = 0; j < b310List.size(); j++) {
				String b310 = "";
				String b320Year = "";
				String b320Month = "";
				String b320Day = "";
				String b330Country = "";
				
				if (b310List.size() != 0) {
					b310 = b310List.get(j);
				}
				
				if (b320YearList.size() != 0) {
					b320Year = b320YearList.get(j);
				}
				
				if (b320MonthList.size() != 0) {
					b320Month = b320MonthList.get(j);
				}
				
				if (b320DayList.size() != 0) {
					b320Day = b320DayList.get(j);
				}
				
				if (b330CountryList.size() != 0) {
					b330Country = b330CountryList.get(j);
				}
				
				B300Bean b300_bean = new B300Bean();
				b300_bean.setB310(b310);
				b300_bean.setB320Year(b320Year);
				b300_bean.setB320Month(b320Month);
				b300_bean.setB320Day(b320Day);
				b300_bean.setB330Ctry(b330Country);
				b300Set.add(b300_bean);
				mainXEB.setB300(b300Set);
			}
			
			//b512 Set
			
			List<String> b512ClassList = new ArrayList<String>();
			List<String> b512VersionList = new ArrayList<String>();
			list = document.selectNodes("/patdoc/sdobi/B500/B510/B512");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleB512 = (Element) iter.next();
				Iterator b512_class = eleB512.elementIterator("class");
				
				if(b512_class.hasNext()){
					Element ele_b512_class = (Element)b512_class.next();
					b512ClassList.add(ele_b512_class.getTextTrim());
				}else{
					b512ClassList.add("");
				}
				
				
				Iterator b512_version = eleB512.elementIterator("version");
				
				if(b512_version.hasNext()){
					Element ele_b512_version = (Element)b512_version.next();
					b512VersionList.add(ele_b512_version.getTextTrim());
				}else{
					b512VersionList.add("");
				}
			
			}
			
			for (int j = 0; j < b512ClassList.size(); j++) {
				String b512Class = "";
				String b512Version = "";
				
				if (b512ClassList.size() != 0) {
					b512Class = b512ClassList.get(j);
				}
				
				if (b512VersionList.size() != 0) {
					b512Version = b512VersionList.get(j);
				}
				
				B512Bean b512_bean = new B512Bean();
				b512_bean.setB512Class(b512Class);
				b512_bean.setB512Version(b512Version);
				b512Set.add(b512_bean);
				mainXEB.setB512(b512Set);
			}
			
			
			
			
			//b561 Set
			
			List<String> b561CtryList = new ArrayList<String>();
			List<String> b561DnumList = new ArrayList<String>();
			List<String> b561KindList = new ArrayList<String>();
			List<String> b561SnmList = new ArrayList<String>();
			list = document.selectNodes("/patdoc/sdobi/B500/B560/B561");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleB561 = (Element) iter.next();
				Iterator b561_ctry = eleB561.elementIterator("ctry");
				
				if(b561_ctry.hasNext()){
					Element ele_b561_ctry = (Element)b561_ctry.next();
					b561CtryList.add(ele_b561_ctry.getTextTrim());
				}else{
					b561CtryList.add("");
				}
				
				Iterator b561_dnum = eleB561.elementIterator("dnum");
				
				if(b561_dnum.hasNext()){
					Element ele_b561_dnum = (Element)b561_dnum.next();
					b561DnumList.add(ele_b561_dnum.getTextTrim());
				}else{
					b561DnumList.add("");
				}
				
				Iterator b561_kind = eleB561.elementIterator("kind");
				
				if(b561_kind.hasNext()){
					Element ele_b561_kind = (Element)b561_kind.next();
					b561KindList.add(ele_b561_kind.getTextTrim());
				}else{
					b561KindList.add("");
				}
				
				Iterator b561_snm = eleB561.elementIterator("snm");
				
				if(b561_snm.hasNext()){
					Element ele_b561_snm = (Element)b561_snm.next();
					b561SnmList.add(ele_b561_snm.getTextTrim());
				}else{
					b561SnmList.add("");
				}
				
			}
			
			for (int j = 0; j < b561CtryList.size(); j++) {
				String b561Ctry = "";
				String b561Dnum = "";
				String b561Kind = "";
				String b561Snm = "";
				
				if (b561CtryList.size() != 0) {
					b561Ctry = b561CtryList.get(j);
				}
				
				if (b561DnumList.size() != 0) {
					b561Dnum = b561DnumList.get(j);
				}
				
				if (b561KindList.size() != 0) {
					b561Kind = b561KindList.get(j);
				}
				
				if (b561SnmList.size() != 0) {
					b561Snm = b561SnmList.get(j);
				}
				
				B561Bean b561_bean = new B561Bean();
				b561_bean.setB561Ctry(b561Ctry);
				b561_bean.setB561Dnum(b561Dnum);
				b561_bean.setB561Kind(b561Kind);
				b561_bean.setB561Snm(b561Snm);
				b561Set.add(b561_bean);
				mainXEB.setB561(b561Set);
			}
			
			
			//711 Set
			
			List<String> b711SnmList = new ArrayList<String>();
			List<String> b711StrList = new ArrayList<String>();
			List<String> b711CityList = new ArrayList<String>();
			List<String> b711CtryList = new ArrayList<String>();
			
			list = document.selectNodes("/patdoc/sdobi/B700/B710/B711");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleB711 = (Element) iter.next();
				
				Iterator b711_snm = eleB711.elementIterator("snm");
				
				if(b711_snm.hasNext()){
					Element ele_b711_snm = (Element)b711_snm.next();
					b711SnmList.add(ele_b711_snm.getTextTrim());
				}else{
					b711SnmList.add("");
				}
				
				
				Iterator b711_adr = eleB711.elementIterator("adr");
				
				if (!b711_adr.hasNext()) {
					b711StrList.add("");
					b711CityList.add("");
					b711CtryList.add("");
				}
				
				while (b711_adr.hasNext()) {
					Element ele_b711_adr = (Element)b711_adr.next();
					
					Iterator b711_str = ele_b711_adr.elementIterator("str");
					
					if(b711_str.hasNext()){
						Element ele_b711_str = (Element)b711_str.next();
						b711StrList.add(ele_b711_str.getTextTrim());
					}else{
						b711StrList.add("");
					}
					
					
					
					Iterator b711_ctry = ele_b711_adr.elementIterator("ctry");
					
					if(b711_ctry.hasNext()){
						Element ele_b711_ctry = (Element)b711_ctry.next();
						b711CtryList.add(ele_b711_ctry.getTextTrim());
					}else{
						b711CtryList.add("");
					}
					
				}
				
			}
			
			for (int j = 0; j < b711SnmList.size(); j++) {
				String b711Snm = "";
				String b711Str = "";
				String b711City = "";
				String b711Ctry = "";
				
				if (b711SnmList.size() != 0) {
					b711Snm = b711SnmList.get(j);
				}
				
				if (b711StrList.size() != 0) {
					b711Str = b711StrList.get(j);
				}				
				
				if (b711CtryList.size() != 0) {
					b711Ctry = b711CtryList.get(j);
				}
				
				B711Bean b711_bean = new B711Bean();
				b711_bean.setB711Snm(b711Snm);
				b711_bean.setB711Str(b711Str);
				b711_bean.setB711Ctry(b711Ctry);
				b711Set.add(b711_bean);
				mainXEB.setB711(b711Set);
			}
			
			//b721 Set
			
			List<String> b721SnmList = new ArrayList<String>();
			List<String> b721StrList = new ArrayList<String>();
			List<String> b721CityList = new ArrayList<String>();
			List<String> b721CtryList = new ArrayList<String>();
			list = document.selectNodes("/patdoc/sdobi/B700/B720/B721");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleB721 = (Element) iter.next();
				
				Iterator b721_snm = eleB721.elementIterator("snm");
				if(b721_snm.hasNext()){
					Element ele_b721_snm = (Element)b721_snm.next();
					b721SnmList.add(ele_b721_snm.getTextTrim());
				}else{
					b721SnmList.add("");
				}
				
				Iterator b721_adr = eleB721.elementIterator("adr");
				
				if (!b721_adr.hasNext()) {
					b721StrList.add("");
					b721CityList.add("");
					b721CtryList.add("");
				}
				
				while (b721_adr.hasNext()) {
					Element ele_b721_adr = (Element)b721_adr.next();
					
					Iterator b721_str = ele_b721_adr.elementIterator("str");
					
					if(b721_str.hasNext()){
						Element ele_b721_str = (Element)b721_str.next();
						b721StrList.add(ele_b721_str.getTextTrim());
					}else{
						b721StrList.add("");
					}
					
					Iterator b721_city = ele_b721_adr.elementIterator("cntry");
					
					if(b721_city.hasNext()){
						Element ele_b721_city = (Element)b721_city.next();
						b721CityList.add(ele_b721_city.getTextTrim());
					}else{
						b721CityList.add("");
					}
					
					Iterator b721_ctry = ele_b721_adr.elementIterator("ctry");
					
					if(b721_ctry.hasNext()){
						Element ele_b721_ctry = (Element)b721_ctry.next();
						b721CtryList.add(ele_b721_ctry.getTextTrim());
					}else{
						b721CtryList.add("");
					}
					
				}
				
			}
			
			for (int j = 0; j < b721SnmList.size(); j++) {
				String b721Snm = "";
				String b721Str = "";
				String b721City = "";
				String b721Ctry = "";
				
				if (b721SnmList.size() != 0) {
					b721Snm = b721SnmList.get(j);
				}
				
				if (b721StrList.size() != 0) {
					b721Str = b721StrList.get(j);
				}
				
				if (b721CityList.size() != 0) {
					b721City = b721CityList.get(j);
				}
				
				if (b721CtryList.size() != 0) {
					b721Ctry = b721CtryList.get(j);
				}
				B721Bean b721_bean = new B721Bean();
				b721_bean.setB721Snm(b721Snm);
				b721_bean.setB721Str(b721Str);
				b721_bean.setB721City(b721City);
				b721_bean.setB721Ctry(b721Ctry);
				b721Set.add(b721_bean);
				mainXEB.setB721(b721Set);
			}
			
			//b731 Set
			
			List<String> b731SnmList = new ArrayList<String>();
			List<String> b731StrList = new ArrayList<String>();
			List<String> b731CityList = new ArrayList<String>();
			List<String> b731CtryList = new ArrayList<String>();
			list = document.selectNodes("/patdoc/sdobi/B700/B730/B731");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleB731 = (Element) iter.next();
				
				Iterator b731_snm = eleB731.elementIterator("snm");
				
				if(b731_snm.hasNext()){
					Element ele_b731_snm = (Element)b731_snm.next();
					b731SnmList.add(ele_b731_snm.getTextTrim());
				}else{
					b731SnmList.add("");
				}
				
				Iterator b731_adr = eleB731.elementIterator("adr");
				
				if (!b731_adr.hasNext()) {
					b731StrList.add("");
					b731CityList.add("");
					b731CtryList.add("");
				}
				
				while (b731_adr.hasNext()) {
					Element ele_b731_adr = (Element)b731_adr.next();
					
					Iterator b731_str = ele_b731_adr.elementIterator("str");
					
					if(b731_str.hasNext()){
						Element ele_b731_str = (Element)b731_str.next();
						b731StrList.add(ele_b731_str.getTextTrim());
					}else{
						b731StrList.add("");
					}
					
					
					Iterator b731_ctry = ele_b731_adr.elementIterator("ctry");
					
					if(b731_ctry.hasNext()){
						Element ele_b731_ctry = (Element)b731_ctry.next();
						b731CtryList.add(ele_b731_ctry.getTextTrim());
					}else{
						b731CtryList.add("");
					}
					
				}
				
			}
			
			for (int j = 0; j < b731SnmList.size(); j++) {
				String b731Snm = "";
				String b731Str = "";
				String b731City = "";
				String b731Ctry = "";
				
				if (b731SnmList.size() != 0) {
					b731Snm = b731SnmList.get(j);
				}
				
				if (b731StrList.size() != 0) {
					b731Str = b731StrList.get(j);
				}
				
				if (b731CityList.size() != 0) {
					b731City = b731CityList.get(j);
				}
				
				if (b731CtryList.size() != 0) {
					b731Ctry = b731CtryList.get(j);
				}
				
				B731Bean b731_bean = new B731Bean();
				b731_bean.setB731Snm(b731Snm);
				b731_bean.setB731Str(b731Str);
				b731_bean.setB731City(b731City);
				b731_bean.setB731Ctry(b731Ctry);
				b731Set.add(b731_bean);
				mainXEB.setB731(b731Set);
			}
			
			//b741 snm
			
			list = document.selectNodes("/patdoc/sdobi/B700/B740/B741/snm");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleB741Snm = (Element) iter.next();
				String tmpB741Snm = eleB741Snm.getTextTrim();
				B741Bean b741_bean = new B741Bean();
				b741_bean.setB741Snm(tmpB741Snm);
				b741Set.add(b741_bean);
				mainXEB.setB741(b741Set);
			}
			
			//claim
			
			list = document.selectNodes("/patdoc/CLAIMS/CLM");
			iter = list.iterator();
			while (iter.hasNext()) {
				Element eleClm = (Element) iter.next();
				String tmpClm = eleClm.getTextTrim();
				ClaimsBean clm_bean = new ClaimsBean();
				clm_bean.setClaimsClm(tmpClm);
				clmSet.add(clm_bean);
				mainXEB.setClaims(clmSet);
			}
			
			//description
			ConversionStringXml conString = new ConversionStringXml();
			list = conString.getDescriptionList(file, "<description>", "</description>");
			iter = list.iterator();
			
			while (iter.hasNext()) {
				String description = (String)iter.next();
				DescriptionsBean des_bean = new DescriptionsBean();
				des_bean.setDescription(description);
				desSet.add(des_bean);
				mainXEB.setDescriptions(desSet);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mainXEB;
		
	}
	
	
}
