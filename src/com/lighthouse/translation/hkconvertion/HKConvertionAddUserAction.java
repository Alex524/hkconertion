package com.lighthouse.translation.hkconvertion;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.hkconvertion.dao.PdfAddUser;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionAddUserAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
			SessionFactory factory = null;
			ActionForward forword = null;
			Session session = null;
			Transaction transaction = null;
			
		try {
			factory = (SessionFactory) servlet.getServletContext()
			.getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			transaction = session.beginTransaction();
			String userName = request.getParameter("userName");
			String password = request.getParameter("password");
			String country = request.getParameter("country");
			String userGroup = request.getParameter("usergroup");
			PdfAddUser pdfUser = PdfAddUser.getInstance();
			boolean addsuccess = pdfUser.addUser(session, userName, password, country, userGroup);
			
			if (addsuccess) {
				
				transaction.commit();
				forword = mapping.findForward("success");
			} else {
				// add
				transaction.rollback();
				forword = mapping.findForward("exception");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			return mapping.findForward("exception");
		} finally {
			if (session != null)
				session.close();

			if (factory != null)
				factory.close();

		}
		
		
		
		return forword;
	}
}