package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.beans.LockNumber;
import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDao;
import com.lighthouse.translation.hkconvertion.dao.TxtPdfNameDaoimp;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionQAPanelAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Session session = null;
		SessionFactory factory = null;
		try {
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			
			String userName = request.getSession().getAttribute("username").toString();
			TxtPdfNameDao dao = TxtPdfNameDaoimp.getInstance();
			List<SplitTxt> submitList = new ArrayList<SplitTxt>();
			submitList = dao.getPubNumberList(session, "submit", "N");
			
			List list = dao.getQAPubNumberLockList(session, userName, "QA");
			String lockPubNumber = "";
			if (!request.getParameter("lockPubNumber").toString().equals("N")){
				lockPubNumber = request.getParameter("lockPubNumber").toString();
			}
			
			String pubNumber = "";
			if (list.size() != 0){
				SplitTxt lockNumber = (SplitTxt)list.get(0);
				pubNumber = lockNumber.getPubNumber();
			}
			
			request.setAttribute("submitList", submitList);
			request.setAttribute("lockPubNumber", lockPubNumber);
			request.setAttribute("pubNumber", pubNumber);
			return mapping.findForward("xmlqapanel");
		}catch (Exception e) {
			e.printStackTrace();
			return mapping.findForward("exception");
		} finally{
			
			if (session != null){
				session.close();
			}
			
			if (factory != null) {
				factory.close();
			}
			
		}
	}
}