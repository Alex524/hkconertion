package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;
import com.lighthouse.translation.hkconvertion.dao.EditFile;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDao;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDaoimp;
import com.lighthouse.translation.hkconvertion.dao.PdfConditionDao;
import com.lighthouse.translation.hkconvertion.dao.PdfConditionDaoimp;
import com.lighthouse.translation.hkconvertion.dao.PdfShowDao;
import com.lighthouse.translation.hkconvertion.dao.PdfShowDaoimp;
import com.lighthouse.translation.hkconvertion.xml.OutTranslationXml;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionCreateNextQAAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Session session = null;
		Transaction transaction = null;
		SessionFactory factory = null;
		String pdfName = request.getParameter("pdfName");
		String userName = request.getSession().getAttribute("username").toString();
		try{
			
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			transaction = session.beginTransaction();
			
			EditorSubmitDao dao = EditorSubmitDaoimp.getInstance();
			
			System.out.println("pdfName: "+pdfName);
				List<SplitTxt> list = dao.isStatus(session, pdfName, "submit");
				
				Iterator iter = list.iterator();
				String pubNumber = "";
				while (iter.hasNext()) {
					SplitTxt splitTxt = (SplitTxt)iter.next();
					pubNumber = splitTxt.getPubNumber();
					break;
				}
				
				if (list.size()>0) {
					
					PdfShowDao  pdfDao = PdfShowDaoimp.getInstance();
					int lock = 0;
					//锁定文章
					lock = pdfDao.updateLock(session, pubNumber, userName, "QA");
					
					if (lock>0) {
						transaction.commit();
					} else {
						transaction.rollback();
					}
					
					XmlEditBean xmlBean = new XmlEditBean();
					String folderPath = CommonConsts.XML_PATH_Editor;
					File file = new File(folderPath+pdfName+"\\"+pubNumber+".xml");
					OutTranslationXml outXml = new OutTranslationXml();
					xmlBean = outXml.parseXML(file, "false");
					
					if (xmlBean.getB541()==null){
						xmlBean.setB541("EN");
					}
					
					if (xmlBean.getB250() == null){
						xmlBean.setB250("ZH");
					} else if(xmlBean.getB250().equals("CH")){
						xmlBean.setB250("ZH");
					}
					
					PdfShowDao  pdfShowDao = PdfShowDaoimp.getInstance();
					String type = pdfShowDao.getPdf(session, pubNumber);
					request.setAttribute("countryName", "HK");
					request.setAttribute("pdfName", pdfName);
					request.setAttribute("type", type);
					request.setAttribute("xmlBean", xmlBean);
					return mapping.findForward("next");
				} else {
					response.sendRedirect("/HKConvertion/HKConvertionQAPanel.do?lockPubNumber=N");
					return null;
				}
				
			
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			return mapping.findForward("exception");
		} finally{
			
			if (session != null){
				session.close();
			}
			
			if (factory != null) {
				factory.close();
			}
			
		}
		
	}
	
}
