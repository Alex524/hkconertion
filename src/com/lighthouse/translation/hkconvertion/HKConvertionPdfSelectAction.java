package com.lighthouse.translation.hkconvertion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.hkconvertion.beans.FtpBean;
import com.lighthouse.translation.hkconvertion.dao.SplitPDFDao;
import com.lighthouse.translation.hkconvertion.dao.SplitPDFDaoimp;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionPdfSelectAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Session session = null;
		SessionFactory factory = null;
		factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
		session = factory.openSession();
		try{
			SplitPDFDao dao = SplitPDFDaoimp.getInstance();
			ArrayList listSplitPdf = dao.getSplitPDFNameList();
			ArrayList<FtpBean> listPDF = dao.getPdfNameList(listSplitPdf);
			String pdfName = request.getParameter("pdfName");
			request.setAttribute("listPDF", listPDF);
			request.setAttribute("getPdfName", pdfName);
		
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("exception", e.getMessage());
			return mapping.findForward("exception");
		} finally {
			if (session != null)
				session.close();

			if (factory != null)
				factory.close();

		}
		
		return mapping.findForward("selectpdf");
			
	}
}