package com.lighthouse.translation.hkconvertion;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.lighthouse.translation.action.BaseAction;


public class HKConvertionAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String method = request.getParameter("method");
		
		if ("initPdfCondition".equals(method)) {
			return mapping.findForward("initPdfCondition");
		}
		
		if("adduser".equals(method)){
			return mapping.findForward("adduser");
		}
		
		return null;
	}
}
