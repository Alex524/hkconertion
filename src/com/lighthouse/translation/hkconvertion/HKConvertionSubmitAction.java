package com.lighthouse.translation.hkconvertion;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.plugin.HibernatePlugIn;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;
import com.lighthouse.translation.hkconvertion.dao.EditFile;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDao;
import com.lighthouse.translation.hkconvertion.dao.EditorSubmitDaoimp;
import com.lighthouse.translation.hkconvertion.dao.XmlBeanDao;
import com.lighthouse.translation.hkconvertion.dao.XmlBeanDaoimp;
import com.lighthouse.translation.hkconvertion.xml.InTranslationXml;

public class HKConvertionSubmitAction extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String method = request.getParameter("method");
		Session session = null;
		SessionFactory factory = null;
		Transaction transaction = null;
		try {
			factory = (SessionFactory) servlet.getServletContext().getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			transaction = session.beginTransaction();
			
			XmlEditBean xmlBean = new XmlEditBean();
			String pubNumber = request.getParameter("b110").toString().trim()+request.getParameter("b130").toString().trim();
			EditorSubmitDao editorSD = EditorSubmitDaoimp.getInstance();
			
			//获得pdfName
			String pdfName = editorSD.getPdfName(session, pubNumber);
			
			XmlBeanDao dao = XmlBeanDaoimp.getInstance();
			
			//把从页面获得的对象填充进xmlBean
			xmlBean = dao.getXmlBean(request, xmlBean);
			
			InTranslationXml xml = new InTranslationXml();
			
			String xmlPath = CommonConsts.XML_PATH_Editor;
			String newPdfPath = xmlPath+pdfName;
			
			EditFile editFile = new EditFile();
			editFile.newFolder(newPdfPath);
			//生成新的xml文件
			boolean succ = xml.parseXML(xmlBean, newPdfPath);
			
			String status = editorSD.getStatus(session, pubNumber);
			
			
			int i = 0;
			int m = 0;
			int j = 0;
			//更新status状态为submit
			i = editorSD.updateStatus(session, "submit", pubNumber);
			m = editorSD.updateEditorLock(session, "N", pubNumber);
			j = editorSD.updateOperateDate(session, pubNumber);
			if (i>0 && m>0 &&j>0) {
				transaction.commit();
			} else {
				transaction.rollback();
			}
			
			//nextPage
			if ("nextPage".equals(method)) {
				response.sendRedirect("/HKConvertion/XmlCreateNext.do?pdfName="+pdfName+"&status="+status);
				return null;
			}
			
			return null;
			
		}catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			return mapping.findForward("exception");
		} finally{
			
			if (session != null){
				session.close();
			}
			
			if (factory != null) {
				factory.close();
			}
			
		}
		
	}
	
}
