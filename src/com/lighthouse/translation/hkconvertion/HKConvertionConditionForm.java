package com.lighthouse.translation.hkconvertion;

import org.apache.struts.validator.ValidatorActionForm;

public class HKConvertionConditionForm extends ValidatorActionForm {
	private static final long serialVersionUID = 1L;
	
	private String country ="";
	private String number = "";
	private String kind = "";
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	

}
