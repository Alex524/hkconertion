package com.lighthouse.translation.hkconvertion.dao;

import org.hibernate.Session;

import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;

public interface PdfConditionDao {
	
	public XmlEditBean getSplitTxtBean(Session session, String pubNumber) throws Exception;
	public boolean isReopen(Session session, String pubNumber, String userName) throws Exception;
	
}
