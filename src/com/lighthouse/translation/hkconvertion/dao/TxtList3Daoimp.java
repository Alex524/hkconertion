package com.lighthouse.translation.hkconvertion.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.lighthouse.translation.beans.SplitTxt;

public class TxtList3Daoimp implements TxtList3Dao{
	private static TxtList3Daoimp instance;

	public static TxtList3Daoimp getInstance() {
		if (instance == null)

			instance = new TxtList3Daoimp();

		return instance;

	}
	
	//获得第三类list里面的所有bean信息
	public SplitTxt getList3Bean(String file, SplitTxt bean, String pubNumber) throws IOException{
		EditTxt editTxt = new EditTxt();
		FileReader filereader = null;
		BufferedReader reader = null;
		try {
			
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			
			if (java.lang.Character.isDigit(pubNumber.substring(pubNumber.length()-1, pubNumber.length()).charAt(0))) {
				pubNumber = pubNumber.substring(2, pubNumber.length()-2);
			} else {
				pubNumber = pubNumber.substring(2, pubNumber.length()-1);
			}
			
			boolean pubNumberflag = false;
			while((line=reader.readLine())!=null){
				line=line.trim();
				
				if(line.length()>0 && line.split(" ").length > 1){
					
					if (pubNumber.equals(line.split(" ")[1])){
						pubNumberflag = true;
					}
					
					if (pubNumberflag) {
						bean.setAppNumber(line.split(" ")[0]);
					}
					
					pubNumberflag = false;
					
				}
			
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
		
		return bean;
	}
	
}
