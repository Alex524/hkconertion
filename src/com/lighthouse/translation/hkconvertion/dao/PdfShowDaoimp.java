package com.lighthouse.translation.hkconvertion.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.common.consts.SqlManager;

public class PdfShowDaoimp implements PdfShowDao{
	
	private static PdfShowDaoimp instance;

	public static PdfShowDaoimp getInstance() {
		
		if (instance == null)

			instance = new PdfShowDaoimp();

		return instance;

	}
	
	
	public String getPdf(Session session, String pubNumber) throws Exception {
		
		Query query = null;
		List<SplitTxt> list = new ArrayList<SplitTxt>();
		String type = "";
		try {
			String sql = CommonConsts.HQL_PDFNAME_STATUS;
			query = session.createQuery(sql);
			query.setString("value1", pubNumber);
			list = query.list();
			Iterator iter = list.iterator();
			while (iter.hasNext()) {
				SplitTxt splitTxt = (SplitTxt)iter.next();
				type = splitTxt.getType();
			} 
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return type;
		
	}
	
	
	//根据pubNumber锁定文章.
	public int updateLock(Session session, String pubNumber, String userName, String userGroup) throws Exception {
		Query query = null;
		int i = 0;
		List list = new ArrayList<SplitTxt>();
		try {
			String hql = CommonConsts.HQL_LOCK;
			SqlManager sqlManager = new SqlManager();
			String[] sql_fileds = new String[2];
			
			if (userGroup.equals("QA")) {
				sql_fileds[0] = "qaId";
			} else {
				sql_fileds[0] = "editorId";
			}
			
			sql_fileds[1] = "pubNumber";
			hql = sqlManager.setFiledsValue(hql, sql_fileds);
			query = session.createQuery(hql);
			query.setString("value1", userName);
			query.setString("value2", pubNumber);
			i = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return i;
	}
	
	
	//查询文章是否被锁定
	public boolean selectUserNameLock(Session session, String pubNumber, String userName, String userGroup) throws Exception{
		boolean succ = false;
		Query query = null;
		List list = null;
		try {
			
			String hql = CommonConsts.HQL_USERNAME_LOCK;
			SqlManager sqlManager = new SqlManager();
			String[] sql_fileds = new String[2];
			sql_fileds[0] = "pubNumber";
			
			if (userGroup.equals("QA")) {
				sql_fileds[1] = "qaId";
			} else {
				sql_fileds[1] = "editorId";
			}
			
			hql = sqlManager.setFiledsValue(hql, sql_fileds);
			query = session.createQuery(hql);
			query.setString("value1", pubNumber);
			query.setString("value2", userName);
			list = query.list();
			session.flush();
			session.clear();
			if (list.size() == 0) {
				succ = true;
			}
		} catch (Exception e) {

			throw e;
		}
		return succ;
	}
		
}
