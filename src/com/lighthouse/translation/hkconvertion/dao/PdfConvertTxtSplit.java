package com.lighthouse.translation.hkconvertion.dao;

import java.io.File;
import java.util.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.pdfbox.exceptions.COSVisitorException;
import org.pdfbox.pdfparser.PDFParser;
import org.pdfbox.pdfwriter.COSWriter;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.Splitter;
import org.pdfbox.util.PDFTextStripper;

public class PdfConvertTxtSplit {
	
	private static PdfConvertTxtSplit instance;

	/**
	 * 
	 * @return This method used to get this class's instance.
	 */
	public static PdfConvertTxtSplit getInstance() {
		if (instance == null) // 1

			instance = new PdfConvertTxtSplit(); // 2

		return instance; // 3

	}
	
	
	public void split() throws IOException, COSVisitorException {
		String split = "1";
		Splitter splitter = new Splitter();
	    InputStream input = null;
	    PDDocument document = null;
	    List documents = null;
	     
	    String pdfFile = "E:\\Work Station\\PdfConvertTxt\\Patent_02022011.pdf";
	     
	     try
         {
	    	 input = new FileInputStream(pdfFile);
	    	 document = parseDocument(input);  
             splitter.setSplitAtPage(Integer.parseInt( split ));
             documents = splitter.split(document);
	    	 
             for ( int i=0; i<documents.size(); i++ )
             {
                 PDDocument doc = (PDDocument)documents.get( i );
               
                String fileName = "E:\\Work Station\\PdfConvertTxt\\pdf\\temp-"+i+".pdf";
                writeTxt(doc,fileName);
                doc.close();
             }
         } catch (FileNotFoundException e) {
        	 e.printStackTrace();
		 } finally {
			 if( input != null )
             {
                 input.close();
             }
             if( document != null )
             {
                 document.close();
             }
             for( int i=0; documents != null && i<documents.size(); i++ )
             {
                 PDDocument doc = (PDDocument)documents.get( i );
                 doc.close();
             }
         }
	}
	
	public void createTxt() throws IOException {
		String pdfSplitterFile = "E:\\Work Station\\PdfConvertTxt\\pdf\\";
		String txtFile = "E:\\Work Station\\PdfConvertTxt\\txt\\";
		PDDocument document = null;
		Writer output = null;
		
		try
        {
		File[] files = this.getFiles(pdfSplitterFile);
		
        	if (files!=null) {
        	
        		for (int i=0;i<files.length;i++) {
        			File pdf_file = new File(pdfSplitterFile+files[i].getName());
            
        			if(pdf_file.exists()){
        				document = PDDocument.load(pdfSplitterFile+files[i].getName());
        				String fileName = txtFile+"temp-"+i+".txt";
        				output = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");   
        				PDFTextStripper stripper = null;
        				stripper = new PDFTextStripper();
        				stripper.writeText(document, output);
        				document.close();
        			}
            	
        		}
        	
        	}
        
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
		} finally {
	    	 
			if (output != null) {    
				output.close();     
	    	}
	    	 
	    	if (document != null) {    
	    		document.close();     
	    	}
	    	
        }
		
	}
	
	private static PDDocument parseDocument( InputStream input )throws IOException {
        PDFParser parser = new PDFParser( input );
        parser.parse();
        return parser.getPDDocument();
    }
	
	 private void writeTxt(PDDocument doc, String fileName) throws IOException, COSVisitorException {
		 FileOutputStream output = null;
	     COSWriter writer = null;
	     try
	     {
	    	 output = new FileOutputStream( fileName );
	            writer = new COSWriter( output );
	            writer.write( doc );
	        }
	        finally
	        {
	            if( output != null )
	            {
	                output.close();
	            }
	            if( writer != null )
	            {
	                writer.close();
	            }
	        }
	    }
	 
	 protected File[] getFiles(String path)
	    {
	        File file = new  File(path);
	        File files[] = null;
	        if(file.isDirectory())
	            files = file.listFiles();
	        else
	        {
	            files =new File[1];
	            files[0] = file;
	        }
	        return files;
	    }
	
	
}
