package com.lighthouse.translation.hkconvertion.dao;

import java.io.IOException;

import com.lighthouse.translation.beans.SplitTxt;

public interface TxtList3Dao {
	public SplitTxt getList3Bean(String file, SplitTxt bean, String pubNumber) throws IOException;
}
