package com.lighthouse.translation.hkconvertion.dao;

public interface TxtSplitDao {
	public void delExcessTxt(String fileName, String filePath) throws Exception;
	public void createDirectoryTxt(String file, String applicationsFilePath, String grantsFilePath, String shortGrantsFilePath) throws Exception;
	public void createListTxt(String file, String type) throws Exception;
	public void delExcessList2(String file) throws Exception;
	public void mergeListTxt(String file, String type) throws Exception;
}
