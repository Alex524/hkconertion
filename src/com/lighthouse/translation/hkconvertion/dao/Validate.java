package com.lighthouse.translation.hkconvertion.dao;

public class Validate {
	
	private static Validate instance;

	public static Validate getInstance() {
		if (instance == null) 

			instance = new Validate();

		return instance;

	}
	
	public String getMonth(String month){
		String[] monthStr = {"January,Jan,01","February,Feb,02","March,Mar,03","April,Apr,04","May,May,05","June,Jun,06","July,Jul,07","August,Aug,08","September,Sept,09","October,Oct,10","November,Nov,11","December,Dec,12"};
		String getMonth = "";
		for (int i=0; i<monthStr.length; i++) {
				if (monthStr[i].split(",")[0].equals(month) || monthStr[i].split(",")[1].equals(month)){
					getMonth = monthStr[i].split(",")[2];
				}
		}
		
		return getMonth;
	}
}
