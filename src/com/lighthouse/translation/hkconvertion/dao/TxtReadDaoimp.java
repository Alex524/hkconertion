package com.lighthouse.translation.hkconvertion.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.SplitTxtBean;

public class TxtReadDaoimp implements TxtReadDao{
	private static TxtReadDaoimp instance;

	public static TxtReadDaoimp getInstance() {
		if (instance == null)

			instance = new TxtReadDaoimp();

		return instance;

	}
	
	//获取3个list2文件路径
	public List getFilePath(String file1, String file2, String file3) throws IOException {
		List list = new ArrayList();
		List list1 = new ArrayList();
		List list2 = new ArrayList();
		list1 = getPubNumber(file1, list1, "A0", "applications");
		list2 = getPubNumber(file2, list1, "A1", "grants");
		list = getPubNumber(file3, list2, "A2", "shortgrants");
		return list;
	}
	
	//获取pubNumber列表
	public List getPubNumber(String file, List list, String kind, String type) throws IOException {
		//ArrayList listPubNumber = new ArrayList();
		FileReader filereader = null;
		BufferedReader reader = null;
		EditTxt editTxt = new EditTxt();
		try {
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			StringBuffer allLineStringBuffer = new StringBuffer();
			while((line=reader.readLine())!=null){
				line=line.trim();
				
				if(line.length()>0){
					
					if (editTxt.isNumber(editTxt.isPubNumber(line))) {
						list.add("HK"+editTxt.isPubNumber(line)+kind+"--Ryan--"+type);
					}
					
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
		
		return list;
		
	}
	
	
	
	
	//储存Bean对象
	public boolean addTxt(Session session, List list, String file1, String file2, String file3, String file4, String pdfName) throws Exception{
		boolean succ = false;
		
		TxtList1Dao dao1 = TxtList1Daoimp.getInstance();
		TxtList2Dao dao2 = TxtList2Daoimp.getInstance();
		TxtList3Dao dao3 = TxtList3Daoimp.getInstance();
		try {
			
			for (int i=0; i<list.size(); i++) {
				SplitTxt bean = new SplitTxt();
				bean.setPdfName(pdfName);
				bean.setStatus("submission");
				bean.setIpc("8");
				bean.setLock("N");
				String publDate = pdfName.substring(pdfName.length()-8, pdfName.length());
				boolean bool = this.getApplicationsGrantDate(list.get(i).toString().split("--Ryan--")[0]);
				if (bool) {
					bean.setGrantDate(publDate.substring(0, 2)+"/"+publDate.substring(2, 4)+"/"+publDate.substring(4, publDate.length()));
				}
				bean.setPublDate(publDate.substring(0, 2)+"/"+publDate.substring(2, 4)+"/"+publDate.substring(4, publDate.length()));
				bean = dao1.getList1Bean(file1, bean, list.get(i).toString());
				bean = dao2.getList2Bean(file2, bean, list.get(i).toString().split("--Ryan--")[0]);
				bean = dao3.getList3Bean(file3, bean, list.get(i).toString().split("--Ryan--")[0]);
				session.save(bean);
			}
			
			session.flush();
			session.clear();
			succ = true;
		} catch (Exception e) {

			throw e;
		}
		return succ;
		
		
	}
	
	//判断pubNumber是否出现在Applicationslist里面
	private boolean getApplicationsGrantDate(String pubNumber)throws Exception{
		
		if (java.lang.Character.isDigit(pubNumber.substring(pubNumber.length()-1, pubNumber.length()).charAt(0))) {
			pubNumber = pubNumber.substring(2, pubNumber.length()-2);
		} else {
			pubNumber = pubNumber.substring(2, pubNumber.length()-1);
		}
		
		String file = CommonConsts.TXT_PATH+"temp\\applicationslist2.txt";
		FileReader filereader = null;
		BufferedReader reader = null;
		boolean bool = true;
		try {
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			while((line=reader.readLine())!=null){
				line=line.trim();
				
				if(line.length()>0 && line.contains(pubNumber)){
					bool = false;
					break;
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
		return bool;
	}
	
	
	
	
	
	

	
	
	
	
	
}
