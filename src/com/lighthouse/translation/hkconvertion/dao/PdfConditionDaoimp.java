package com.lighthouse.translation.hkconvertion.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.B512Bean;
import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;

public class PdfConditionDaoimp implements PdfConditionDao{
	
	private static PdfConditionDaoimp instance;

	public static PdfConditionDaoimp getInstance() {
		if (instance == null)

			instance = new PdfConditionDaoimp();

		return instance;

	}
	
	//根据pubNumber获得bean对象，填充进XmlEditBean
	public XmlEditBean getSplitTxtBean(Session session, String pubNumber) throws Exception {
		XmlEditBean bean = new XmlEditBean();
		List<SplitTxt> list = new ArrayList<SplitTxt>();
		Query query = null;
		String number = "";
		String kind = "";
		try {
			
			if (java.lang.Character.isDigit(pubNumber.substring(pubNumber.length()-1, pubNumber.length()).charAt(0))) {
				number = pubNumber.substring(0, pubNumber.length()-2);
				kind = pubNumber.substring(pubNumber.length()-2, pubNumber.length());
			} else {
				number = pubNumber.substring(0, pubNumber.length()-1);
				kind = pubNumber.substring(pubNumber.length()-1, pubNumber.length());
			}
			
			String sql = CommonConsts.HQL_PUBNUMBER_LIST;
			query = session.createQuery(sql);
			query.setString("value1", pubNumber);
			list = query.list();
			Iterator iter = list.iterator();
			while (iter.hasNext()) {
				SplitTxt txtBean = (SplitTxt)iter.next();
				bean.setPdfName(txtBean.getPdfName());
				bean.setB110(number);
				bean.setB130(kind);
				bean.setB210(txtBean.getAppNumber());
				if (!"".equals(txtBean.getAppDate())&&txtBean.getAppDate() != null){
					bean.setB220Day(txtBean.getAppDate().substring(0, 2));
					bean.setB220Month(txtBean.getAppDate().substring(3, 5));
					bean.setB220Year(txtBean.getAppDate().substring(6, txtBean.getAppDate().length()));
				}
				
				bean.setB250(txtBean.getAppLanguage());
				bean.setB516(txtBean.getIpc());
				
				if (txtBean.getWoAppDate() != null){
					bean.setB861Day(txtBean.getWoAppDate().substring(0, 2));
					bean.setB861Month(txtBean.getWoAppDate().substring(3, 5));
					bean.setB861Year(txtBean.getWoAppDate().substring(6, 10));
				}
				
				bean.setB861Anum(txtBean.getWoAppNumber());
				bean.setB871Pnum(txtBean.getWoPublNumber());
				
				if (txtBean.getWoPublDate() != null){
					bean.setB871Day(txtBean.getWoPublDate().substring(0, 2));
					bean.setB871Month(txtBean.getWoPublDate().substring(3, 5));
					bean.setB871Year(txtBean.getWoPublDate().substring(6, 10));
				}
				
				if (txtBean.getIpcClass() != null) {
					
					if (txtBean.getIpcClass().split(",").length == 1) {
						bean.setB511Class(txtBean.getIpcClass());
					} else {
						bean.setB511Class(txtBean.getIpcClass().split(",")[0]);
						int b512_number = txtBean.getIpcClass().split(",").length-1;
						Set b512List = new LinkedHashSet(0);
						
						for (int i = 0; i < b512_number; i++) {
							B512Bean b512 = new B512Bean();
							String b512Class = txtBean.getIpcClass().split(",")[i+1];
							b512.setB512Class(b512Class);
							b512List.add(b512);
							
							if (!"".equals(b512Class)) {
								bean.setB512(b512List);
							}
							
						}
						
					}
					
				}
				
				if (txtBean.getPublDate() != null) {
					bean.setB430Day(txtBean.getPublDate().split("/")[1]);
					bean.setB430Month(txtBean.getPublDate().split("/")[0]);
					bean.setB430Year(txtBean.getPublDate().split("/")[2]);
				}
				
				if (txtBean.getGrantDate() != null) {
					bean.setB450Day(txtBean.getGrantDate().split("/")[1]);
					bean.setB450Month(txtBean.getGrantDate().split("/")[0]);
					bean.setB450Year(txtBean.getGrantDate().split("/")[2]);
				}
				
				if (txtBean.getDivDnum() != null){
					bean.setB620Dnum(txtBean.getDivDnum());
				}
				
				if (txtBean.getDivDate() != null) {
					bean.setB620Day(txtBean.getDivDate().substring(0, 2));
					bean.setB620Month(txtBean.getDivDate().substring(3, 5));
					bean.setB620Year(txtBean.getDivDate().substring(6, 10));
				}
				
				
				bean.setB542(txtBean.getTitle());
				bean.setB655Ctry(txtBean.getRefCountry());
				bean.setB655Pnum(txtBean.getRefNumber());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return bean;
	}
	
	
	public boolean isReopen(Session session, String pubNumber, String userName) throws Exception {
		XmlEditBean bean = new XmlEditBean();
		List<SplitTxt> list = new ArrayList<SplitTxt>();
		Query query = null;
		boolean bool = true;
		try {
			String sql = CommonConsts.HQL_REOPEN_STATUS;
			query = session.createQuery(sql);
			query.setString("value1", pubNumber);
			query.setString("value2", userName);
			list = query.list();
			if (list.size() == 0) {
				bool = false;
			}
			
	
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return bool;
	}
}
