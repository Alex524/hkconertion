package com.lighthouse.translation.hkconvertion.dao;

import org.hibernate.Session;

public interface LogoutUnlockDao {
	public int getUnlock(Session session, String userName, String userGroup) throws Exception;

}
