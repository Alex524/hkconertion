package com.lighthouse.translation.hkconvertion.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditTxt {
	
	//截取pubNumber
	public String isPubNumber(String line) {
		
		for (int i=0; i<line.length(); i++) {
			int ii = (int) line.charAt(i);
			
			if (ii==32) {
				return line.substring(0, i);
			}
			
		}
		
		if (isNumber(line)) {
			return line;
		}
		
		return "NULL";
		
	}
	
	//判断是否是数字
	public boolean isNumber(String str) { 
		Pattern pattern = Pattern.compile("[0-9]*"); 
		Matcher isNum = pattern.matcher(str);
		
		if( !isNum.matches() ){
		    return false; 
		} 
			return true; 
	}
	
	//截取[11] pubNumber 位置
	
	public boolean isPubNumber_11(String line, String pubNumber) {
		String pubNumber_11 = this.getKind(line, "[11]");
		if (!isNumber(pubNumber_11)){
			return false;
		}
		
		if (java.lang.Character.isDigit(pubNumber.substring(pubNumber.length()-1, pubNumber.length()).charAt(0))) {
			pubNumber = pubNumber.substring(2, pubNumber.length()-2);
		} else {
			pubNumber = pubNumber.substring(2, pubNumber.length()-1);
		}
		
		
		
		if (pubNumber_11.equals(pubNumber)) {
			return true;
		}
		
		return false;
	}
	
	//截取指定字符串后面出现的值,[]里面数字是2位数
	public String getKind(String line, String StrName) {
		//if (line.indexOf(StrName) == line.length()-4){
		//	return " ";
		//}
		
		if (line.substring(line.indexOf(StrName), line.length()).equals(StrName)){
			//System.out.println(line);
			return "";
		}
		int startInt = line.indexOf(StrName)+5;
		return line.substring(startInt, line.length());
		
	}
	
	
}
