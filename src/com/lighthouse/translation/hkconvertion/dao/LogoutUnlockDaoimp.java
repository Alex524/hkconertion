package com.lighthouse.translation.hkconvertion.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.common.consts.SqlManager;

public class LogoutUnlockDaoimp implements LogoutUnlockDao{
		
	private static LogoutUnlockDaoimp instance;

	public static LogoutUnlockDaoimp getInstance() {
		if (instance == null)

			instance = new LogoutUnlockDaoimp();

		return instance;

	}
	
	//根据pubNumber锁定文章.
	public int getUnlock(Session session, String userName, String userGroup) throws Exception {
		Query query = null;
		int i = 0;
		List list = new ArrayList<SplitTxt>();
		try {
			String hql = "";
			if (userGroup.equals("QA")) {
				hql = CommonConsts.HQL_QA_UNLOCK;
			} else {
				hql = CommonConsts.HQL_EDITOR_UNLOCK;
			}
			SqlManager sqlManager = new SqlManager();
			String[] sql_fileds = new String[2];
			
			if (userGroup.equals("QA")) {
				sql_fileds[0] = "qaId";
			} else {
				sql_fileds[0] = "editorId";
			}
			sql_fileds[1] = "status";
			hql = sqlManager.setFiledsValue(hql, sql_fileds);
			query = session.createQuery(hql);
			query.setString("value1", userName);
			if (userGroup.equals("QA")){
				query.setString("value2", "submit");
			} else {
				query.setString("value2", "submission");
			}
			i = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return i;
	}

}
