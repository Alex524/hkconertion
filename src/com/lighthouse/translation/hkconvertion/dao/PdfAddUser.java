package com.lighthouse.translation.hkconvertion.dao;

import org.hibernate.Session;



import com.lighthouse.translation.beans.UserTable;

public class PdfAddUser {
	
private static PdfAddUser instance;

	
	public static PdfAddUser getInstance() {
		if (instance == null) 

			instance = new PdfAddUser();

		return instance;

	}
	public boolean addUser(Session session, String userName, String password, String country, String userGroup) throws Exception {
		boolean succ = false;

		try {
			
			UserTable pdfUser = new UserTable();
			pdfUser.setUserName(userName);
			pdfUser.setPassword(password);
			pdfUser.setCountry(country);
			pdfUser.setUsergroup(userGroup);
			session.save(pdfUser);

			session.flush();
	// get the lastest value
			session.clear();
			succ = true;
		} catch (Exception e) {

			throw e;
		}
		return succ;
	}
}
