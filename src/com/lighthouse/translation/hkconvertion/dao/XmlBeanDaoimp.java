package com.lighthouse.translation.hkconvertion.dao;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.lighthouse.translation.hkconvertion.beans.*;
import com.lighthouse.translation.hkconvertion.dao.Validate;

public class XmlBeanDaoimp implements XmlBeanDao{
	
	private static XmlBeanDaoimp instance;

	public static XmlBeanDaoimp getInstance() {
		if (instance == null)

			instance = new XmlBeanDaoimp();

		return instance;

	}
	
	//根据提交页面获得bean对象
	public XmlEditBean getXmlBean(HttpServletRequest request, XmlEditBean xmlBean) throws Exception {
		try {
			Validate validate = new Validate();
			//b100
			xmlBean.setB110(this.getConStr(request.getParameter("b110")));
			xmlBean.setB130(this.getConStr(request.getParameter("b130")));
			String country = request.getParameter("b110").substring(0, 2);
			
			//b200
			xmlBean.setB210(this.getConStr(request.getParameter("b210")));
			
			if (!request.getParameter("b220Date").equals("")) {
				
				if (request.getParameter("b220Date").substring(2,3).equals(" ")) {
					xmlBean.setB220Year(request.getParameter("b220Date").split(" ")[2]);
					xmlBean.setB220Month(validate.getMonth(request.getParameter("b220Date").split(" ")[1]));
					xmlBean.setB220Day(request.getParameter("b220Date").split(" ")[0]);
				} else {
					xmlBean.setB220Year(request.getParameter("b220Date").substring(6,10));
					xmlBean.setB220Month(request.getParameter("b220Date").substring(3,5));
					xmlBean.setB220Day(request.getParameter("b220Date").substring(0,2));
				}
				
			}
			
			xmlBean.setB250(this.getConStr(request.getParameter("b250")));
			
			
			//b300
			int b300_number = Integer.parseInt(request.getParameter("b300_number"));
			
			Set b300List = new LinkedHashSet(0);
			for (int i = 0; i < b300_number; i++) {
				B300Bean b300 = new B300Bean();
				String b310 = this.getConStr(request.getParameter("b310_" + i));
				
				if (!request.getParameter("b320Date_"+i).equals("")) {
					String b320Year = "";
					String b320Month = "";
					String b320Day = "";
					if (request.getParameter("b320Date_"+i).substring(2,3).equals(" ")) {
						b320Year = request.getParameter("b320Date_"+i).split(" ")[2];
						b320Month = validate.getMonth(request.getParameter("b320Date_"+i).split(" ")[1]);
						b320Day = request.getParameter("b320Date_"+i).split(" ")[0];
					} else {
						b320Year = request.getParameter("b320Date_"+i).substring(6, 10);
						b320Month = request.getParameter("b320Date_"+i).substring(3,5);
						b320Day = request.getParameter("b320Date_"+i).substring(0,2);
					}
					
					
					b300.setB320Year(b320Year);
					b300.setB320Month(b320Month);
					b300.setB320Day(b320Day);
				}
				
				String b330Ctry = request.getParameter("b330Ctry_" + i);
				b300.setB310(b310.trim());
				b300.setB330Ctry(b330Ctry);
				b300List.add(b300);
				xmlBean.setB300(b300List);
				
			}
			
			//b400
			if (!request.getParameter("b430Date").equals("")) {
				
				if (request.getParameter("b430Date").substring(2, 3).equals(" ")) {
					xmlBean.setB430Year(request.getParameter("b430Date").split(" ")[2]);
					xmlBean.setB430Month(validate.getMonth(request.getParameter("b430Date").split(" ")[1]));
					xmlBean.setB430Day(request.getParameter("b430Date").split(" ")[0]);
				} else{
					xmlBean.setB430Year(request.getParameter("b430Date").substring(6, 10));
					xmlBean.setB430Month(request.getParameter("b430Date").substring(3,5));
					xmlBean.setB430Day(request.getParameter("b430Date").substring(0,2));
				}
				
			}
			
			if (!request.getParameter("b450Date").equals("")) {
				
				if (request.getParameter("b450Date").substring(2,3).equals(" ")) {
					xmlBean.setB450Year(request.getParameter("b450Date").split(" ")[2]);
					xmlBean.setB450Month(validate.getMonth(request.getParameter("b450Date").split(" ")[1]));
					xmlBean.setB450Day(request.getParameter("b450Date").split(" ")[0]);
				} else {
					xmlBean.setB450Year(request.getParameter("b450Date").substring(6, 10));
					xmlBean.setB450Month(request.getParameter("b450Date").substring(3,5));
					xmlBean.setB450Day(request.getParameter("b450Date").substring(0,2));
				}
				
				
			}
			
			//b500
			xmlBean.setB511Class(this.getConStr(request.getParameter("b511Class")));
			xmlBean.setB511Version(this.getConStr(request.getParameter("b511Version")));
			xmlBean.setB516(this.getConStr(request.getParameter("b516")));
			xmlBean.setB541(this.getConStr(request.getParameter("b541")));
			xmlBean.setB542(this.getConStr(request.getParameter("b542")));
			xmlBean.setB563(this.getConStr(request.getParameter("b563")));
			
			//562
			int b562_number = Integer.parseInt(request.getParameter("b562_number"));
			
			Set b562List = new LinkedHashSet(0);
			for (int i = 0; i < b562_number; i++) {
				B562Bean b562 = new B562Bean();
				String b562Reference = this.getConStr(request.getParameter("b562Reference_" + i));
				b562.setB562Reference(b562Reference);
				b562List.add(b562);
				if (!"".equals(b562Reference)) {
					xmlBean.setB562(b562List);
				}
				
			}
			
			int b512_number = Integer.parseInt(request.getParameter("b512_number"));
			
			Set b512List = new LinkedHashSet(0);
			for (int i = 0; i < b512_number; i++) {
				B512Bean b512 = new B512Bean();
				String b512Class = this.getConStr(request.getParameter("b512Class_" + i));
				String b512Version = this.getConStr(request.getParameter("b512Version_" + i));
				b512.setB512Class(b512Class);
				b512.setB512Version(b512Version);
				b512List.add(b512);
				if (!"".equals(b512Class) || !"".equals(b512Version)) {
					xmlBean.setB512(b512List);
				}
				
			}
			
			int b561_number = Integer.parseInt(request.getParameter("b561_number"));
			
			Set b561List = new LinkedHashSet(0);
			for (int i = 0; i < b561_number; i++) {
				B561Bean b561 = new B561Bean();
				String b561Ctry = this.getConStr(request.getParameter("b561Ctry_" + i));
				String b561Dnum = this.getConStr(request.getParameter("b561Dnum_" + i));
				String b561Kind = this.getConStr(request.getParameter("b561Kind_" + i));
				String b561Snm = this.getConStr(request.getParameter("b561Snm_" + i));
				b561.setB561Ctry(b561Ctry);
				b561.setB561Dnum(b561Dnum);
				b561.setB561Kind(b561Kind);
				b561.setB561Snm(b561Snm);
				b561List.add(b561);
				if (!"".equals(b561Ctry) || !"".equals(b561Dnum) || !"".equals(b561Kind) || !"".equals(b561Snm)) {
					xmlBean.setB561(b561List);
				}
				
			}
			
			//b600
			
			if (!request.getParameter("b620Date").equals("")) {
				
				if (request.getParameter("b620Date").substring(2,3).equals(" ")) {
					xmlBean.setB620Year(request.getParameter("b620Date").split(" ")[2]);
					xmlBean.setB620Month(validate.getMonth(request.getParameter("b620Date").split(" ")[1]));
					xmlBean.setB620Day(request.getParameter("b620Date").split(" ")[0]);
				} else {
					xmlBean.setB620Year(request.getParameter("b620Date").substring(6,10));
					xmlBean.setB620Month(request.getParameter("b620Date").substring(3,5));
					xmlBean.setB620Day(request.getParameter("b620Date").substring(0,2));
				}
				
			}
			
			xmlBean.setB620Dnum(this.getConStr(request.getParameter("b620Dnum")));
			xmlBean.setB655Ctry(this.getConStr(request.getParameter("b655Ctry")));
			xmlBean.setB655Pnum(this.getConStr(request.getParameter("b655Pnum")));
			xmlBean.setB655Kind(this.getConStr(request.getParameter("b655Kind")));
			
			//b700
			int b711_number = Integer.parseInt(request.getParameter("b711_number"));
			
			Set b711List = new LinkedHashSet(0);
			for (int i = 0; i < b711_number; i++) {
				B711Bean b711 = new B711Bean();
				String b711Snm = this.getConStr(request.getParameter("b711Snm_" + i));
				String b711Str = this.getConStr(request.getParameter("b711Str_" + i));
				String b711Ctry = this.getConStr(request.getParameter("b711Ctry_" + i));
				b711.setB711Snm(b711Snm);
				b711.setB711Str(b711Str);
				b711.setB711Ctry(b711Ctry);
				b711List.add(b711);
				if (!"".equals(b711Snm) || !"".equals(b711Str) || !"".equals(b711Ctry)) {
					xmlBean.setB711(b711List);
				}
				
			}
			
			int b721_number = Integer.parseInt(request.getParameter("b721_number"));
			
			Set b721List = new LinkedHashSet(0);
			for (int i = 0; i < b721_number; i++) {
				B721Bean b721 = new B721Bean();
				String b721Snm = this.getConStr(request.getParameter("b721Snm_" + i));
				String b721Str = this.getConStr(request.getParameter("b721Str_" + i));
				String b721City = this.getConStr(request.getParameter("b721City_" + i));
				String b721Ctry = this.getConStr(request.getParameter("b721Ctry_" + i));
				b721.setB721Snm(b721Snm);
				b721.setB721Str(b721Str);
				b721.setB721City(b721City);
				b721.setB721Ctry(b721Ctry);
				b721List.add(b721);
				if (!"".equals(b721Snm) || !"".equals(b721Str) || !"".equals(b721City) || !"".equals(b721Ctry)) {
					xmlBean.setB721(b721List);
				}
				
			}
			
			int b731_number = Integer.parseInt(request.getParameter("b731_number"));
			
			Set b731List = new LinkedHashSet(0);
			for (int i = 0; i < b731_number; i++) {
				B731Bean b731 = new B731Bean();
				String b731Snm = this.getConStr(request.getParameter("b731Snm_" + i));
				String b731Str = this.getConStr(request.getParameter("b731Str_" + i));
				String b731Ctry = this.getConStr(request.getParameter("b731Ctry_" + i));
				b731.setB731Snm(b731Snm);
				b731.setB731Str(b731Str);
				b731.setB731Ctry(b731Ctry);
				b731List.add(b731);
				if (!"".equals(b731Snm) || !"".equals(b731Str) || !"".equals(b731Ctry)) {
					xmlBean.setB731(b731List);
				}
				
			}
			
			int b741_number = Integer.parseInt(request.getParameter("b741_number"));
			
			Set b741List = new LinkedHashSet(0);
			for (int i = 0; i < b741_number; i++) {
				B741Bean b741 = new B741Bean();
				String b741Snm = this.getConStr(request.getParameter("b741Snm_" + i));
				b741.setB741Snm(b741Snm);
				b741List.add(b741);
				if (!"".equals(b741Snm)) {
					xmlBean.setB741(b741List);
				}
				
			}
			

			//b800
			if (!request.getParameter("b850Date").equals("")) {
				
				if (request.getParameter("b850Date").substring(2,3).equals(" ")) {
					xmlBean.setB850Year(request.getParameter("b850Date").split(" ")[2]);
					xmlBean.setB850Month(validate.getMonth(request.getParameter("b850Date").split(" ")[1]));
					xmlBean.setB850Day(request.getParameter("b850Date").split(" ")[0]);
				} else {
					xmlBean.setB850Year(request.getParameter("b850Date").substring(6,10));
					xmlBean.setB850Month(request.getParameter("b850Date").substring(3,5));
					xmlBean.setB850Day(request.getParameter("b850Date").substring(0,2));
				}
				
			}
			
			xmlBean.setB861Anum(this.getConStr(request.getParameter("b861Anum")));
			
			if (!request.getParameter("b861Date").equals("")) {
				
				if (request.getParameter("b861Date").substring(2,3).equals(" ")) {
					xmlBean.setB861Year(request.getParameter("b861Date").split(" ")[2]);
					xmlBean.setB861Month(validate.getMonth(request.getParameter("b861Date").split(" ")[1]));
					xmlBean.setB861Day(request.getParameter("b861Date").split(" ")[0]);
				} else {
					xmlBean.setB861Year(request.getParameter("b861Date").substring(6,10));
					xmlBean.setB861Month(request.getParameter("b861Date").substring(3,5));
					xmlBean.setB861Day(request.getParameter("b861Date").substring(0,2));
				}
				
			}
			
			xmlBean.setB871Pnum(this.getConStr(request.getParameter("b871Pnum")));
			if (!request.getParameter("b871Date").equals("")) {
				
				if (request.getParameter("b871Date").substring(2,3).equals(" ")) {
					xmlBean.setB871Year(request.getParameter("b871Date").split(" ")[2]);
					xmlBean.setB871Month(validate.getMonth(request.getParameter("b871Date").split(" ")[1]));
					xmlBean.setB871Day(request.getParameter("b871Date").split(" ")[0]);
				} else {
					xmlBean.setB871Year(request.getParameter("b871Date").substring(6,10));
					xmlBean.setB871Month(request.getParameter("b871Date").substring(3,5));
					xmlBean.setB871Day(request.getParameter("b871Date").substring(0,2));
				}
				
			}
			
				
			//abXXX
			xmlBean.setAbeng(this.getConStr(request.getParameter("abeng")));
			
			
			//claims
			int claims_number = Integer.parseInt(request.getParameter("claims_number"));
			
			Set claimsList = new LinkedHashSet(0);
			for (int i = 0; i < claims_number; i++) {
				ClaimsBean claims = new ClaimsBean();
				String claims_str = this.getConStr(request.getParameter("claimsClm_" + i));
				claims.setClaimsClm(claims_str);
				claimsList.add(claims);
				if (!"".equals(claims_str)) {
					xmlBean.setClaims(claimsList);
				}
				
			}
			
			//description
			int descriptions_number = Integer.parseInt(request.getParameter("descriptions_number"));
			
			Set descriptionsList = new LinkedHashSet(0);
			for (int i = 0; i < descriptions_number; i++) {
				DescriptionsBean descriptions = new DescriptionsBean();
				String descriptions_str = this.getConStr(request.getParameter("description_" + i));
				descriptions.setDescription(descriptions_str);
				descriptionsList.add(descriptions);
				
				if (!"".equals(descriptions_str)) {
					xmlBean.setDescriptions(descriptionsList);
				}
				
			}
			
			
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return xmlBean;
	}

	public String getConStr(String oldStr){
		
		try{
			return new String(oldStr.getBytes("ISO-8859-1"),"UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
}
