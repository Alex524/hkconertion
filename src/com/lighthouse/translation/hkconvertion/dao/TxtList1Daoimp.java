package com.lighthouse.translation.hkconvertion.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.lighthouse.translation.beans.SplitTxt;

public class TxtList1Daoimp implements TxtList1Dao{
	
	private static TxtList1Daoimp instance;

	public static TxtList1Daoimp getInstance() {
		if (instance == null)

			instance = new TxtList1Daoimp();

		return instance;

	}
	
	//获得第一类list里面的所有bean信息
	public SplitTxt getList1Bean(String file, SplitTxt bean, String number) throws IOException{
		EditTxt editTxt = new EditTxt();
		FileReader filereader = null;
		BufferedReader reader = null;
		try {
			
				String pubNumber = number.split("--Ryan--")[0];
				String type = number.split("--Ryan--")[1];
				bean.setType(type);
				bean.setPubNumber(pubNumber);
				filereader = new FileReader(file);
				reader = new BufferedReader(filereader);
				String line=null;
				boolean pubNumberflag = false;
				while((line=reader.readLine())!=null){
					line=line.trim();
					
					if(line.length()>0){
						
						if (line.contains("[11]") && pubNumberflag) {
							pubNumberflag = false;
						}
						
						if (line.contains("[11]")){
							if (editTxt.isPubNumber_11(line, pubNumber)) {
								pubNumberflag = true;
							}
						}
						
						if (line.contains("[13]") && pubNumberflag) {
							String kind_13 = editTxt.getKind(line, "[13]");
							bean.setPubKind(kind_13);
						}
						
						if (line.contains("[25]") && pubNumberflag) {
							String appl_language_25 = editTxt.getKind(line, "[25]");
							bean.setAppLanguage(appl_language_25);
						}
						
						if (line.contains("[22]") && pubNumberflag) {
							String appl_date_25 = editTxt.getKind(line, "[22]");
							bean.setAppDate(appl_date_25);
						}
						
						if (line.contains("[30]") && pubNumberflag) {
							String str_30 = line.substring(5, line.length());
							if (str_30.split(" ").length == 3){
								bean.setPriDate(str_30.split(" ")[0]);
								bean.setPriCountry(str_30.split(" ")[1]);
								bean.setPriNumber(str_30.split(" ")[2]);
							}
						}
						
						if (line.contains("[54]") && pubNumberflag) {
							String title_54 = line.substring(5, line.length());
							bean.setTitle(title_54);
						}
						
						if (line.contains("[62]") && pubNumberflag) {
							bean.setDivDnum(line.split(" ")[2]);
							bean.setDivDate(line.split(" ")[1]);
						}
						
						if (line.contains("[72]") && pubNumberflag) {
							String inventor_72 = line.substring(5, line.length());
							bean.setInvName(inventor_72);
						}
						
						if (line.contains("[73]") && pubNumberflag) {
							String grantee_73 = line.substring(5, line.length());
							bean.setGranName(grantee_73);
						}
						
						if (line.contains("[74]") && pubNumberflag) {
							String attorney_74 = line.substring(5, line.length());
							bean.setAttName(attorney_74);
						}
						
						if (line.contains("[86]") && pubNumberflag) {
							String wo_appl_date_86 = editTxt.getKind(line, "[86]");
							bean.setWoAppDate(wo_appl_date_86);
						}
						
						if (line.contains("[87]") && pubNumberflag) {
							String wo_publ_date_87 = editTxt.getKind(line, "[87]");
							bean.setWoPublDate(wo_publ_date_87);
						}
						
					}
					
				
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
		
		return bean;
	}
}
