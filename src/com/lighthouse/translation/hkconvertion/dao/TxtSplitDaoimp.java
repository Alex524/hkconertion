package com.lighthouse.translation.hkconvertion.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.lighthouse.translation.common.consts.CommonConsts;

public class TxtSplitDaoimp implements TxtSplitDao{
		
	private static TxtSplitDaoimp instance;

	public static TxtSplitDaoimp getInstance() {
		if (instance == null)

			instance = new TxtSplitDaoimp();

		return instance;

	}
	
	
	//删除以“香港特別行政區政府知識產權署專利註冊處”为断点的前面所有文字
	
	public void delExcessTxt(String fileName, String filePath) throws Exception {
		FileReader filereader = null;
		BufferedReader reader = null;
		try{
			EditFile editFile = new EditFile();
			editFile.newFolder(filePath+"\\temp\\");
			editFile.copyFile(filePath+fileName, filePath+"\\temp\\"+fileName);
			String file = filePath+"\\temp\\"+fileName;
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			StringBuffer allLineStringBuffer = new StringBuffer();
			boolean flag = false;
			while((line=reader.readLine())!=null){
				line=line.trim();
				if(line.length()>0){
					if (line.contains("The Government of the Hong Kong Special Administrative Region") || flag){
						allLineStringBuffer.append(line);
						allLineStringBuffer.append("\r\n");
						flag = true;
					}
				}
				
			}
			
			FileOutputStream fos = new FileOutputStream(file);  
			Writer bw = new OutputStreamWriter(fos,"GBK");
			bw.write(allLineStringBuffer.toString());
			bw.flush();
			bw.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
	}
	
	// 分别生成3个txt文件
	public void createDirectoryTxt(String file, String applicationsFilePath, String grantsFilePath, String shortGrantsFilePath) throws Exception {
		
		FileReader filereader = null;
		BufferedReader reader = null;
		try{
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			StringBuffer applicationsStringBuffer = new StringBuffer();
			StringBuffer grantsStringBuffer = new StringBuffer();
			StringBuffer shortGrantsStringBuffer = new StringBuffer();
			boolean applicationsFlag = true;
			boolean grantsFlag = true;
			while((line=reader.readLine())!=null){
				line=line.trim();
				if(line.length()>0){
					if (line.contains("Granted Standard Patents published under section 27 of the Patents Ordinance")) {
						applicationsFlag = false;
					}
					
					if (applicationsFlag){
						applicationsStringBuffer.append(line);
						applicationsStringBuffer.append("\r\n");
					}
					
					if (line.contains("Granted Short-term Patents published under section 118 of the Patents Ordinance")) {
						grantsFlag = false;
					}
					
					if (!applicationsFlag && grantsFlag){
						grantsStringBuffer.append(line);
						grantsStringBuffer.append("\r\n");
					}
					
					if (!grantsFlag){
						shortGrantsStringBuffer.append(line);
						shortGrantsStringBuffer.append("\r\n");
					}
					
				}
				
			}
			
			if (applicationsStringBuffer.length() != 0) {
				FileOutputStream applicationsfos = new FileOutputStream(applicationsFilePath);  
				Writer applicationsBw = new OutputStreamWriter(applicationsfos,"GBK");
				applicationsBw.write(applicationsStringBuffer.toString());
				applicationsBw.flush();
				applicationsBw.close();
			}
			
			if (grantsStringBuffer.length() != 0) {
				FileOutputStream grantsfos = new FileOutputStream(grantsFilePath);  
				Writer grantsBw = new OutputStreamWriter(grantsfos,"GBK");
				grantsBw.write(grantsStringBuffer.toString());
				grantsBw.flush();
				grantsBw.close();
			}
			
			if (shortGrantsStringBuffer.length() != 0) {
				FileOutputStream shortGrantsfos = new FileOutputStream(shortGrantsFilePath);  
				Writer shortGrantsBw = new OutputStreamWriter(shortGrantsfos,"GBK");
				shortGrantsBw.write(shortGrantsStringBuffer.toString());
				shortGrantsBw.flush();
				shortGrantsBw.close();
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
			this.deleteFile(file);
			
		}
		
	}
	
	//生成3个目录下的4个list文件
	
	public void createListTxt(String file, String type) throws Exception {
		FileReader filereader = null;
		BufferedReader reader = null;
		
		String filePathList1 = CommonConsts.TXT_PATH+"temp\\"+type+"list1.txt";
		String filePathList2 = CommonConsts.TXT_PATH+"temp\\"+type+"list2.txt";
		String filePathList3 = CommonConsts.TXT_PATH+"temp\\"+type+"list3.txt";
		String filePathList4 = CommonConsts.TXT_PATH+"temp\\"+type+"list4.txt";
		try{
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			StringBuffer stringBufferList1 = new StringBuffer();
			StringBuffer stringBufferList2 = new StringBuffer();
			StringBuffer stringBufferList3 = new StringBuffer();
			StringBuffer stringBufferList4 = new StringBuffer();
			boolean flagList1 = false;
			boolean flagList2 = false;
			boolean flagList3 = false;
			boolean flagList4 = false;
			
			while((line=reader.readLine())!=null){
				line=line.trim();
				if(line.length()>0){
					
					
					if (line.contains("Arranged by International Patent Classification")) {
						flagList1 = true;
					}
					
					if (line.contains("Arranged by Publication Number")) {
						flagList2 = true;
					}
					
					if (line.contains("Arranged by Application Number")) {
						flagList3 = true;
					}
					
					if (line.contains("Arranged by Name of Applicant") || line.contains("Arranged by Name of Proprietor")) {
						flagList4 = true;
					}
					
					
					if ((flagList1 && !flagList2 && !flagList3 && !flagList4)){
						stringBufferList1.append(line);
						stringBufferList1.append("\r\n");
					}
					
					if (flagList2 && !flagList3 && !flagList4) {
						stringBufferList2.append(line);
						stringBufferList2.append("\r\n");
					}
					
					if (flagList3 && !flagList4) {
						stringBufferList3.append(line);
						stringBufferList3.append("\r\n");
					}
					
					if (flagList4) {
						stringBufferList4.append(line);
						stringBufferList4.append("\r\n");
					}
					
				}
				
			}
			
			
			if (stringBufferList1.length() != 0) {
				FileOutputStream fosList1 = new FileOutputStream(filePathList1);  
				Writer bwList1 = new OutputStreamWriter(fosList1,"GBK");
				bwList1.write(stringBufferList1.toString());
				bwList1.flush();
				bwList1.close();
			}
			
			if (stringBufferList2.length() != 0) {
				FileOutputStream fosList2 = new FileOutputStream(filePathList2);  
				Writer bwList2 = new OutputStreamWriter(fosList2,"GBK");
				bwList2.write(stringBufferList2.toString());
				bwList2.flush();
				bwList2.close();
			}
			
			if (stringBufferList3.length() != 0) {
				FileOutputStream fosList3 = new FileOutputStream(filePathList3);  
				Writer bwList3 = new OutputStreamWriter(fosList3,"GBK");
				bwList3.write(stringBufferList3.toString());
				bwList3.flush();
				bwList3.close();
			}
			
			if (stringBufferList4.length() != 0) {
				FileOutputStream fosList4 = new FileOutputStream(filePathList4);  
				Writer bwList4 = new OutputStreamWriter(fosList4,"GBK");
				bwList4.write(stringBufferList4.toString());
				bwList4.flush();
				bwList4.close();
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			this.delExcessList2(filePathList2);
			this.deleteFile(file);
			
		}
	}
	
	//删除list2多余字节
	public void delExcessList2(String file) throws Exception {
		FileReader filereader = null;
		BufferedReader reader = null;
		try{
			filereader = new FileReader(file);
			reader = new BufferedReader(filereader);
			String line=null;
			StringBuffer allLineStringBuffer = new StringBuffer();
			boolean flag1 = false;
			boolean flag2 = false;
			boolean flag3 = true;
			while((line=reader.readLine())!=null){
				line=line.trim();
				if(line.length()>0){
					
					if (line.contains("Journal No.")) {
						flag2 = false;
					}
					
					if (line.contains("Section Name")) {
						flag3 = false;
					}
					
					if (flag1&&flag2&&flag3) {
						allLineStringBuffer.append(line);
						allLineStringBuffer.append("\r\n");
					}
					
					if (line.contains("Publication No.")) {
						flag1 = true;
					}
					
					flag2 = true;
					flag3 = true;
					
				}
				
			}
			
			FileOutputStream fos = new FileOutputStream(file);  
			Writer bw = new OutputStreamWriter(fos,"GBK");
			bw.write(allLineStringBuffer.toString());
			bw.flush();
			bw.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
	}
	
	
	//合并3类txt文件
	public void mergeListTxt(String file, String type) throws Exception {
		FileReader filereader = null;
		BufferedReader reader = null;
		String fileList = "";
		String filePathList1 = CommonConsts.TXT_PATH+"temp\\applications"+type+".txt";
		String filePathList2 = CommonConsts.TXT_PATH+"temp\\grants"+type+".txt";
		String filePathList3 = CommonConsts.TXT_PATH+"temp\\shortGrants"+type+".txt";
		
		StringBuffer stringBuffer = new StringBuffer();
		for (int i=0; i<3; i++) {
			if (i == 0) {
				fileList = filePathList1;
			}
			
			if (i == 1) {
				fileList = filePathList2;
			}
			
			if (i == 2) {
				fileList = filePathList3;
			}
			
			try{
				
				filereader = new FileReader(fileList);
				reader = new BufferedReader(filereader);
				String line=null;
				while((line=reader.readLine())!=null){
					line=line.trim();
					
					if(line.length()>0){
						stringBuffer.append(line);
						stringBuffer.append("\r\n");
					}
					
				}
			
			
			
			}catch (Exception e) {
				e.printStackTrace();
			} finally {
			
				if (reader != null){
					reader.close();
				}
			
				if (filereader != null){
					filereader.close();
				}
			
			}
			
			if (stringBuffer.length() != 0) {
				FileOutputStream fos = new FileOutputStream(file);  
				Writer bw = new OutputStreamWriter(fos,"GBK");
				bw.write(stringBuffer.toString());
				bw.flush();
				bw.close();
			}
		}
		
	}
	
	
	
	public  void  deleteFile(String folderPath)  {  
	       try  {
	    	   String  filePath  =  folderPath;  
	           filePath  =  filePath.toString(); 
	           File myFilePath = new File(filePath);  
	           if (myFilePath.exists()) { 
	               myFilePath.delete();
	           }  
	       }catch (Exception  e) {   
	           e.printStackTrace();  
	       }  
	   }
	
	
	
	
	
	
	
	
}
