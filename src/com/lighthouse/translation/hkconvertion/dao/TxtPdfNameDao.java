package com.lighthouse.translation.hkconvertion.dao;

import java.util.List;

import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;

public interface TxtPdfNameDao {
	
	public List<SplitTxt> getPubNumberList(Session session, String type, String lock) throws Exception;
	public List getPubNumberLockList(Session session, String userName, String userGroup) throws Exception;
	public List getQAPubNumberLockList(Session session, String userName, String userGroup) throws Exception;
}
