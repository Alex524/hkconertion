package com.lighthouse.translation.hkconvertion.dao;

import java.io.IOException;
import java.util.List;

import com.lighthouse.translation.beans.SplitTxt;

public interface TxtList1Dao {
	public SplitTxt getList1Bean(String file, SplitTxt bean, String number) throws IOException;
}
