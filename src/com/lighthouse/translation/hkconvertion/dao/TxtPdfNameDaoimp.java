package com.lighthouse.translation.hkconvertion.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.common.consts.SqlManager;
import com.lighthouse.translation.beans.SplitTxt;

public class TxtPdfNameDaoimp implements TxtPdfNameDao{
	
	private static TxtPdfNameDaoimp instance;

	public static TxtPdfNameDaoimp getInstance() {
		if (instance == null)

			instance = new TxtPdfNameDaoimp();

		return instance;

	}
	
	public List<SplitTxt> getPubNumberList(Session session, String type, String lock) throws Exception {
		SplitTxt dto = null;
		Query query = null;
		List list = new ArrayList<SplitTxt>();
		try {
			String sql = CommonConsts.HQL_PUBNUMBER_NAME_LIST;
			query = session.createQuery(sql);
			query.setString("value1", type);
			query.setString("value2", lock);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}
	
	
	//获得登陆用户的文件名称
	public List getPubNumberLockList(Session session, String userName, String userGroup) throws Exception{
		Query query = null;
		List list = null;
		try {
			
			String hql = CommonConsts.HQL_PUBNUMBER_LOCK;
			SqlManager sqlManager = new SqlManager();
			String[] sql_fileds = new String[1];
			if (userGroup.equals("QA")) {
				sql_fileds[0] = "qaId";
			} else {
				sql_fileds[0] = "editorId";
			}
			
			hql = sqlManager.setFiledsValue(hql, sql_fileds);
			query = session.createQuery(hql);
			query.setString("value1", userName);
			list = query.list();
			session.flush();
			session.clear();
		} catch (Exception e) {

			throw e;
		}
		return list;
	}
	
	
	//获得QA锁定文件列表
	public List getQAPubNumberLockList(Session session, String userName, String userGroup) throws Exception{
		Query query = null;
		List list = null;
		try {
			
			String hql = CommonConsts.HQL_QA_PUBNUMBER_LOCK;
			SqlManager sqlManager = new SqlManager();
			String[] sql_fileds = new String[2];
			if (userGroup.equals("QA")) {
				sql_fileds[0] = "qaId";
			} else {
				sql_fileds[0] = "editorId";
			}
			sql_fileds[1] = "status";
			
			
			hql = sqlManager.setFiledsValue(hql, sql_fileds);
			query = session.createQuery(hql);
			query.setString("value1", userName);
			if (userGroup.equals("QA")) {
				query.setString("value2", "submit");
			} else {
				query.setString("value2", "submission");
			}
			list = query.list();
			session.flush();
			session.clear();
		} catch (Exception e) {

			throw e;
		}
		return list;
	}
	
}
