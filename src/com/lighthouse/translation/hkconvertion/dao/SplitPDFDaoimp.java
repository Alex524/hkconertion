package com.lighthouse.translation.hkconvertion.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.FtpBean;
import com.lighthouse.translation.hkconvertion.beans.SplitPdfBean;

public class SplitPDFDaoimp implements SplitPDFDao{
	private static SplitPDFDaoimp instance;

	public static SplitPDFDaoimp getInstance() {
		if (instance == null)

			instance = new SplitPDFDaoimp();

		return instance;

	}
	
	//获得还未解析的pubNumber列表
	public ArrayList<FtpBean> getPdfNameList(ArrayList list) throws Exception{
		ArrayList<FtpBean> listPDF = new ArrayList<FtpBean>();
		File file =  new File(CommonConsts.PDF_PATH);
		File[] filesPDF = file.listFiles();
		if (filesPDF != null) {
			for (int i=0; i<filesPDF.length; i++) {
				File tmp_file = filesPDF[i];
				if (tmp_file.getName().endsWith(".pdf")) {
					
					if (list.size() != 0) {
						
						if (!list.contains(tmp_file.getName().replace(".pdf", "").trim().toString())){
							FtpBean ftpBean = new FtpBean();
							ftpBean.setFtpNumber(tmp_file.getName().replace(".pdf", ""));
							listPDF.add(ftpBean);
						}
						
					} else {
						FtpBean ftpBean = new FtpBean();
						ftpBean.setFtpNumber(tmp_file.getName().replace(".pdf", ""));
						listPDF.add(ftpBean);
					}
					
				}
				
			}
			
		}
		
		return listPDF;
	}
	
	//获得以分离pdf但未保存txt的文件名列表
	public ArrayList<FtpBean> getTxtNameList(ArrayList splitPdfList, ArrayList txtList) throws Exception{
		ArrayList<FtpBean> listPDF = new ArrayList<FtpBean>();
		
		if (splitPdfList.size() != 0) {
			
			for (int i=0; i<splitPdfList.size(); i++) {
				
				if (!txtList.contains(splitPdfList.get(i).toString())) {
					FtpBean ftpBean = new FtpBean();
					ftpBean.setFtpNumber(splitPdfList.get(i).toString());
					listPDF.add(ftpBean);
				}
				
			}
			
		}
		
		return listPDF;
	}
	
	//获得pdfsplit文件夹下文件名列表
	public ArrayList getSplitPDFNameList() throws Exception{
		ArrayList listPDF = new ArrayList();
		File file =  new File(CommonConsts.HK_PATH+"txt\\");
		File[] filesPdf = file.listFiles();
		
		if (filesPdf != null) {
			
			for (int i=0; i<filesPdf.length; i++) {
				File tmp_file = filesPdf[i];
				
				if (tmp_file.getName().endsWith(".txt")) {
					String pdfName = tmp_file.getName().substring(0, tmp_file.getName().lastIndexOf("."));
					
					if (!listPDF.contains(pdfName)){ 
						listPDF.add(pdfName);
						
					}
					
				}
				
			}
			
		}
		
		return listPDF;
	}
	
	//获得以保存的pdfName列表
	public ArrayList getTxtPdfNameList(Session session) throws Exception {
		ArrayList listPDF = new ArrayList();
		Query query = null;
		List list = null;
		try {
			String hql = CommonConsts.HQL_SPLIT_PDF_NAME_LIST;
			query = session.createQuery(hql);
			list = query.list();
			Iterator iter = list.iterator();
			while(iter.hasNext()){
				SplitTxt splitTxt = (SplitTxt)iter.next();
				FtpBean ftpBean = new FtpBean();
				
				if (!listPDF.contains(splitTxt.getPdfName())) {
					listPDF.add(splitTxt.getPdfName());
				}
				
			}
			session.flush();
			session.clear();
		} catch (Exception e) {
			throw e;
		}
		return listPDF;
	}
	
	
}
