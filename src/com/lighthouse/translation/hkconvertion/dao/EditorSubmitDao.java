package com.lighthouse.translation.hkconvertion.dao;

import java.sql.Connection;
import java.util.List;

import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;

public interface EditorSubmitDao {
	
	public String getPdfName(Session session, String pubNumber) throws Exception;
	public List<SplitTxt> isStatus(Session session, String pdfName, String type) throws Exception;
	public int updateStatus(Session session, String status, String pubNumber) throws Exception;	
	public boolean deleteLockOut(Connection conn, String pubNumber) throws Exception;
	public String getStatus(Session session, String pdfName) throws Exception;
	public int updateQALock(Session session, String pubNumber) throws Exception;
	public int updateEditorLock(Session session, String lock, String pubNumber) throws Exception;
	public int updateOperateDate(Session session,String pubNumber)throws Exception;
	public int updateQADate(Session session,String pubNumber)throws Exception;
}
