package com.lighthouse.translation.hkconvertion.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.lighthouse.translation.hkconvertion.beans.FtpBean;
import com.lighthouse.translation.hkconvertion.beans.SplitPdfBean;

public interface SplitPDFDao {
	
	public ArrayList<FtpBean> getPdfNameList(ArrayList list) throws Exception;
	public ArrayList getSplitPDFNameList() throws Exception;
	public ArrayList getTxtPdfNameList(Session session) throws Exception;
	public ArrayList<FtpBean> getTxtNameList(ArrayList splitPdfList, ArrayList txtList) throws Exception;
	
}
