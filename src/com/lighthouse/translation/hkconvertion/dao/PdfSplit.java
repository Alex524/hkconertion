package com.lighthouse.translation.hkconvertion.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import org.pdfbox.exceptions.COSVisitorException;
import org.pdfbox.pdfparser.PDFParser;
import org.pdfbox.pdfwriter.COSWriter;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFMergerUtility;
import org.pdfbox.util.Splitter;

import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.hkconvertion.beans.SplitPdfBean;

public class PdfSplit {
	
	private static PdfSplit instance;

	public static PdfSplit getInstance() {
		if (instance == null) 

			instance = new PdfSplit(); 

		return instance;

	}
	
	//以单页为单位把整个pdf截取成单独的pdf并根据起始页重新组合pdf
	
	public void split(String path, String getName, SplitPdfBean splitPdf) throws Exception
    {	
		String split = "1";
		Splitter splitter = new Splitter();
        String pdfFile = path;
        InputStream input = null;
        PDDocument document = null;
        List documents = null;
	    String tempFolder = CommonConsts.HK_PATH+"pdfsplit\\temp\\";
	    String newFile = CommonConsts.HK_PATH+"pdfsplit\\";
		try
        {
			input = new FileInputStream(pdfFile);
	    	document = parseDocument(input);  
            splitter.setSplitAtPage(Integer.parseInt(split));
            documents = splitter.split(document);
            EditFile editFile = new EditFile();
            editFile.newFolder(tempFolder);
            
            for (int i=0; i<documents.size(); i++) {
               PDDocument doc = (PDDocument)documents.get( i );
               String fileName = tempFolder+"temp-"+i+".pdf";
               writeDocument(doc,fileName);
               doc.close();
            }
            
            this.newPdf(splitPdf.getStartPage_applications(), splitPdf.getEndPage_applications(), getName+"_applications.pdf", newFile, tempFolder, documents, editFile);
            this.newPdf(splitPdf.getStartPage_grants(), splitPdf.getEndPage_grants(), getName+"_grants.pdf", newFile, tempFolder, documents, editFile);
            this.newPdf(splitPdf.getStartPage_shortGrants(), splitPdf.getEndPage_shortGrants(), getName+"_shortgrants.pdf", newFile, tempFolder, documents, editFile);
            
            for (int j=0; j<documents.size(); j++) {
				editFile.deleteFile(tempFolder+"temp-"+j+".pdf");
			}
			
			editFile.deleteFile(tempFolder);
            
            
        } catch (FileNotFoundException e) {
       	 e.printStackTrace();
		 } finally {
			 if( input != null )
            {
                input.close();
            }
            if( document != null )
            {
                document.close();
            }
            for( int i=0; documents != null && i<documents.size(); i++ )
            {
                PDDocument doc = (PDDocument)documents.get( i );
                doc.close();
            }
        }
		 
    }
	
	private static PDDocument parseDocument( InputStream input )throws IOException
    {
        PDFParser parser = new PDFParser( input );
        parser.parse();
        return parser.getPDDocument();
    }
	
	private void writeDocument( PDDocument doc, String fileName ) throws IOException, COSVisitorException
    {
        FileOutputStream output = null;
        COSWriter writer = null;
        try
        {
            output = new FileOutputStream( fileName );
            writer = new COSWriter( output );
            writer.write( doc );
        }
        finally
        {
            if( output != null )
            {
                output.close();
            }
            if( writer != null )
            {
                writer.close();
            }
        }
    }
	
	// 生成新的PDF文件
	private void newPdf(String startPage, String endPage, String getName, String newFile, String tempFolder, List documents, EditFile editFile) throws Exception{
		
        if (!startPage.equals("NULL") && !endPage.equals("NULL")) {
    		int start = Integer.valueOf(startPage.toString());
    		int end = Integer.valueOf(endPage.toString());
    		String fileNew = newFile+ getName;
    		String fileOld = tempFolder+"\\temp-"+(start-1)+".pdf";    		   		
    		try {
				PDFMergerUtility pdfM = new PDFMergerUtility();
				pdfM.setDestinationFileName(fileOld);
				for (int i = start-1; i < end; i++) {					
					String fileTemp = tempFolder + "\\temp-" + i + ".pdf";					
					pdfM.addSource(new File(fileTemp));
				}
				pdfM.mergeDocuments();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
    		editFile.copyFile(fileOld, fileNew);    		
    	}
	}	
}
