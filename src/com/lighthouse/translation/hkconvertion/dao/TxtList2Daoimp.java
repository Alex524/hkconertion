package com.lighthouse.translation.hkconvertion.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.lighthouse.translation.beans.SplitTxt;

public class TxtList2Daoimp implements TxtList2Dao{
	private static TxtList2Daoimp instance;

	public static TxtList2Daoimp getInstance() {
		if (instance == null)

			instance = new TxtList2Daoimp();

		return instance;

	}
	
	//获得第二类list里面的所有bean信息
	public SplitTxt getList2Bean(String file, SplitTxt bean, String pubNumber) throws IOException{
		EditTxt editTxt = new EditTxt();
		FileReader filereader = null;
		BufferedReader reader = null;
		try {
			
			
				filereader = new FileReader(file);
				reader = new BufferedReader(filereader);
				String line=null;
				
				if (java.lang.Character.isDigit(pubNumber.substring(pubNumber.length()-1, pubNumber.length()).charAt(0))) {
					pubNumber = pubNumber.substring(2, pubNumber.length()-2);
				} else {
					pubNumber = pubNumber.substring(2, pubNumber.length()-1);
				}

				boolean pubNumberflag = false;
				while((line=reader.readLine())!=null){
					line=line.trim();
					
					if(line.length()>0){
						
						if (pubNumber.equals(line.split(" ")[0])){
							pubNumberflag = true;
						}
						
						if (line.split(" ").length == 2 && pubNumberflag) {
							bean.setIpcClass(line.split(" ")[1]);
						}
						
						if (line.split(" ").length > 2 && pubNumberflag) {
							bean.setRefCountry(line.split(" ")[1].substring(0, 2));
							bean.setRefNumber(line.split(" ")[1].substring(2, line.split(" ")[1].length()));
							String ipcClass = "";
							for (int i=0; i<line.split(" ").length-2; i++) {
								ipcClass = ipcClass+line.split(" ")[i+2]+" ";
							}
							bean.setIpcClass(ipcClass);
						}
						
						pubNumberflag = false;
						
					}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (reader != null){
				reader.close();
			}
			
			if (filereader != null){
				filereader.close();
			}
			
		}
		
		return bean;
	}
	
}
