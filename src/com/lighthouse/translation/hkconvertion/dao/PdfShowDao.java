package com.lighthouse.translation.hkconvertion.dao;

import org.hibernate.Session;

public interface PdfShowDao {
	public String getPdf(Session session, String pubNumber) throws Exception;
	public int updateLock(Session session, String pubNumber, String userName, String userGroup) throws Exception;
	public boolean selectUserNameLock(Session session, String pubNumber, String userName, String userGroup) throws Exception;
}
