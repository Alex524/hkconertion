package com.lighthouse.translation.hkconvertion.dao;

import java.io.IOException;
import java.util.List;

import org.hibernate.Session;

public interface TxtReadDao {
	public List getPubNumber(String file, List list, String kind, String type) throws IOException;
	public List getFilePath(String file1, String file2, String file3) throws IOException;
	public boolean addTxt(Session session, List list, String file1, String file2, String file3, String file4, String pdfName) throws Exception;
}
