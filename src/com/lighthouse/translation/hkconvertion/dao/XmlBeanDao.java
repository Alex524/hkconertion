package com.lighthouse.translation.hkconvertion.dao;

import javax.servlet.http.HttpServletRequest;

import com.lighthouse.translation.hkconvertion.beans.XmlEditBean;

public interface XmlBeanDao {
	
	public XmlEditBean getXmlBean(HttpServletRequest request, XmlEditBean xmlBean) throws Exception;
	
}
