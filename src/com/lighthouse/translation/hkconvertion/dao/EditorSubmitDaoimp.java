package com.lighthouse.translation.hkconvertion.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.lighthouse.translation.beans.SplitTxt;
import com.lighthouse.translation.common.consts.CommonConsts;

public class EditorSubmitDaoimp implements EditorSubmitDao{
	
	private static EditorSubmitDaoimp instance;

	public static EditorSubmitDaoimp getInstance() {
		if (instance == null)

			instance = new EditorSubmitDaoimp();

		return instance;

	}
	
	//根据pubNumber获得pdfName.
	public String getPdfName(Session session, String pubNumber) throws Exception {
		Query query = null;
		String pdfName = "";
		List list = new ArrayList<SplitTxt>();
		try {
			String sql = CommonConsts.HQL_PUBNUMBER_LIST;
			query = session.createQuery(sql);
			query.setString("value1", pubNumber);
			list = query.list();
			Iterator iter = list.iterator();
			while (iter.hasNext()) {
				SplitTxt splitTxt = (SplitTxt)iter.next();
				pdfName = splitTxt.getPdfName();
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return pdfName;
	}
	
	//根据pdfName判断status
	public List<SplitTxt> isStatus(Session session, String pdfName, String type) throws Exception {
		Query query = null;
		List<SplitTxt> list = new ArrayList<SplitTxt>();
		try {
			String sql = CommonConsts.HQL_PDFNAME_STATUS_LIST;
			query = session.createQuery(sql);
			query.setString("value1", pdfName);
			query.setString("value2", type);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}
	
	//根据pubNumber获得status
	public String getStatus(Session session, String pubNumber) throws Exception {
		Query query = null;
		List<SplitTxt> list = new ArrayList<SplitTxt>();
		String status = "";
		try {
			String sql = CommonConsts.HQL_PDFNAME_STATUS;
			query = session.createQuery(sql);
			query.setString("value1", pubNumber);
			list = query.list();
			Iterator iter = list.iterator();
			while (iter.hasNext()) {
				SplitTxt splitTxt = (SplitTxt)iter.next();
				status = splitTxt.getStatus();
			} 
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return status;
	}
	
	
	//根据pubNumber修改编辑状态.
	public int updateStatus(Session session, String status, String pubNumber) throws Exception {
		Query query = null;
		String pdfName = "";
		int i = 0;
		List list = new ArrayList<SplitTxt>();
		try {
			String sql = CommonConsts.HQL_PDFNAME;
			query = session.createQuery(sql);
			query.setString("value1", status);
			query.setString("value2", pubNumber);
			i = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return i;
	}
	
	//根据pubNumber修改operateDate.
	public int updateOperateDate(Session session,String pubNumber) throws Exception{
		Query query = null;
		int i = 0;
		try {
			String sql = "update SplitTxt set operateDate=:value1 where pubNumber=:value2";
			query = session.createQuery(sql);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String operateDate = sdf.format(new Date());
			query.setString("value1", operateDate);
			query.setString("value2", pubNumber);
			i = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return i;
	}
	
	//根据pubNumber修改lock状态.
	public int updateEditorLock(Session session, String lock, String pubNumber) throws Exception {
		Query query = null;
		String pdfName = "";
		int i = 0;
		List list = new ArrayList<SplitTxt>();
		try {
			String sql = CommonConsts.HQL_UPDATE_LOCK;
			query = session.createQuery(sql);
			query.setString("value1", lock);
			query.setString("value2", pubNumber);
			i = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return i;
	}
	
	//根据pubNumber修改QADate.
	public int updateQADate(Session session,String pubNumber)throws Exception{
		Query query = null;
		int i = 0;
		try {
			String sql = "update SplitTxt set qaDate=:value1 where pubNumber=:value2";
			query = session.createQuery(sql);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String operateDate = sdf.format(new Date());
			query.setString("value1", operateDate);
			query.setString("value2", pubNumber);
			i = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return i;
	}
	
	
	//根据pubNumber解除锁定，并移出QA用户名.
	public int updateQALock(Session session, String pubNumber) throws Exception {
		Query query = null;
		String pdfName = "";
		int i = 0;
		List list = new ArrayList<SplitTxt>();
		try {
			String sql = CommonConsts.HQL_QA_REOPEN;
			query = session.createQuery(sql);
			query.setString("value1", pubNumber);
			i = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return i;
	}
	
	//QA提交后删除数据库数据
	public boolean deleteLockOut(Connection conn, String pubNumber) throws Exception {

		boolean succ = false;
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(CommonConsts.SQL_DELETE_SUBMIT);

			pst.setString(1, pubNumber);
			int result = pst.executeUpdate();

			succ = true;
		} catch (Exception e) {

			throw e;
		} finally {
			if (pst != null)
				pst.close();
		}
		return succ;
	}
	
	
}
