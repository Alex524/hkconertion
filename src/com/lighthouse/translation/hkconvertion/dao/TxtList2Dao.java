package com.lighthouse.translation.hkconvertion.dao;

import java.io.IOException;

import com.lighthouse.translation.beans.SplitTxt;

public interface TxtList2Dao {
	public SplitTxt getList2Bean(String file, SplitTxt bean, String pubNumber) throws IOException;
}
