package com.lighthouse.translation.hkconvertion.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.lighthouse.translation.common.consts.CommonConsts;

public class EditFile {
	
	//create folder
	public  void  newFolder(String folderPath)  {  
	       try  {  
	    	   String  filePath  =  folderPath;  
	           filePath  =  filePath.toString(); 
	           File myFilePath = new File(filePath);  
	           if (!myFilePath.exists()) {  
	               myFilePath.mkdirs();
	           }  
	       }catch (Exception  e) {   
	           e.printStackTrace();  
	       }  
	   }
	
	//exists folder
	public boolean  existsFolder(String folderPath)  {  
		 boolean existsFolder = false;   
		 try  {  
	    	   String  filePath  =  folderPath;  
	           filePath  =  filePath.toString(); 
	           File myFilePath = new File(filePath);  
	           if (myFilePath.exists()) {  
	               existsFolder = true;
	           }  
	       }catch (Exception  e) {   
	           e.printStackTrace();  
	       }  
	       return existsFolder;
	   }
	
	//copyFile
	public void copyFile(String oldPath, String newPath) {  
	       try {  
	           int bytesum = 0;  
	           int byteread = 0;  
	           File oldfile = new File(oldPath);  
	           if (oldfile.exists()) {    
	               InputStream inStream = new FileInputStream(oldPath);  
	               FileOutputStream fs = new FileOutputStream(newPath);  
	               byte[] buffer = new byte[1444];  
	               int length;  
	               while ((byteread = inStream.read(buffer)) != -1) {  
	                   bytesum += byteread;
	                   fs.write(buffer, 0, byteread);  
	               }  
	               inStream.close();
	               fs.close();
	           }  
	       }catch (Exception e) {  
	           e.printStackTrace();  
	 
	       }  
	   }
	
	//deleteFile
	public  void  deleteFile(String folderPath)  {  
	       try  {  
	    	   String  filePath  =  folderPath;  
	           filePath  =  filePath.toString(); 
	           File myFilePath = new File(filePath);  
	           if (myFilePath.exists()) {  
	               myFilePath.delete();
	           }  
	       }catch (Exception  e) {   
	           e.printStackTrace();  
	       }  
	   }
	
	//deleteFilePath
	public  void  deleteDirectoryFile(String folderDirectoryPath)  {  
	       try  {  
	    	   File file = new File(folderDirectoryPath.toString());
	    	   String[] filelist = file.list();
	    	   
	    	   for (int i = 0; i < filelist.length; i++) {
	    		   File delfile = new File(folderDirectoryPath + "\\" + filelist[i]);
	    		   delfile.delete();
	    	   }
	    	   
	    	   file.delete();
	       }catch (Exception  e) {   
	           e.printStackTrace();  
	       }  
	   }
	
	//nameList
	public ArrayList<String> getCountryFileNameList(String dir, String type, String country){
		ArrayList<String> namesArray = new ArrayList<String>();
		File file = new File(dir);			
		File[] fileNames = file.listFiles();
		
		if (fileNames != null){
			for (int i = 0; i < fileNames.length; i++) {
		
				File tmp_file = fileNames[i];
				String countryName = tmp_file.getName().substring(0, 2);
				if (tmp_file.getName().endsWith("."+type) && countryName.equals(country)) {
					namesArray.add(tmp_file.getName().replace("."+type, ""));
				}
			}
	
		}
		return namesArray;
	}
	
	//getFileNameList
	
	public ArrayList<String> getFileNameList(String dir){
		ArrayList<String> namesArray = new ArrayList<String>();
		File file = new File(dir);			
		File[] fileNames = file.listFiles();
		if (fileNames != null){
			for (int i = 0; i < fileNames.length; i++) {
		
				File tmp_file = fileNames[i];
				if (tmp_file.getName().endsWith(".pdf")) {
					namesArray.add(tmp_file.getName().replace(".pdf", ""));
				}
			}
	
		}
		return namesArray;
	}
	
}
