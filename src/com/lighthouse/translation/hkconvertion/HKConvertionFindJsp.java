package com.lighthouse.translation.hkconvertion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lighthouse.translation.action.BaseAction;
import com.lighthouse.translation.plugin.HibernatePlugIn;

public class HKConvertionFindJsp extends BaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SessionFactory factory = null;
		Session session = null;
		List listTime = new ArrayList();
		try {
			factory = (SessionFactory) servlet.getServletContext()
			.getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			session = factory.openSession();
			Connection con = session.connection();
			String sql = "select pdfName,CONVERT(datetime,publDate, 101) from SplitTxt group by pdfName,CONVERT(datetime,publDate, 101) order by CONVERT(datetime,publDate, 101) desc";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				listTime.add(rs.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("listTime", listTime);
		request.setAttribute("allDocCount", new String("0"));
		request.setAttribute("trDocCount", new String("0"));
		request.setAttribute("qaDocCount", new String("0"));
		request.setAttribute("list1", new ArrayList());
		request.setAttribute("list2", new ArrayList());
		request.setAttribute("list3", new ArrayList());
		request.setAttribute("list4", new ArrayList());
				return mapping.findForward("docCount");
	}
}
