package com.lighthouse.translation.common.consts;

import java.io.*;

/**
 * @author Johnny
 * @category this class is used to make a filter which is used to get the right
 *           prefix or suffix files
 */

public class DirFilter implements FilenameFilter {
	private String prefix = "", suffix = ""; // 文件名的前缀、后缀

	public DirFilter(String filterstr) {
		filterstr = filterstr.toLowerCase();
		int i = filterstr.indexOf('*');
		int j = filterstr.indexOf('.');
		if (i > 0)
			prefix = filterstr.substring(0, i);
		if (j > 0)
			suffix = filterstr.substring(j);
	}

	public String[] getNames(String dir, FilenameFilter filter) { // 创建带通配符的文件名过滤器对象

		File f1 = new File(dir);
		File curdir = new File(f1.getAbsolutePath(), ""); // 当前目录
		System.out.println(curdir.getAbsolutePath());
		String[] str = curdir.list(filter); // 列出带过滤器的文件名清单
		return str;
	}

	public boolean accept(File dir, String filename) {
		boolean yes = true;
		try {
			filename = filename.toLowerCase();
			yes = (filename.startsWith(prefix)) & (filename.endsWith(suffix));
		} catch (NullPointerException e) {
		}
		return yes;
	}
}
