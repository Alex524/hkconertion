/*
 * Created on 2006-3-22
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.lighthouse.translation.common.consts;

import java.util.HashMap;
import java.util.List;

import com.lighthouse.translation.plugin.CountryPlugIn;
import com.lighthouse.translation.plugin.DockindPlugIn;
import com.lighthouse.translation.plugin.GroupPlugIn;
import com.lighthouse.translation.plugin.PropertyFileManager;

/**
 * @author Johnny
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
@SuppressWarnings("unchecked")
public class CommonConsts {
	
	//pdfeditor
	public static final List<String> COUNTRY_LIST = CountryPlugIn.getCountry_list();
	
	public static final String HQL_USER_SEARCH = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_USER_SEARCH");
	
	public static final String PDF_PATH = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "PDF_PATH");
	
	public static final String HQL_PDF_NAME_LIST = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PDF_NAME_LIST");
	
	public static final String HQL_PUBNUMBER_NAME_LIST = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PUBNUMBER_NAME_LIST");
	
	public static final String HQL_PUBNUMBER_LIST = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PUBNUMBER_LIST");
	
	public static final String HQL_PDFNAME_STATUS_LIST = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PDFNAME_STATUS_LIST");
	
	public static final String HQL_PDFNAME_STATUS = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PDFNAME_STATUS");
	
	public static final String HQL_PDFNAME = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PDFNAME");
	
	public static final String HQL_PDFNAME1 = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PDFNAME1");
	public static final String HQL_REOPEN_STATUS = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_REOPEN_STATUS");
	
	public static final String SQL_DELETE_SUBMIT = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "SQL_DELETE_SUBMIT");
	
	public static final String XML_PATH = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "XML_PATH");
	
	public static final String XML_PATH_Editor = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "XML_PATH_Editor");
	
	public static final String TXT_PATH = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "TXT_PATH");
	
	public static final String HK_PATH = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "HK_PATH");
	
	public static final String HQL_SPLIT_PDF_NAME_LIST = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_SPLIT_PDF_NAME_LIST");
	
	public static final String HQL_LOCK = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_LOCK");
	
	public static final String HQL_QA_UNLOCK = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_QA_UNLOCK");
	
	public static final String HQL_EDITOR_UNLOCK = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_EDITOR_UNLOCK");
	
	public static final String HQL_QA_REOPEN = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_QA_REOPEN");
	
	public static final String HQL_USERNAME_LOCK = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_USERNAME_LOCK");
	
	public static final String HQL_PUBNUMBER_LOCK = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_PUBNUMBER_LOCK");
	
	public static final String HQL_QA_PUBNUMBER_LOCK = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_QA_PUBNUMBER_LOCK");
	
	public static final String HQL_UPDATE_LOCK = (String) PropertyFileManager
	.getValue("HQL_FILE_HASH", "HQL_UPDATE_LOCK");
	
	public static final String XML_VALID_PATH = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "XML_VALID_PATH");
	
	public static final String XML_BACKUP = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "XML_BACKUP");
	
	public static final String XML_VALID_PATH_QA = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "XML_VALID_PATH_QA");
	
	public static final String XML_NEWVALID_PATH_QA = (String) PropertyFileManager
	.getValue("CONFIG_FILE_HASH", "XML_NEWVALID_PATH_QA");
	
	
	
}