package com.lighthouse.translation.common.consts;

/**
 * 
 * @author Johnny
 * @category this class is used to manage HQL or sql. it can replace many '#'
 *           from the HQL or Sql one by one.
 * 
 */
public class SqlManager {

	public String setFiledsValue(String strSql, String[] sql_fileds) {
		if (null == sql_fileds || "".equals(sql_fileds)) {
			throw new IllegalArgumentException("SQL fields are empty");
		} else {
			StringBuffer segments = new StringBuffer();

			int formatNumber = 0;

			char ch = '\0';
			try {
				final int iSqlLen = strSql.length();
				for (int i = 0; i < iSqlLen; i++) {
					ch = strSql.charAt(i);
					if ('#' == ch) {
						segments.append(sql_fileds[formatNumber]);
						formatNumber++;
					} else {
						segments.append(ch);
					}
				}
			} catch (IllegalArgumentException e) {
				throw new IllegalArgumentException("Set SQL fileds error");
			}
			return segments.toString();
		}
	}
}
