package com.lighthouse.translation.tags;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.lighthouse.translation.common.consts.CommonConsts;

public class CountryTags extends TagSupport {
	private String name = "";

	private String value = "";

	private String additems = "";

	/**
	 * @return This method override parent class method. It's used to generate a
	 *         select HTML element with country options.
	 */
	public final int doStartTag() throws JspException {
		try {
			List country = CommonConsts.COUNTRY_LIST;

			StringBuffer bf = new StringBuffer();
			bf.append("<select id=\"" + name + "\" name=\"" + name + "\" ");
			if(this.additems.equalsIgnoreCase("disabled")){
				bf.append(" disabled >");
			}
			bf.append(" >");
			
			bf.append(System.getProperty("line.separator"));
			if (this.additems.equals("All")) {
				bf.append("<option value=\"\"");
				if (value.equals("")) {
					bf.append(" Selected");
				}

				bf.append(">");
				bf.append("All");
				bf.append("</option>");
				bf.append(System.getProperty("line.separator"));
			}
			for (int i = 0; i < country.size(); i++) {

				bf.append("<option value=\"" + country.get(i) + "\"");
				if (value.equals(country.get(i))) {
					bf.append(" Selected");
				}

				bf.append(">");
				bf.append(country.get(i));
				bf.append("</option>");
				bf.append(System.getProperty("line.separator"));
			}
			
			bf.append("</select>");

			JspWriter writer = pageContext.getOut();
			writer.write(bf.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return EVAL_BODY_INCLUDE;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAdditems() {
		return additems;
	}

	public void setAdditems(String additems) {
		this.additems = additems;
	}
}
