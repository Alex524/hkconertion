package com.lighthouse.translation.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;



import com.lighthouse.translation.beans.UserTable;
import com.lighthouse.translation.dao.UserManageDao;
import com.lighthouse.translation.dao.UserManageDaoimp;
import com.lighthouse.translation.form.LoginForm;
import com.lighthouse.translation.plugin.HibernatePlugIn;

/**
 * @category The Action is used to login for admin,superviser,QA,translator.
 */
public class LoginAction extends BaseAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SessionFactory factory = null;
		ActionErrors errors = new ActionErrors();
		ActionError error = null;
		ActionForward forword = null;
		// add
		Session session = null;
		List list = null;
		try {

			// get form value
			LoginForm loginForm = (LoginForm) form;

			String valiNumber = loginForm.getValiNumber();
			// validate number
			if (!this.validateGimy(request, response, valiNumber)) {
				error = new ActionError("com.error.Validate.Error",
						"Noregister");

				errors.add("Noregister", error);

				saveErrors(request, errors);

				forword = mapping.findForward("faile");
				return forword;
			}
			
			

			// get Session factory
			factory = (SessionFactory) servlet.getServletContext()
					.getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			// add
			session = factory.openSession();
			// get dao
			UserManageDao dao = UserManageDaoimp.getInstance();
			list = dao.login(session, loginForm);
			// deal the return object
			if (list.size() == 0 || list.equals(null)) {
				error = new ActionError("com.error.Noregister.Error",
						"Noregister");

				errors.add("Noregister", error);

				saveErrors(request, errors);

				forword = mapping.findForward("faile");

			} else {
				UserTable translationpdf = (UserTable) list.get(0);
				String usergroup = translationpdf.getUsergroup();
				if ("admin".equals(usergroup)){
					forword = mapping.findForward("successPdfEdit");
				}
				
				if ("QA".equals(usergroup)) {
					forword = mapping.findForward("successXmlEditQA");
				} 
				
				if ("PdfEditor".equals(usergroup)) {
					forword = mapping.findForward("successPdfEditCountry");
				} 
				request.getSession().setAttribute("user", new UserTable());
				request.getSession().setAttribute("usergroup", usergroup);
				request.setAttribute("userBean", new UserTable());
				String userName = loginForm.getUserName();
				request.getSession().setAttribute("xmlcountry", translationpdf.getCountry());
				request.getSession().setAttribute("username", userName);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("exception", e.getMessage());

			return mapping.findForward("exception");
		} finally {

			if (session != null)
				session.close();

			if (factory != null)
				factory.close();
		}

		return forword;

	}

	private boolean validateGimy(HttpServletRequest request,
			HttpServletResponse response, String responseStr) {

		boolean isResponseCorrect = false;

		String captchaId = (String) request.getSession().getAttribute("rand");

		if (responseStr.equals(captchaId)) {
			isResponseCorrect = true;
		}

		return isResponseCorrect;
	}
}
