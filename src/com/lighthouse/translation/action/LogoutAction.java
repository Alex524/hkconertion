package com.lighthouse.translation.action;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.lighthouse.translation.hkconvertion.dao.LogoutUnlockDao;
import com.lighthouse.translation.hkconvertion.dao.LogoutUnlockDaoimp;
import com.lighthouse.translation.plugin.HibernatePlugIn;


public class LogoutAction extends BaseAction {
	/**
	 * @author Johnny,Kevin,Jason
	 * 
	 * @param mapping
	 *            ActionMapping
	 * @param form
	 *            ActionForm
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @return The Method is used to logout and clear the session .
	 *          return a ActionForward at the end.
	 * @throws Exception
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		response.setContentType("text/html,UTF-8");
		request.setCharacterEncoding("UTF-8");
		SessionFactory factory = null;
		Connection conn = null;
		Session sessionSql = null;
		Transaction transaction = null;
		try {
			HttpSession session = request.getSession();
			factory = (SessionFactory) servlet.getServletContext()
			.getAttribute(HibernatePlugIn.SESSION_FACTORY_KEY);
			sessionSql = factory.openSession();
			transaction = sessionSql.beginTransaction();
			conn = sessionSql.connection();
			String userName = request.getSession().getAttribute("username").toString();
			String userGroup = request.getSession().getAttribute("usergroup").toString();
			
			LogoutUnlockDao dao = LogoutUnlockDaoimp.getInstance();
			int unlock = dao.getUnlock(sessionSql, userName, userGroup);
			if (unlock>0) {
				transaction.commit();
			} else {
				transaction.rollback();
			}
			
			
			session.removeAttribute("user");
			session.setAttribute("user", null);
			session.invalidate();
		} catch (Exception e) {
			request.setAttribute("Exception", e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (conn != null)
				conn.close();

			if (sessionSql != null)
				sessionSql.close();

			if (factory != null)
				factory.close();
		}
		
		return mapping.findForward("success");
	}
}
