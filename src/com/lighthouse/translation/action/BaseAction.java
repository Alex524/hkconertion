/*
 * Created on 2006-3-23
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.lighthouse.translation.action;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionMapping;

import com.lighthouse.translation.common.consts.CommonConsts;

/**
 * @category Extends Action of Struts. Do the basic authentication check Check
 *           IP, time and so on
 * 
 * 
 */
public abstract class BaseAction extends Action {

	/**
	 * 
	 * @author Johnny,Kevin,Jason
	 * 
	 * @param mapping
	 *            ActionMapping
	 * @param request
	 *            HttpServletRequest
	 * @return The Method is used to check the mapping between Action and
	 *         Authentication. return a boolean at the end.
	 */



	/**
	 * @author Johnny,Kevin,Jason
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param user
	 *            Tableuser
	 * @return The Method is used to validateIP. return a boolean at the end.
	 */


	/**
	 * 
	 * @author Johnny,Kevin,Jason
	 * 
	 * @param user
	 *            Tableuser
	 * @return The Method is used validate time if you can use the tanslation
	 *         system. return a boolean at the end.
	 */
	

	/**
	 * @author Johnny,Kevin,Jason
	 * 
	 * @param ipArry
	 *            String[]
	 * @param ip
	 *            String
	 * @return The Method is used to check the ip if which belongs this group .
	 *         return a boolean at the end.
	 */
	private boolean checkIP(String[] ipArry, String ip) {
		boolean succ = false;
		String[] addr = ip.replace(".", "/").split("/");
		String ip_ = addr[0] + "." + addr[1] + ".*.*";
		for (int i = 0; i < ipArry.length; i++) {
			if (ip_.equals(ipArry[i])) {
				succ = true;
				break;
			}
		}
		return succ;

	}
}
