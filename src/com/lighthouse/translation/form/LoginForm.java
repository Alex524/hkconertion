package com.lighthouse.translation.form;

import org.apache.struts.validator.ValidatorActionForm;

public class LoginForm extends ValidatorActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userName = "";

	private String password = "";

	private String valiNumber = "";

	public String getPassword() {
		if (password != null)
			password = password.trim();
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		if (userName != null)
			userName = userName.trim();
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getValiNumber() {
		if (valiNumber != null)
			valiNumber = valiNumber.trim();
		return valiNumber;
	}

	public void setValiNumber(String valiNumber) {
		this.valiNumber = valiNumber;
	}
}
