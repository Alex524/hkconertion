package com.lighthouse.translation.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class DockindPlugIn implements PlugIn {
	private static final String DOCKIND_FILE = "com.lighthouse.translation.struts.prop.Dockind";

	private static HashMap dockind_map = null;

	private static HashMap dockind_beginTime_map = null;

	private static HashMap dockind_endTime_map = null;

	private static List<String> dockind_list = null;

	public void destroy() {

		dockind_map.clear();
		dockind_map = null;

	}

	public final void init(ActionServlet servlet, ModuleConfig config)
			throws ServletException {

		propertyFileRead();

	}

	private void propertyFileRead() {
		dockind_map = this.loadProp(DOCKIND_FILE, "ipscope");
		dockind_beginTime_map = this.loadProp(DOCKIND_FILE, "timebegin");
		dockind_endTime_map = this.loadProp(DOCKIND_FILE, "timeend");
		dockind_list = DockindPlugIn.parseList(dockind_map);

	}

	private HashMap loadProp(String config, String scope) {
		if (null == config || "".equals(config)) {
			return null;
		}
		HashMap<String, String> propTable = new HashMap<String, String>();
		String configName = config.replace('.', '/');
		StringBuffer buf = new StringBuffer();
		buf.append(configName);
		buf.append(".xml");
		String name = buf.toString(); // configName + ".properties";
		System.out.println("***********************************" + "Load  "
				+ name + "***********************************");
		InputStream is = null;

		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		if (null == classLoader) {
			classLoader = this.getClass().getClassLoader();
		}

		is = classLoader.getResourceAsStream(name);
		if (null != is) {

			try {
				SAXReader saxReader = new SAXReader();
				saxReader.setEncoding("UTF-8");
				Document document = saxReader.read(is);
				List list = document.selectNodes("/dockinds/dockind");
				Iterator iter = list.iterator();
				while (iter.hasNext()) {
					Element countryEle = (Element) iter.next();
					propTable.put(countryEle.attributeValue("namekind"),
							countryEle.attributeValue(scope));

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {

				}
			}
		}

		/** Copy the corresponding values into our cache */

		return propTable;
	}

	private static List<String> parseList(HashMap propTable) {
		if (null == propTable) {
			return null;
		}
		List<String> allKey = new ArrayList<String>();
		Iterator names = propTable.keySet().iterator();
		while (names.hasNext()) {
			String key = (String) names.next();
			allKey.add(key);
		}
		return allKey;
	}

	public static HashMap getDockind_map() {
		return dockind_map;
	}

	public static void setDockind_map(HashMap dockind_map) {
		DockindPlugIn.dockind_map = dockind_map;
	}

	public static HashMap getDockind_beginTime_map() {
		return dockind_beginTime_map;
	}

	public static void setDockind_beginTime_map(HashMap dockind_beginTime_map) {
		DockindPlugIn.dockind_beginTime_map = dockind_beginTime_map;
	}

	public static HashMap getDockind_endTime_map() {
		return dockind_endTime_map;
	}

	public static void setDockind_endTime_map(HashMap dockind_endTime_map) {
		DockindPlugIn.dockind_endTime_map = dockind_endTime_map;
	}

	public static List<String> getDockind_list() {
		return dockind_list;
	}

	public static void setDockind_list(List<String> dockind_list) {
		DockindPlugIn.dockind_list = dockind_list;
	}

	public static String getDOCKIND_FILE() {
		return DOCKIND_FILE;
	}
	
	
}
