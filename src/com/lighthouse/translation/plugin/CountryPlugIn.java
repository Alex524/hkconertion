package com.lighthouse.translation.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class CountryPlugIn implements PlugIn {
	private static final String COUNTRY_FILE = "com.lighthouse.translation.struts.prop.Country";

	private static HashMap country_map = null;

	private static HashMap country_beginTime_map = null;

	private static HashMap country_endTime_map = null;

	private static List<String> country_list = null;

	public void destroy() {

		country_map.clear();
		country_map = null;

	}

	public final void init(ActionServlet servlet, ModuleConfig config)
			throws ServletException {

		propertyFileRead();

	}
	private void propertyFileRead() {
		country_map = this.loadProp(COUNTRY_FILE, "ipscope");
		country_beginTime_map = this.loadProp(COUNTRY_FILE, "timebegin");
		country_endTime_map = this.loadProp(COUNTRY_FILE, "timeend");
		country_list = CountryPlugIn.parseList(country_map);

	}

	private HashMap loadProp(String config, String scope) {
		if (null == config || "".equals(config)) {
			return null;
		}
		HashMap<String, String> propTable = new HashMap<String, String>();
		String configName = config.replace('.', '/');
		StringBuffer buf = new StringBuffer();
		buf.append(configName);
		buf.append(".xml");
		String name = buf.toString(); // configName + ".properties";
		System.out.println("***********************************" + "Load  "
				+ name + "***********************************");
		InputStream is = null;

		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		if (null == classLoader) {
			classLoader = this.getClass().getClassLoader();
		}

		is = classLoader.getResourceAsStream(name);
		if (null != is) {

			try {
				SAXReader saxReader = new SAXReader();
				saxReader.setEncoding("UTF-8");
				Document document = saxReader.read(is);
				List list = document.selectNodes("/countries/country");
				Iterator iter = list.iterator();
				while (iter.hasNext()) {
					Element countryEle = (Element) iter.next();
					propTable.put(countryEle.attributeValue("namecountry"),
							countryEle.attributeValue(scope));

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {

				}
			}
		}

		/** Copy the corresponding values into our cache */

		return propTable;
	}

	private static List<String> parseList(HashMap propTable) {
		if (null == propTable) {
			return null;
		}
		List<String> allKey = new ArrayList<String>();
		Iterator names = propTable.keySet().iterator();
		while (names.hasNext()) {
			String key = (String) names.next();
			allKey.add(key);
		}
		return allKey;
	}

	public static HashMap getCountry_map() {
		return country_map;
	}

	public static void setCountry_map(HashMap country_map) {
		CountryPlugIn.country_map = country_map;
	}

	public static HashMap getCountry_beginTime_map() {
		return country_beginTime_map;
	}

	public static void setCountry_beginTime_map(HashMap country_beginTime_map) {
		CountryPlugIn.country_beginTime_map = country_beginTime_map;
	}

	public static HashMap getCountry_endTime_map() {
		return country_endTime_map;
	}

	public static void setCountry_endTime_map(HashMap country_endTime_map) {
		CountryPlugIn.country_endTime_map = country_endTime_map;
	}

	public static List<String> getCountry_list() {
		return country_list;
	}

	public static void setCountry_list(List<String> country_list) {
		CountryPlugIn.country_list = country_list;
	}

	public static String getCOUNTRY_FILE() {
		return COUNTRY_FILE;
	}

}
