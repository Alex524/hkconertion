package com.lighthouse.translation.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;

/**
 * 
 * @category This class is used to plugin the properties information, the
 *           mapping file is : 'com.lighthouse.translation.struts.prop.HQL',
 *           'com.lighthouse.translation.struts.prop.Config'
 *           'com.lighthouse.translation.struts.prop.Authentication' And init 3
 *           static hashmap
 *           variables:hqlString,configString,authenticationString
 * 
 */
public class PropertyFileManager implements PlugIn {
	private static final String HQL_FILE = "com.lighthouse.translation.struts.prop.HQL";

	private static final String CONFIG_FILE = "com.lighthouse.translation.struts.prop.Config";

	private static final String AUTHENTICATION_FILE = "com.lighthouse.translation.struts.prop.Authentication";

	private static final String MAIL_CONFIG_FILE = "com.lighthouse.translation.struts.prop.MailConfig";

	private static final String BAD_WORDS = "com.lighthouse.translation.struts.prop.BadWords";

	private static final String LIST_WORDS = "com.lighthouse.translation.struts.prop.ListWords";

	private static final String HQL_FILE_HASH = "HQL_FILE_HASH";

	private static final String CONFIG_FILE_HASH = "CONFIG_FILE_HASH";

	private static final String AUTHENTICATION_FILE_HASH = "AUTHENTICATION_FILE_HASH";

	private static final String MAIL_CONFIG_FILE_HASH = "MAIL_CONFIG_FILE_HASH";

	private static HashMap hqlString = null;

	private static HashMap configString = null;

	private static HashMap authenticationString = null;

	private static HashMap mailString = null;

	private static List<String> badWordList = new ArrayList<String>();

	private static List<String> listWordList = new ArrayList<String>();
	private static final String CAT_USERS = "com.lighthouse.translation.struts.prop.CatUsers";
	private static List<String> listCatUsers = new ArrayList<String>();
	
	private static final String COUNTRY = "com.lighthouse.translation.struts.prop.Country";
	private static HashMap countryString = null;
	
	private static final String DOCKIND = "com.lighthouse.translation.struts.prop.Dockind";
	private static HashMap dockindString = null;
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.PlugIn#destroy()
	 */
	public void destroy() {

		hqlString.clear();
		hqlString = null;

		configString.clear();
		configString = null;

		authenticationString.clear();
		authenticationString = null;

		mailString.clear();
		mailString = null;

		badWordList.clear();
		badWordList = null;

		listWordList.clear();
		listWordList = null;
		
		listCatUsers.clear();  //add by jane qiu
		listCatUsers=null;
		
		countryString.clear();
		countryString=null;
		
		dockindString.clear();
		dockindString=null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.PlugIn#init(org.apache.struts.action.ActionServlet,
	 *      org.apache.struts.config.ModuleConfig)
	 */
	public final void init(ActionServlet servlet, ModuleConfig config)
			throws ServletException {

		propertyFileRead();

	}

	private void propertyFileRead() {
		hqlString = this.loadProp(HQL_FILE);
		configString = this.loadProp(CONFIG_FILE);
		authenticationString = this.loadProp(AUTHENTICATION_FILE);
		mailString = this.loadProp(MAIL_CONFIG_FILE);
		badWordList = this.loadProp(BAD_WORDS, true);
		listWordList = this.loadProp(LIST_WORDS, true);
		listCatUsers=this.loadProp(CAT_USERS, true);   //add by jane qiu
		countryString=this.loadProp(COUNTRY);
		dockindString=this.loadProp(DOCKIND);
	}

	public static void setBadWordList(List<String> badWordList) {
		PropertyFileManager.badWordList = badWordList;
	}

	public static void setListWordList(List<String> listWordList) {
		PropertyFileManager.listWordList = listWordList;
	}

	private List<String> loadProp(String config, boolean isList) {
		if (null == config || "".equals(config)) {
			return null;
		}
		String configName = config.replace('.', '/');
		StringBuffer buf = new StringBuffer();
		buf.append(configName);
		buf.append(".properties");
		String name = buf.toString(); // configName + ".properties";
		System.out.println("***********************************" + "Load  "
				+ name + "***********************************");
		InputStream is = null;
		Properties props = new Properties();

		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		if (null == classLoader) {
			classLoader = this.getClass().getClassLoader();
		}

		is = classLoader.getResourceAsStream(name);
		if (null != is) {
			try {
				props.load(is);

			} catch (IOException e) {

			} finally {
				try {
					is.close();
				} catch (IOException e) {

				}
			}
		}

		/** Copy the corresponding values into our cache */
		if (1 > props.size()) {
			return null;
		}
		List<String> tmp_list = new ArrayList<String>();
		// HashMap<String, String> propTable = new HashMap<String, String>();
		Iterator names = props.keySet().iterator();
		while (names.hasNext()) {
			String key = (String) names.next();
			tmp_list.add(key);
			// propTable.put(key, props.getProperty(key));

		}
		return tmp_list;
		// this.setPath_map(propTable);

	}

	private HashMap loadProp(String config) {
		if (null == config || "".equals(config)) {
			return null;
		}

		String configName = config.replace('.', '/');
		StringBuffer buf = new StringBuffer();
		buf.append(configName);
		buf.append(".properties");
		String name = buf.toString(); // configName + ".properties";
		System.out.println("***********************************" + "Load  "
				+ name + "***********************************");
		InputStream is = null;
		Properties props = new Properties();

		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		if (null == classLoader) {
			classLoader = this.getClass().getClassLoader();
		}

		is = classLoader.getResourceAsStream(name);
		if (null != is) {
			try {
				props.load(is);

			} catch (IOException e) {

			} finally {
				try {
					is.close();
				} catch (IOException e) {

				}
			}
		}

		/** Copy the corresponding values into our cache */
		if (1 > props.size()) {
			return null;
		}

		HashMap<String, String> propTable = new HashMap<String, String>();
		Iterator names = props.keySet().iterator();
		while (names.hasNext()) {
			String key = (String) names.next();

			propTable.put(key, props.getProperty(key));
		}
		return propTable;
	}

	public static final Object getValue(String fileHashId, String key) {
		HashMap tagetHas = null;
		if (HQL_FILE_HASH.equals(fileHashId)) {
			tagetHas = hqlString;
		} else if (CONFIG_FILE_HASH.equals(fileHashId)) {
			tagetHas = configString;
		} else if (AUTHENTICATION_FILE_HASH.equals(fileHashId)) {
			tagetHas = authenticationString;
		} else if (MAIL_CONFIG_FILE_HASH.equals(fileHashId)) {
			tagetHas = mailString;
		} else {
			return null;
		}
		if (null == tagetHas) {
			return null;
		}

		return tagetHas.get(key);

	}

	public static final List getAllKey(String fileHashId) {
		if (HQL_FILE_HASH.equals(fileHashId)) {
			return parseList(hqlString);
		}
		if (CONFIG_FILE_HASH.equals(fileHashId)) {
			return parseList(configString);
		}
		if (MAIL_CONFIG_FILE_HASH.equals(fileHashId)) {
			return parseList(mailString);
		}
		if (AUTHENTICATION_FILE_HASH.equals(fileHashId)) {
			return parseList(authenticationString);
		}
		return null;

	}

	public static final List<String> getBadWordsList() {
		return badWordList;
	}

	public static final List<String> getListWordsList() {
		return listWordList;
	}
	public static final List<String> getListCatUsers() {
		return listCatUsers;
	}
	private static List parseList(HashMap propTable) {
		if (null == propTable) {
			return null;
		}
		List<String> allKey = new ArrayList<String>();
		Iterator names = propTable.keySet().iterator();
		while (names.hasNext()) {
			String key = (String) names.next();
			allKey.add(key);
		}
		return allKey;
	}
}
