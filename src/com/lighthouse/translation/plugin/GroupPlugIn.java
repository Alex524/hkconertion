package com.lighthouse.translation.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 
 * @category This class is used to plugin the userGroup information, the mapping
 *           file is 'com/lighthouse/translation/struts/prop/Group.xml' And init
 *           4 static variables:
 *           group_map,group_beginTime_map,group_endTime_map, and group_list
 * 
 */
public class GroupPlugIn implements PlugIn {

	private static final String GROUP_FILE = "com.lighthouse.translation.struts.prop.Group";

	private static HashMap group_map = null;

	private static HashMap group_beginTime_map = null;

	private static HashMap group_endTime_map = null;

	private static List<String> group_list = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.PlugIn#destroy()
	 */
	public void destroy() {

		group_map.clear();
		group_map = null;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.PlugIn#init(org.apache.struts.action.ActionServlet,
	 *      org.apache.struts.config.ModuleConfig)
	 */
	public final void init(ActionServlet servlet, ModuleConfig config)
			throws ServletException {

		propertyFileRead();

	}

	private void propertyFileRead() {
		group_map = this.loadProp(GROUP_FILE, "ipscope");
		group_beginTime_map = this.loadProp(GROUP_FILE, "timebegin");
		group_endTime_map = this.loadProp(GROUP_FILE, "timeend");
		group_list = GroupPlugIn.parseList(group_map);

	}

	private HashMap loadProp(String config, String scope) {
		if (null == config || "".equals(config)) {
			return null;
		}
		HashMap<String, String> propTable = new HashMap<String, String>();
		String configName = config.replace('.', '/');
		StringBuffer buf = new StringBuffer();
		buf.append(configName);
		buf.append(".xml");
		String name = buf.toString(); // configName + ".properties";
		System.out.println("***********************************" + "Load  "
				+ name + "***********************************");
		InputStream is = null;

		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		if (null == classLoader) {
			classLoader = this.getClass().getClassLoader();
		}

		is = classLoader.getResourceAsStream(name);
		if (null != is) {

			try {
				SAXReader saxReader = new SAXReader();
				saxReader.setEncoding("UTF-8");
				Document document = saxReader.read(is);
				List list = document.selectNodes("/groups/group");
				Iterator iter = list.iterator();
				while (iter.hasNext()) {
					Element groupEle = (Element) iter.next();
					propTable.put(groupEle.attributeValue("namegroup"),
							groupEle.attributeValue(scope));

				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {

				}
			}
		}

		/** Copy the corresponding values into our cache */

		return propTable;
	}

	private static List<String> parseList(HashMap propTable) {
		if (null == propTable) {
			return null;
		}
		List<String> allKey = new ArrayList<String>();
		Iterator names = propTable.keySet().iterator();
		while (names.hasNext()) {
			String key = (String) names.next();
			allKey.add(key);
		}
		return allKey;
	}

	public static HashMap getGroup_map() {
		return group_map;
	}

	public static void setGroup_map(HashMap group_map) {
		GroupPlugIn.group_map = group_map;
	}

	public static List<String> getGroup_list() {
		return group_list;
	}

	public static void setGroup_list(List<String> group_list) {
		GroupPlugIn.group_list = group_list;
	}

	public static HashMap getGroup_beginTime_map() {
		return group_beginTime_map;
	}

	public static void setGroup_beginTime_map(HashMap group_beginTime_map) {
		GroupPlugIn.group_beginTime_map = group_beginTime_map;
	}

	public static HashMap getGroup_endTime_map() {
		return group_endTime_map;
	}

	public static void setGroup_endTime_map(HashMap group_endTime_map) {
		GroupPlugIn.group_endTime_map = group_endTime_map;
	}

}
