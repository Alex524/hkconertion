package com.lighthouse.translation.plugin;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

/**
 * Send Mail
 * 
 * @author pangxin
 * 
 */
public class MailSender {

	private MimeMessage mimeMsg; // MIME mail object

	private Session session; // Mail sesstion object

	private MimeMultipart multipart = new MimeMultipart();

	private Properties props; // System properties

	private boolean needAuth = true; // Whether need SMTP authentication

	private boolean set_on_ = false;// Whether content mail condition

	private String encoding = "gb2312";

	private String smtpHost_;

	private String to_;

	private String cc_;

	private String bcc_;

	private String[] toList_;

	private String[] ccList_;

	private String[] bccList_;

	private String from_;

	private String password_;

	private String sub_;

	private String mailContent_;

	private String personalName_;

	private boolean mult = false;// Whether have attachment

	public MailSender() {

	}

	/**
	 * 
	 * @param smtp
	 * @param from
	 * @param password
	 */
	public MailSender(String smtp, String password) {
		setSmtpHost_(smtp);
		setPassword_(password);
	}

	/**
	 * no SMTP authentication
	 * 
	 * @param smtp
	 */
	public MailSender(String smtp) {
		setSmtpHost_(smtp);
	}

	/**
	 * 
	 * 
	 */
	private void init() {
		props = new Properties();
		props.put("mail.smtp.host", smtpHost_);
		Authenticat();
		mimeMsg = new MimeMessage(session);

	}

	/**
	 * Validate SMTP
	 * 
	 */
	private void Authenticat() {
		if (needAuth) {
			props.put("mail.smtp.auth", "true");
			SMTPAuth smtpau = new SMTPAuth(from_, password_);
			smtpau.getPasswordAuthentication();
			session = Session.getInstance(props, smtpau);
		} else {
			session = Session.getInstance(props, null);
		}
	}

	/**
	 * 
	 * @param toList
	 *            Addressee address list
	 * @param ccList
	 *            Carbon copy address list
	 * @param bccList
	 *            Blind carbon copy address list
	 * @param sub
	 *            Mail title
	 * @param msg
	 *            Mail straight matter
	 * @param from
	 *            Addresser address
	 * @param personalName
	 *            Addresser name
	 */
	public void set(String[] toList, String[] ccList, String[] bccList,
			String sub, String content, String from, String personalName) {
		setToList_(toList);
		setCcList_(ccList);
		setBccList_(bccList);
		setFrom_(from);

		setSub_(sub);
		setMailContent_(content);
		setPersonalName_(personalName);

		checkValidate();
	}

	/**
	 * 
	 * @param to
	 *            Addressee address
	 * @param cc
	 *            Carbon copy address
	 * @param bcc
	 *            Blind carbon copy address
	 * @param sub
	 *            Mail title
	 * @param msg
	 *            Mail straight matter
	 * @param from
	 *            Addresser address
	 * @param personalName
	 *            Addresser name
	 */
	public void set(String to, String cc, String bcc, String sub,
			String content, String from, String personalName) {
		setTo_(to);
		setCc_(cc);
		setBcc_(bcc);
		setFrom_(from);
		setSub_(sub);

		setMailContent_(content);
		setPersonalName_(personalName);

		checkValidate();
	}

	/**
	 * send mail
	 * 
	 * @throws Exception
	 * 
	 */
	public void doSend() throws Exception {
		init();
		if (set_on_) {
			if (to_ != null)
				setToRecipients(to_);
			if (cc_ != null)
				setCcRecipients(cc_);
			if (bcc_ != null)
				setBccRecipients(bcc_);
			if (toList_ != null)
				setToMultipleRecipients(toList_);
			if (ccList_ != null)
				setCcMultipleRecipients(ccList_);
			if (bccList_ != null)
				setBccMultipleRecipients(bccList_);
			sendOut();
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	private void sendOut() throws Exception {
		try {
			if (!set_on_) {
				System.err.println("Necessary parameter was not passed.");
				Thread.dumpStack();
				return;
			}

			if (personalName_ != null) {

				mimeMsg.setFrom(new InternetAddress(from_, personalName_,
						encoding));

			} else {

				mimeMsg.setFrom(new InternetAddress(from_));

			}

			mimeMsg.setSubject(MimeUtility.encodeWord(sub_, encoding, "B"));
			mimeMsg.setSentDate(new Date());
			if (!mult) {
				mimeMsg.setContent(mailContent_, "text/plain; charset="
						+ encoding + "");
			} else {
				setBody();
				mimeMsg.setContent(multipart);
			}
			Transport.send(mimeMsg);

		} catch (Exception e) {
            e.printStackTrace();
            throw e;
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	private void setBody() throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(mailContent_);
		multipart.addBodyPart(messageBodyPart);
	}

	/**
	 * upload one attachfile
	 * 
	 * @param path
	 *            filepath
	 * @throws Exception
	 */
	public void setAttachFile(String path) throws Exception {
		setAttachFile(path, getFileName(path));
	}

	/**
	 * upload more then one attachfile
	 * 
	 * @param paths
	 * @throws Exception
	 */
	public void setAttachFile(String[] paths) throws Exception {
		for (int i = 0; i < paths.length; i++) {
			setAttachFile(paths[i], getFileName(paths[i]));
		}
	}

	/**
	 * get attachfile name
	 * 
	 * @param paths
	 * @return
	 */
	private String getFileName(String paths) {
		String fileName;
		int n = paths.lastIndexOf("\\");
		if (n != -1) {
			fileName = paths.substring(n);
		} else {
			fileName = paths;
		}
		return fileName;
	}

	/**
	 * upload a file and give it a name
	 * 
	 * @param path
	 * @param name
	 * @throws Exception
	 */
	public void setAttachFile(String path, String name) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(path);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(name);
		multipart.addBodyPart(messageBodyPart);
		mult = true;
	}

	/**
	 * 
	 * @param toList
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void setToMultipleRecipients(String[] toList)
			throws AddressException, MessagingException {
		for (int i = 0; i < toList.length; ++i) {
			setToRecipients(toList[i]);
		}
	}

	/**
	 * 
	 * @param bccList
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void setBccMultipleRecipients(String[] bccList)
			throws AddressException, MessagingException {
		for (int i = 0; i < bccList.length; ++i) {
			setBccRecipients(bccList[i]);
		}
	}

	/**
	 * 
	 * @param ccList
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void setCcMultipleRecipients(String[] ccList)
			throws AddressException, MessagingException {
		for (int i = 0; i < ccList.length; ++i) {
			setCcRecipients(ccList[i]);
		}
	}

	/**
	 * 
	 * @param bcc
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void setBccRecipients(String bcc) throws AddressException,
			MessagingException {

		mimeMsg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(
				bcc, false));

	}

	/**
	 * 
	 * @param cc
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void setCcRecipients(String cc) throws AddressException,
			MessagingException {

		mimeMsg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(
				cc, false));

	}

	/**
	 * 
	 * @param to
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void setToRecipients(String to) throws AddressException,
			MessagingException {

		mimeMsg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(
				to, false));

	}

	/**
	 * check if Necessary parameter passed
	 * 
	 */
	private void checkValidate() {
		if ((smtpHost_ != null)
				&& (to_ != null || cc_ != null || bcc_ != null
						|| toList_ != null || ccList_ != null || bccList_ != null)
				&& (sub_ != null) && (mailContent_ != null || mult)
				&& (from_ != null))
			set_on_ = true;
	}

	/**
	 * set if do smtp auth
	 * 
	 * @param need
	 */
	public void setNeedAuth(boolean need) {
		this.needAuth = need;
	}

	/**
	 * 
	 * @param bcc_
	 */
	public void setBcc_(String bcc_) {
		this.bcc_ = bcc_;
	}

	/**
	 * 
	 * @param bccList_
	 */
	public void setBccList_(String[] bccList_) {
		this.bccList_ = bccList_;
	}

	/**
	 * 
	 * @param cc_
	 */
	public void setCc_(String cc_) {
		this.cc_ = cc_;
	}

	/**
	 * 
	 * @param ccList_
	 */
	public void setCcList_(String[] ccList_) {
		this.ccList_ = ccList_;
	}

	/**
	 * 
	 * @param to_
	 */
	public void setTo_(String to_) {
		this.to_ = to_;
	}

	/**
	 * 
	 * @param toList_
	 */
	public void setToList_(String[] toList_) {
		this.toList_ = toList_;
	}

	/**
	 * 
	 * @param smtpHost_
	 */
	public void setSmtpHost_(String smtpHost_) {
		this.smtpHost_ = smtpHost_;
	}

	/**
	 * 
	 * @param from_
	 */
	public void setFrom_(String from_) {
		this.from_ = from_;
	}

	/**
	 * 
	 * @param password_
	 */
	public void setPassword_(String password_) {
		this.password_ = password_;
	}

	/**
	 * 
	 * @param encoding
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	/**
	 * 
	 * @param mailContent_
	 */
	public void setMailContent_(String mailContent_) {
		this.mailContent_ = mailContent_;
	}

	/**
	 * 
	 * @param personalName_
	 */
	public void setPersonalName_(String personalName_) {
		this.personalName_ = personalName_;
	}

	/**
	 * 
	 * @param sub_
	 */
	public void setSub_(String sub_) {
		this.sub_ = sub_;
	}
}

/**
 * SMTP Validation Class
 * 
 * @author pangxin
 * 
 */
class SMTPAuth extends Authenticator {
	String username = null;

	String password = null;

	public SMTPAuth(String username, String password) {
		this.username = username;
		this.password = password;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
}
