package com.lighthouse.translation.nameValidation;

import javax.servlet.http.HttpServlet;

public class NameValidationServlet extends HttpServlet{
	
	public void init(){
		Thread validateThread = new NameValidationThread();
		validateThread.start();
	}

}
