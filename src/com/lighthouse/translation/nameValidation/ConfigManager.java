package com.lighthouse.translation.nameValidation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.lighthouse.translation.common.consts.CommonConsts;

public class ConfigManager {
	private Hashtable<String, NameInfo> namesTable = new Hashtable<String, NameInfo>();
	private static ConfigManager config = null;

	private ConfigManager() {
	}

	public static ConfigManager getInstance() {
		if (config == null) {
			config = new ConfigManager();
		}
		return config;
	}

	public void validateAndUpdateXML(String rootDir) throws Exception {
		File file = new File(rootDir);
		try {

			if (file == null) {
				System.out.println("The directory is not existed");
				return;
			}
			if (file.isDirectory()) {
				// System.out.println("Directory: "+ rootDir);
				processDir(file);
			}
			if (file.isFile()) {
				processFile(file);
				// System.out.println("File: "+ rootDir);
			}
		} catch (Exception ex) {
			System.out.println(rootDir);
			ex.printStackTrace();
			throw ex;
		}
		
	}

	private void processDir(File file) throws Exception {
		String[] fileList = file.list();
		String path = file.getAbsolutePath();
		for (int i = 0; i < fileList.length; i++) {
			String fileName = fileList[i];
			String filePath = path + "\\" + fileName;
			// System.out.println("filePath: "+ filePath);
			validateAndUpdateXML(filePath);
		}
	}

	private void processFile(File file){
		String fileName = file.getName();
		if((fileName.endsWith(".xml") || fileName.endsWith(".XML") )){
			if(fileName.endsWith(".xml") || fileName.endsWith(".XML")){
				Document document = this.getDocument(file);
				boolean isValid = NamesValidator.getInstance().startValidate(document);
				if(isValid){
					try{
						document = this.updateNames(document);
						createFile(document, file.getName());	
						file.delete();
					}catch(Exception ex){
						System.out.println(file.getAbsolutePath());
						ex.printStackTrace();
					}
				}
			}
		}		
	}

	public void createFile(Document document, String fileName) {
		String pubDate = this.getPubDate(document);
//		String path = CommonConsts.XML_VALID_PATH;
		 String path = "E:\\Work Station HK\\HKConvertion\\XML_Final";
		path = path + "\\" + pubDate;
		File file = new File(path);
		if (!file.exists()) {
			try {
				file.mkdir();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		path = path + "\\" + fileName;
		writeDocument(document, path);

	}

	public Document updateNames(Document document) {
		document = updateApplicants(document);
		document = updateInventors(document);
		document = updateGrantees(document);
		document = updateAttorney(document);
		return document;
	}

	private Document updateApplicants(Document document) {
		document = updateNames(document, "/patdoc/sdobi/B700/B710/B711");
		return document;
	}

	private Document updateInventors(Document document) {
		document = updateNames(document, "/patdoc/sdobi/B700/B720/B721");
		return document;
	}

	private Document updateGrantees(Document document) {
		document = updateNames(document, "/patdoc/sdobi/B700/B730/B731");
		return document;
	}

	private Document updateAttorney(Document document) {
		document = updateNames(document, "/patdoc/sdobi/B700/B740/B741");
		return document;
	}

	public Document getDocument(File fileName) {
		Document document = null;
		try {
			SAXReader saxReader = new SAXReader();
			saxReader.setEncoding("UTF-8");
			document = saxReader.read(fileName);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return document;
	}

	public String getPubDate(Document document) {
		String year = null;
		String month = null;
		String day = null;
		
		ArrayList<String> typeList = getTextList(document,"/patdoc/sdobi/B100/B130");
		String type = typeList.get(0);
		String dateTag = "B450";
		if(type.equals("A0")){
			dateTag = "B430";
		}
		
		ArrayList<String> list = getTextList(document,
				"/patdoc/sdobi/B400/"+dateTag+"/date/@Year");
		year = list.get(0);
		list = getTextList(document, "/patdoc/sdobi/B400/"+dateTag+"/date/@Month");
		month = list.get(0);
		list = getTextList(document, "/patdoc/sdobi/B400/"+dateTag+"/date/@Day");
		day = list.get(0);
		String date = year + month + day;
		return date;
	}

	public ArrayList<String> getTextList(Document document, String saxPath) {
		ArrayList<String> txtList = new ArrayList<String>();
		String text = "";
		List list = document.selectNodes(saxPath);
		Iterator iter = list.iterator();
		while (iter.hasNext()) {
			Node p = (Node) iter.next();
			text = p.getText();
			if (text != "" && text != null) {
				txtList.add(text);
			}
		}
		return txtList;
	}

	private Document updateNames(Document document, String saxPath) {
		List list = document.selectNodes(saxPath);
		Iterator iter = list.iterator();
		String text = "";
		NameInfo nameInfo = null;
		Element nameNode = null;

		while (iter.hasNext()) {

			Element topEle = (Element) iter.next();
			Element nameEle = topEle.element("snm");
			if (nameEle == null) {
				continue;
			}
			text = nameEle.getText().trim();
			if (text.equals("")) {
				continue;
			}
			nameInfo = namesTable.get(text);
			nameEle.setText(nameInfo.getEnName());

			// if(saxPath.indexOf("B740") != -1 || nameInfo.getCountryCode() ==
			// null){
			// continue;
			// }

			Element adrEle = topEle.element("adr");
			if (adrEle != null) {
				// adrEle = topEle.addElement("adr");
				Element str = adrEle.element("str");
				if (str != null) {
					adrEle.remove(str);
				}
				Element cntry = adrEle.element("cntry");
				if (cntry != null&&(saxPath.indexOf("710")!=-1||(saxPath.indexOf("730")!=-1))) {
					adrEle.setName("ctry");
				}

			}
		}
		return document;
	}

	public void writeDocument(Document document, String path) {
		try {

			XMLWriter output = new XMLWriter(new FileOutputStream(path));
			output.write(document);
			output.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void putName(String cnName, NameInfo nameInfo) {
		namesTable.put(cnName, nameInfo);
	}

	public void clearTable() {
		namesTable.clear();
	}

	public boolean hasName(String cnName) {
		if (namesTable.get(cnName) != null) {
			return true;
		}
		return false;
	}

	public static void main(String args[]) {
		String root = "E:/Work Station HK/HKConvertion/xml_valid";

		try {
			ConfigManager.getInstance().validateAndUpdateXML(root);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
