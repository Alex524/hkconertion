package com.lighthouse.translation.nameValidation;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.dom4j.Document;

public class NamesValidator {
	private static NamesValidator validator= null;
	private static Connection conn = null;	
	private ConfigManager conf = null; 
	
	private NamesValidator(){
		conn = NameDatabaseConnection.getInstance().initConnection();
		conf = ConfigManager.getInstance();
	}
	
	public static NamesValidator getInstance(){
		if(validator == null){
			validator = new NamesValidator();
		}
		return validator;
	}
	
	public boolean startValidate(Document document){		
		String countryCode = this.getCountryCode(document);
		String appNumber = this.getAppNumber(document);
		String kindCode = this.getKindCode(document);
		ArrayList<String> applicants = this.getApplicantNameAndAddr(document);
		ArrayList<String> inventors = this.getInventorNameAndAddr(document);
		ArrayList<String> grantees = this.getGranteeNameAndAddr(document);
		ArrayList<String> attorneys = this.getAttorneyName(document);
		
		boolean applicantValid = this.isAllValid(countryCode, appNumber, kindCode, applicants);
		if(!applicantValid){
			return false;
		}		
		boolean inventorsValid = this.isAllValid(countryCode, appNumber, kindCode, inventors);
		if(!inventorsValid){
			return false;
		}
		boolean granteesValid = this.isAllValid(countryCode, appNumber, kindCode, grantees);
		if(!granteesValid){
			return false;
		}
		boolean attorneysValid = this.isAllValid(countryCode, appNumber, kindCode, attorneys);
		if(!attorneysValid){
			return false;
		}
		return true;
		
	}	
	
	
	
	
	public boolean isAllValid(String countryCode, String appNumber, String kindCode,ArrayList<String> list){
		for(int i = 0; i < list.size(); i++){
			String cnName = list.get(i).trim();
			if(ConfigManager.getInstance().hasName(cnName)){
				continue;
			}
			boolean isValid = isValidSingleName(countryCode, appNumber, kindCode, cnName);
			if(!isValid){
				return false;
			}			
		}
		return true;
	}
	
	public boolean isValidSingleName(String countryCode, String appNumber, String kindCode,String cnName){
		boolean isValid = false;
		String orignalName = new String(cnName);		
		NameInfo nameInfo = getNameInfo(countryCode, appNumber, kindCode, cnName);
		if(nameInfo.getEnName() == null){
			cnName = cnName.replaceAll(" +", "");
			nameInfo = getNameInfo(countryCode, appNumber, kindCode, cnName);
			if(nameInfo.getEnName() == null){				
				int begin = cnName.indexOf("(");
				int end = cnName.indexOf(")");
				if(begin != -1 && end != -1){
					cnName = cnName.substring(0, begin)+cnName.substring(end+1);	
				}							
				nameInfo = getNameInfo(countryCode, appNumber, kindCode, cnName);
				if(nameInfo.getEnName() == null){
					cnName = cnName.replaceAll("\\d+", "");
					nameInfo = getNameInfo(countryCode, appNumber, kindCode, cnName);
					if(nameInfo.getEnName() == null){						
						return false;						
					}
					
				}else{
					isValid = true;
				}				
			}else{
				isValid = true;
			}
		}else{
			isValid = true;
		}
		if(isValid){
			ConfigManager.getInstance().putName(orignalName, nameInfo);
		}
		return isValid;
		
	}
	
	public NameInfo getNameInfo(String countryCode, String appNumber, String kindCode, String cnName){
		String name = null;
		String country = null;
		NameInfo nameInfo = new NameInfo();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			cstmt = conn.prepareCall("{call P_Select_EnglishName_Java(?,?,?)}");			
			cstmt.setString(1, appNumber);
			cstmt.setString(2, kindCode);
			cstmt.setString(3, cnName);	
			rs = (ResultSet) cstmt.executeQuery();
			while (rs.next()) {
				name = rs.getString("EnglishName");	
				country = rs.getString("CountryCode");
			}
			nameInfo.setEnName(name);
			nameInfo.setCountryCode(country);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				cstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return nameInfo;		
	}
	
	
	
	public String getCountryCode(Document document){
		//ArrayList<String> list = conf.getTextList(document, "/patdoc/sdobi/B100/B190");
		String countryCode = "HK";
		return countryCode;
	}
	
	public String getKindCode(Document document){
		String kindCode = null;		
		ArrayList<String> list = conf.getTextList(document, "/patdoc/sdobi/B100/B130");
		kindCode = list.get(0);
		return kindCode;
	}
	
	public String getAppNumber(Document document){
		String number = null;			
		ArrayList<String> list = conf.getTextList(document, "/patdoc/sdobi/B200/B210");
		number = list.get(0);
		return number;
	}
	
	public ArrayList<String> getApplicantNameAndAddr(Document document){
		ArrayList<String> inventors = new ArrayList<String>();		
		ArrayList<String> names = conf.getTextList(document, "/patdoc/sdobi/B700/B710/B711/snm");
//		ArrayList<String> strs = conf.getTextList(document,"/patdoc/sdobi/B700/B710/B711/adr/str");
//		ArrayList<String> cities = conf.getTextList(document, "/patdoc/sdobi/B700/B710/B711/adr/cntry");
		inventors.addAll(names);
//		inventors.addAll(strs);
//		inventors.addAll(cities);				
		return inventors;
	}
	
	
	
	public ArrayList<String> getInventorNameAndAddr(Document document){
		ArrayList<String> inventors = new ArrayList<String>();		
		ArrayList<String> names = conf.getTextList(document, "/patdoc/sdobi/B700/B720/B721/snm");
//		ArrayList<String> strs = conf.getTextList(document,"/patdoc/sdobi/B700/B720/B721/adr/str");
//		ArrayList<String> cities = conf.getTextList(document, "/patdoc/sdobi/B700/B720/B721/adr/cntry");
		inventors.addAll(names);
//		inventors.addAll(strs);
//		inventors.addAll(cities);				
		return inventors;
	}
	
	public ArrayList<String> getGranteeNameAndAddr(Document document){
		ArrayList<String> grantees = new ArrayList<String>();		
		ArrayList<String> names = conf.getTextList(document, "/patdoc/sdobi/B700/B730/B731/snm");
//		ArrayList<String> strs = conf.getTextList(document,"/patdoc/sdobi/B700/B730/B731/adr/str");
//		ArrayList<String> cities = conf.getTextList(document, "/patdoc/sdobi/B700/B730/B731/adr/cntry");
		grantees.addAll(names);
//		grantees.addAll(strs);
//		grantees.addAll(cities);				
		return grantees;
	}
	
	public ArrayList<String> getAttorneyName(Document document){
		ArrayList<String> attorney = new ArrayList<String>();		
		ArrayList<String> names = conf.getTextList(document, "/patdoc/sdobi/B700/B740/B741/snm");	
		
		attorney.addAll(names);						
		return attorney;
	}
	
	
}
