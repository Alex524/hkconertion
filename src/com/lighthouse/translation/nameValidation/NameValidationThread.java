package com.lighthouse.translation.nameValidation;

import com.lighthouse.translation.common.consts.CommonConsts;

public class NameValidationThread extends Thread{

	public void run() {		
		String rootDir = CommonConsts.XML_PATH;
		while(true){			
			try {
				
				ConfigManager.getInstance().validateAndUpdateXML(rootDir);
				sleep(5*1000);

			} catch (InterruptedException e) {
				e.printStackTrace();
				
			} catch (Exception e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
	}	
}
