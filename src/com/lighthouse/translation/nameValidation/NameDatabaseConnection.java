package com.lighthouse.translation.nameValidation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class NameDatabaseConnection {
	private static final String DRIVER_CLASS = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private static final String DATABASE_URL = "jdbc:sqlserver://192.168.1.2:1433;databaseName=CPO_Name2";
	private static final String DATABASE_USER = "sa";
	private static final String DATABASE_PASSWORD = "LSCD@dmin";
	
	private static Connection conn = null;
	private static NameDatabaseConnection connection = null;
	
	private NameDatabaseConnection(){
		
	}
	
	public static NameDatabaseConnection getInstance(){
		if(connection == null){
			connection = new NameDatabaseConnection();
		}
		return connection;
	}
		
	public Connection initConnection(){
		if(conn != null){
			return conn;
		}
		try {
			Class.forName(DRIVER_CLASS);
			conn = DriverManager.getConnection(DATABASE_URL, DATABASE_USER,
					DATABASE_PASSWORD);
			System.out.println("SQL2005 Connected!");
		} catch (Exception ex) {
			System.out.println("2:" + ex.getMessage());
		}
		return conn;
	}
	
	public void closeConnection(){
		if(conn != null){
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
