package com.lighthouse.translation.dao;

import java.util.List;

import org.hibernate.Session;

import com.lighthouse.translation.form.LoginForm;

/**
 * 
 * 
 * @category this interface is used to operate the communication between
 *           business logic and Database, focus on User operation.
 */
public interface UserManageDao {
	/**
	 * 
	 * @param session
	 *            Session
	 * @param loginForm
	 *            LoginForm
	 * @return This method is used to get the login account.
	 * @throws Exception
	 */
	public List login(Session session, LoginForm loginForm) throws Exception;

	
}