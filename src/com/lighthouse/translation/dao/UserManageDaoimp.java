package com.lighthouse.translation.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.lighthouse.translation.common.consts.CommonConsts;
import com.lighthouse.translation.common.consts.SqlManager;
import com.lighthouse.translation.form.LoginForm;

public class UserManageDaoimp implements UserManageDao {
	private static UserManageDaoimp instance;

	public static UserManageDaoimp getInstance() {
		if (instance == null) // 1

			instance = new UserManageDaoimp(); // 2

		return instance; // 3

	}

	public List login(Session session, LoginForm loginForm) throws Exception {

		Query query = null;
		List list = null;
		try {
			// get form value

			String userName = loginForm.getUserName();
			String password = loginForm.getPassword();

			//

			// create hql
			String hql = CommonConsts.HQL_USER_SEARCH;
			SqlManager sqlManager = new SqlManager();
			String[] sql_fileds = new String[2];
			sql_fileds[0] = "userName";
			sql_fileds[1] = "password";
			hql = sqlManager.setFiledsValue(hql, sql_fileds);

			// create query
			query = session.createQuery(hql);

			query.setString("value1", userName);
			query.setString("value2", password);

			list = query.list();
			session.flush();
			// get the lastest value
			session.clear();

		} catch (Exception e) {

			throw e;
		}
		return list;
	}
}